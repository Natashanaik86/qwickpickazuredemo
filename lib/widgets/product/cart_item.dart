import 'dart:convert';

import 'package:flash/flash.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';

import '../../common/constants.dart';
import '../../common/tools.dart';
import '../../generated/l10n.dart';
import '../../models/entities/index.dart' show AddonsOption;
import '../../models/index.dart'
    show AppModel, CartModel, Product, ProductVariation;
import '../../screens/checkout/widgets/payment_methods.dart';
import '../../services/index.dart';
import 'dialog_add_to_cart.dart';
import 'widgets/cart_button_with_quantity.dart';

// var productError = {};
// var qtyToBePassed;
var qtyCondition = {};

class ShoppingCartRow extends StatefulWidget {
  //on 28-4-2023 statelessWidget convert to stateful by natasha for api calling
  const ShoppingCartRow(
      {required this.product,
      required this.quantity,
      this.onRemove,
      this.onChangeQuantity,
      this.variation,
      this.options,
      this.addonsOptions,
      this.errorProduct});

  final Product? product;
  final List<AddonsOption>? addonsOptions;
  final ProductVariation? variation;
  final Map<String, dynamic>? options;
  final int? quantity;
  final Function? onChangeQuantity;
  final VoidCallback? onRemove;
  final String? errorProduct;

  @override
  State<ShoppingCartRow> createState() => _ShoppingCartRowState();
}

//--------------------------------------code comment on 19-7-23 for application loading (this api call in payment_method page)-----------------------
// ----------code added on 20-5-2023 for show error specific product by natasha and shireen---------------

// var productError = {};

// var cartModel;
// void getProductErrorListData(
//     BuildContext context,
//     CartModel cartModel,
//     ) async {
//   //23-5-2023
//   printLog('cartModel.item.keys');
//   printLog(cartModel.item.keys.toString());
//   // CartModel get cartModel => Provider.of<CartModel>(context, listen: false);
//   // Extract the product IDs and quantities from the CartModel
//   // List<Map<String, dynamic>> selectedProducts = cartModel.productsInCart.entries.map((entry) {
//   //   var productId = entry.key;
//   //   var quantity = entry.value;
//   //   print('productId $productId');
//   //   print('quantity $quantity');
//   //   return {
//   //     "product_id": productId,
//   //     "quantity": quantity,
//   //   };
//   // }).toList();
//   List<Map<String, dynamic>> selectedProducts = [];
//   cartModel.productsInCart.forEach((productId, quantity) {
//     //model.productsInCart.forEach((productId, quantity) {
//     selectedProducts.add({
//       'product_id': int.parse(productId),
//       'quantity': quantity,
//     });
//   });
//
//   printLog(selectedProducts);
//   // Create the request body
//   Map<String, dynamic> requestBody = {
//     'selected_products': selectedProducts,
//   };
//
//   // Convert the request body to JSON format
//   String jsonBody = json.encode(requestBody);
//   printLog('jsonBody $jsonBody');
//   // Make the HTTP POST request
//   var url = '$baseUrl/wp-json/wc/v2/flutter/check_product_errors';
//   final response = await http.post(
//     Uri.parse(url),
//     headers: {'Content-Type': 'application/json'},
//     body: json.encode(requestBody),
//   );
//   // Check the response status code
//   printLog('Response STATUS CODE IS ${response.statusCode}');
//   if (response.statusCode == 200) {
//     // Request was successful
//     printLog('Response: ${response.body}');
//     var responseData = json.decode(response.body);
//     printLog('response Data decode $responseData');
//
//     productError = {};
//     for (var item in responseData) {
//       productError[item['product_id'].toString()] = item['message'].toString();
//       qtyCondition[item['product_id'].toString()] = item['quantity'];
//     }
//     printLog(productError.toString());
//
//     //-------------------------22-5-2023----------------------
//   } else {
//     // Request failed
//     printLog('Error: ${response.statusCode}');
//   }
// }
//----------------------code end on 20-5-2023 --------------

class _ShoppingCartRowState extends State<ShoppingCartRow> {
  @override
  Widget build(BuildContext context) {
    var currency = Provider.of<AppModel>(context).currency;
    final currencyRate = Provider.of<AppModel>(context).currencyRate;

    final price = Services().widget.getPriceItemInCart(
        widget.product!, widget.variation, currencyRate, currency,
        selectedOptions: widget.addonsOptions);
    final imageFeature =
        widget.variation != null && widget.variation!.imageFeature != null
            ? widget.variation!.imageFeature
            : widget.product!.imageFeature;
    //unused code comment on 18-7-23 by natasha
    // var maxQuantity = kCartDetail['maxAllowQuantity'] ?? 100;
    // var totalQuantity = widget.variation != null
    //     ? (widget.variation!.stockQuantity ?? maxQuantity)
    //     : (widget.product!.stockQuantity ?? maxQuantity);
    // var limitQuantity =
    // totalQuantity > maxQuantity ? maxQuantity : totalQuantity;

    var theme = Theme.of(context);
    // printLog(' productError[widget.product!.id].toString()');
    // printLog(productError[widget.product!.id].toString());
    //code comment start on 23-7-23
    // setState(() {
    //   productError = productError;
    // });
    //code comment end on 23-7-23
    //cartModel = Provider.of<CartModel>(context, listen: false); //23-5-2023
    // getProductErrorListData(context, cartModel); //code added by natasha and shireen  for show error to specific product on 23-5-2023

    return LayoutBuilder(
      builder: (context, constraints) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              key: ValueKey(widget.product!.id),
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                if (widget.onRemove != null)
                  IconButton(
                    icon: const Icon(Icons
                        .delete_outline_outlined), //remove_circle_outline),
                    onPressed: widget.onRemove,
                  ),
                Expanded(
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(
                        width: constraints.maxWidth * 0.25,
                        height: constraints.maxWidth * 0.3,
                        child: ImageTools.image(url: imageFeature),
                      ),
                      const SizedBox(width: 16.0),
                      Expanded(
                        child: SingleChildScrollView(
                          child: Padding(
                            padding: const EdgeInsets.only(top: 12.0),
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  widget.product!.name!,
                                  style: TextStyle(
                                    fontSize: 15,
                                    color: theme.colorScheme.secondary,
                                    // fontWeight: FontWeight.w500
                                  ),
                                  maxLines: 4,
                                  overflow: TextOverflow.ellipsis,
                                ),
                                const SizedBox(height: 12),
                                Row(
                                  children: [
                                    SizedBox(
                                      width: MediaQuery.of(context).size.width /
                                          3.6, //112,
                                      height: 34,
                                      child: Selector<CartModel, int>(
                                        selector: (context, cartModel) =>
                                            cartModel.productsInCart[
                                                widget.product!.id] ??
                                            0,
                                        builder: (context, quantity, child) {
                                          return CartButtonWithQuantity(
                                              quantity: quantity,
                                              borderRadiusValue: 12,
                                              increaseQuantityFunction: () {
                                                var qty = qtyCondition1[
                                                        widget.product!.id]
                                                    .toString(); //26-6-2023
                                                if (int.tryParse(qty) == null ||
                                                    int.parse(qty) > quantity) {
                                                  //26-6-2023 code added if and else condition for checking stock quantity by natasha
                                                  addToCart1(
                                                    context,
                                                    quantity: 1, //quantity,//1,
                                                    product: widget.product!,
                                                  );
                                                } else {
                                                  showFlash(
                                                    context: context,
                                                    duration: const Duration(
                                                        seconds: 3),
                                                    persistent:
                                                        !Config().isBuilder,
                                                    builder:
                                                        (context, controller) {
                                                      return Flash(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(3.0),
                                                        backgroundColor:
                                                            Theme.of(context)
                                                                .errorColor,
                                                        controller: controller,
                                                        behavior: FlashBehavior
                                                            .floating,
                                                        position:
                                                            FlashPosition.top,
                                                        horizontalDismissDirection:
                                                            HorizontalDismissDirection
                                                                .horizontal,
                                                        child: FlashBar(
                                                          icon: const Icon(
                                                            Icons.check,
                                                            color: Colors.white,
                                                          ),
                                                          content: Text(
                                                            'Currently we only have $qty of this product',
                                                            style:
                                                                const TextStyle(
                                                              color:
                                                                  Colors.white,
                                                              fontSize: 18.0,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w700,
                                                            ),
                                                          ),
                                                        ),
                                                      );
                                                    },
                                                  );
                                                }
                                              },
                                              decreaseQuantityFunction: () {
                                                updateQuantity(
                                                  context: context,
                                                  quantity: quantity - 1,
                                                  product: widget.product!,
                                                );
                                              });
                                        },
                                      ),
                                    ),
                                    const Spacer(),
                                    Text(
                                      price!,
                                      style: TextStyle(
                                          color: theme.colorScheme.secondary,
                                          fontSize: 14,
                                          fontWeight: FontWeight.w700),
                                    ),
                                    const SizedBox(
                                      width: 12,
                                    ),
                                  ],
                                ),
                                const SizedBox(height: 10),
                                if (widget.product!.options != null &&
                                    widget.options != null)
                                  Services().widget.renderOptionsCartItem(
                                      widget.product!, widget.options),
                                if (widget.variation != null)
                                  Services().widget.renderVariantCartItem(
                                      context,
                                      widget.variation!,
                                      widget.options),
                                if (widget.addonsOptions?.isNotEmpty ?? false)
                                  Services().widget.renderAddonsOptionsCartItem(
                                      context, widget.addonsOptions),
                                if (widget.product?.store != null &&
                                    (widget.product?.store?.name != null &&
                                        widget.product!.store!.name!
                                            .trim()
                                            .isNotEmpty))
                                  Padding(
                                    padding: const EdgeInsets.only(top: 5.0),
                                    child: Text(
                                      widget.product!.store!.name!,
                                      style: TextStyle(
                                          color: theme.colorScheme.secondary,
                                          fontSize: 12),
                                    ),
                                  ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                const SizedBox(width: 16.0),
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(left: 18.0),
              child: productError[widget.product!.id].toString() == 'null'
                  ? const Text('')
                  : Text(
                      productError[widget.product!.id].toString(),
                      style: const TextStyle(
                          color: Colors.red,
                          fontWeight: FontWeight.w600,
                          fontSize: 13),
                    ),
            ),
            //code ended on 22-5-2023
            const SizedBox(height: 10.0),
            const Divider(color: kGrey200, height: 1),
            const SizedBox(height: 10.0),
          ],
        );
      },
    );
  }

  void addToCart1(
    BuildContext context, {
    int quantity = 1,
    bool enableBottomAddToCart = false,
    required Product product,
  }) {
    final String Function({
      dynamic context,
      dynamic isSaveLocal,
      Function notify,
      Map<String, dynamic> options,
      Product? product,
      int quantity,
      ProductVariation variation,
    }) addProductToCart =
        Provider.of<CartModel>(context, listen: false).addProductToCart;
    if (enableBottomAddToCart) {
      DialogAddToCart.show(context, product: product, quantity: quantity);
    } else {
      var message = addProductToCart(
        product: product,
        context: context,
        quantity: quantity,
      );
      _showFlashNotification(product, message, context);
    }
  }

  void _showFlashNotification(Product? product, String message, context) {
    if (message.isNotEmpty) {
      showFlash(
        context: context,
        duration: const Duration(seconds: 3),
        persistent: !Config().isBuilder,
        builder: (context, controller) {
          return Flash(
            borderRadius: BorderRadius.circular(3.0),
            backgroundColor: Theme.of(context).errorColor,
            controller: controller,
            behavior: FlashBehavior.floating,
            position: FlashPosition.top,
            horizontalDismissDirection: HorizontalDismissDirection.horizontal,
            child: FlashBar(
              icon: const Icon(
                Icons.check,
                color: Colors.white,
              ),
              content: Text(
                message,
                style: const TextStyle(
                  color: Colors.white,
                  fontSize: 18.0,
                  fontWeight: FontWeight.w700,
                ),
              ),
            ),
          );
        },
      );
    } else {
      showFlash(
        context: context,
        duration: const Duration(seconds: 3),
        persistent: !Config().isBuilder,
        builder: (context, controller) {
          return Flash(
            borderRadius: BorderRadius.circular(3.0),
            backgroundColor: Theme.of(context).primaryColor,
            controller: controller,
            behavior: FlashBehavior.floating,
            position: FlashPosition.top,
            horizontalDismissDirection: HorizontalDismissDirection.horizontal,
            child: FlashBar(
              icon: const Icon(Icons.check, color: kDarkBG //Colors.white,
                  ),
              title: Text(
                product!.name!,
                style: const TextStyle(
                  color: kDarkBG, //Colors.white,
                  fontWeight: FontWeight.w700,
                  fontSize: 15.0,
                ),
              ),
              content: Text(
                S.of(context).addToCartSucessfully,
                style: const TextStyle(
                  color: kDarkBG, //Colors.white,
                  fontSize: 15.0,
                ),
              ),
            ),
          );
        },
      );
    }
  }

  void updateQuantity({
    required Product product,
    required int quantity,
    required BuildContext context,
  }) =>
      Provider.of<CartModel>(context, listen: false)
          .updateQuantity(product, product.id!, quantity);
}
