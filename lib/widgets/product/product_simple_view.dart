import 'package:flash/flash.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../common/config.dart';
import '../../../common/constants.dart';
import '../../../common/tools.dart';
import '../../../generated/l10n.dart';
import '../../../models/index.dart' show AppModel, CartModel, Product, ProductVariation;
import '../../../routes/flux_navigate.dart';
import '../../modules/dynamic_layout/config/product_config.dart';
import '../../services/service_config.dart';
import 'dialog_add_to_cart.dart';
import 'index.dart' show  CartQuantity, ProductOnSale;
import 'widgets/cart_button_with_quantity.dart';

enum SimpleType { backgroundColor, priceOnTheRight }

class ProductSimpleView extends StatelessWidget {
  final Product? item;
  final SimpleType? type;
  final bool isFromSearchScreen;
  final bool enableBackgroundColor;
  final ProductConfig? config;

  const ProductSimpleView({
    this.item,
    this.type,
    this.isFromSearchScreen = false,
    this.enableBackgroundColor = true,
    this.config,
  });

  @override
  Widget build(BuildContext context) {
    if (item?.name == null) return const SizedBox();
    var productConfig = config ?? ProductConfig.empty();

    final currency = Provider.of<AppModel>(context).currency;
    final currencyRate = Provider.of<AppModel>(context).currencyRate;
    var screenWidth = MediaQuery.of(context).size.width;
    var titleFontSize = 15.0;
    var imageWidth = 60.0;
    var imageHeight = 60.0;

    final theme = Theme.of(context);

    var isSale = (item!.onSale ?? false) &&
        PriceTools.getPriceProductValue(item, currency, onSale: true) !=
            PriceTools.getPriceProductValue(item, currency, onSale: false);
    if (item!.isVariableProduct) {
      isSale = item!.onSale ?? false;
    }

    var _canAddToCart = !item!.isEmptyProduct() &&
        ((item?.inStock ?? false) || item!.backordersAllowed) &&
        item!.type != 'variable' &&
        item!.type != 'appointment' &&
        item!.type != 'booking' &&
        item!.type != 'external' &&
        (item!.addOns?.isEmpty ?? true);

    var priceProduct = PriceTools.getPriceProductValue(
      item,
      currency,
      onSale: true,
    );

    void onTapProduct(BuildContext context) {
      if (item!.imageFeature == '') return;

      if (isFromSearchScreen) {
        Navigator.of(context).pushNamed(
          RouteList.productDetail,
          arguments: item,
        );
        return;
      }
      FluxNavigate.pushNamed(
        RouteList.productDetail,
        arguments: item,
      );
    }

    /// Product Pricing
    Widget _productPricing = Wrap(
      crossAxisAlignment: WrapCrossAlignment.end,
      children: <Widget>[
        Text(
          item!.type == 'grouped'
              ? '${S.of(context).from} ${PriceTools.getPriceProduct(item, currencyRate, currency, onSale: true)}'
              : priceProduct == '0.0'
                  ? S.of(context).loading
                  : PriceTools.getPriceProduct(item, currencyRate, currency,
                      onSale: true)!,
          style: Theme.of(context).textTheme.headline6!.copyWith(
                fontSize: 15,
                color: theme.colorScheme.secondary,
              ),
        ),
        if (isSale) ...[
          const SizedBox(width: 5),
          Text(
            item!.type == 'grouped'
                ? ''
                : PriceTools.getPriceProduct(item, currencyRate, currency,
                    onSale: false)!,
            style: Theme.of(context).textTheme.headline6!.copyWith(
                  fontSize: 15,
                  fontWeight: FontWeight.w300,
                  color:
                      Theme.of(context).colorScheme.secondary.withOpacity(0.6),
                  decoration: TextDecoration.lineThrough,
                ),
          ),
        ]
      ],
    );

    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 5.0, horizontal: 10.0),
      child: GestureDetector(
        onTap: () => onTapProduct(context),
        child: Container(
          width: screenWidth,
          decoration: BoxDecoration(
            color: type == SimpleType.backgroundColor && enableBackgroundColor
                ? Theme.of(context).primaryColorLight
                : null,
            borderRadius: const BorderRadius.all(
              Radius.circular(20),
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Stack(
                  children: [
                    ClipRRect(
                      borderRadius:
                          const BorderRadius.all(Radius.circular(10.0)),
                      child: ImageTools.image(
                        url: item!.imageFeature,
                        width: imageWidth,
                        size: kSize.medium,
                        isResize: true,
                        height: imageHeight,
                        fit: BoxFit.cover,
                      ),
                    ),
                    ProductOnSale(
                      product: item!,
                      config: productConfig,
                      padding: const EdgeInsets.symmetric(horizontal: 3),
                      margin: EdgeInsets.zero,
                    ),
                  ],
                ),
                const SizedBox(
                  width: 20.0,
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        item!.name!,
                        style: TextStyle(
                          fontSize: titleFontSize,
                          fontWeight: FontWeight.w600,
                        ),
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                      ),
                      if (type != SimpleType.priceOnTheRight)
                        Row(
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(top: 8),
                              child: _productPricing,
                            ),
                            const Spacer(),
                            // code added on 8-5-2023 for show add button into search bar by natasha
                            Padding(
                              padding: const EdgeInsets.only(top: 8),
                              child: Selector<CartModel, int>(
                                selector: (context, cartModel) =>
                                cartModel.productsInCart[item!.id] ?? 0,
                                builder: (context, quantity, child) {
                                  return SizedBox(
                                    width: MediaQuery.of(context).size.width/3.76,//4,//112,//22-5-2023
                                    height: 33,
                                    child: CartButtonWithQuantity(
                                      quantity: quantity,
                                      borderRadiusValue: 12,
                                      increaseQuantityFunction: () {
                                        addToCart1(
                                          context,
                                          quantity: 1, //quantity,//1,
                                          product: item!,
                                        );
                                      },
                                      decreaseQuantityFunction: () => updateQuantity(
                                        context: context,
                                        quantity: quantity - 1,
                                        product: item!,
                                      ),
                                    ),
                                  );
                                },
                              ),
                            ),
                            // code ended on 8-5-2023 for show add button into search bar
                            if (productConfig.showQuantity)
                              SizedBox(
                                width: 140,
                                child: CartQuantity(
                                  product: item!,
                                  config: productConfig,
                                ),
                              ),
                          ],
                        ),
                    ],
                  ),
                ),
                if (type == SimpleType.priceOnTheRight)
                  Padding(
                    padding: const EdgeInsets.only(left: 20.0),
                    child: _productPricing,
                  ),
                if ((kProductDetail.showAddToCartInSearchResult &&
                    _canAddToCart &&
                    !productConfig.showQuantity))
                  Container(),
                  // CartIcon(
                  //   product: item!,
                  //   config: productConfig,
                  // ),
                  // code added start on 8-5-2023 for show add button into search bar by natasha
                  // Selector<CartModel, int>(
                  //   selector: (context, cartModel) =>
                  //   cartModel.productsInCart[item!.id] ?? 0,
                  //   builder: (context, quantity, child) {
                  //     return SizedBox(
                  //       width: MediaQuery.of(context).size.width/4,//112,
                  //       height: 33,
                  //       child: CartButtonWithQuantity(
                  //           quantity: quantity,
                  //           borderRadiusValue: 12,
                  //           increaseQuantityFunction: () {
                  //             addToCart1(
                  //               context,
                  //               quantity: 1, //quantity,//1,
                  //               product: item!,
                  //             );
                  //           },
                  //           decreaseQuantityFunction: () => updateQuantity(
                  //             context: context,
                  //             quantity: quantity - 1,
                  //             product: item!,
                  //           ),
                  //       ),
                  //     );
                  //   },
                  // ),
                // code ended on 8-5-2023 for show add button into search bar by natasha
              ],
            ),
          ),
        ),
      ),
    );
  }
  void addToCart1(
      BuildContext context, {
        int quantity = 1,
        bool enableBottomAddToCart = false,
        required Product product,
      }) {
    final String Function({
    dynamic context,
    dynamic isSaveLocal,
    Function notify,
    Map<String, dynamic> options,
    Product? product,
    int quantity,
    ProductVariation variation,
    }) addProductToCart =
        Provider.of<CartModel>(context, listen: false).addProductToCart;
    if (enableBottomAddToCart) {
      DialogAddToCart.show(context, product: product, quantity: quantity);
    } else {
      var message = addProductToCart(
        product: product,
        context: context,
        quantity: quantity,
      );
      _showFlashNotification(product, message, context);
    }
  }

  void _showFlashNotification(Product? product, String message, context) {
    if (message.isNotEmpty) {
      showFlash(
        context: context,
        duration: const Duration(seconds: 3),
        persistent: !Config().isBuilder,
        builder: (context, controller) {
          return Flash(
            borderRadius: BorderRadius.circular(3.0),
            backgroundColor: Theme.of(context).errorColor,
            controller: controller,
            behavior: FlashBehavior.floating,
            position: FlashPosition.top,
            horizontalDismissDirection: HorizontalDismissDirection.horizontal,
            child: FlashBar(
              icon: const Icon(
                Icons.check,
                color: Colors.white,
              ),
              content: Text(
                message,
                style: const TextStyle(
                  color: Colors.white,
                  fontSize: 18.0,
                  fontWeight: FontWeight.w700,
                ),
              ),
            ),
          );
        },
      );
    } else {
      showFlash(
        context: context,
        duration: const Duration(seconds: 3),
        persistent: !Config().isBuilder,
        builder: (context, controller) {
          return Flash(
            borderRadius: BorderRadius.circular(3.0),
            backgroundColor: Theme.of(context).primaryColor,
            controller: controller,
            behavior: FlashBehavior.floating,
            position: FlashPosition.top,
            horizontalDismissDirection: HorizontalDismissDirection.horizontal,
            child: FlashBar(
              icon: const Icon(
                Icons.check,
                color: kDarkBG,//Colors.white,
              ),
              title: Text(
                product!.name!,
                style: const TextStyle(
                  color: kDarkBG,//Colors.white,
                  fontWeight: FontWeight.w700,
                  fontSize: 15.0,
                ),
              ),
              content: Text(
                S.of(context).addToCartSucessfully,
                style: const TextStyle(
                  color: kDarkBG,//Colors.white,
                  fontSize: 15.0,
                ),
              ),
            ),
          );
        },
      );
    }
  }
  void updateQuantity({
    required Product product,
    required int quantity,
    required BuildContext context,
  }) =>
      Provider.of<CartModel>(context, listen: false)
          .updateQuantity(product, product.id!, quantity);
}
