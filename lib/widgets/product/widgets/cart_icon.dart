import 'package:flash/flash.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../common/config.dart';
import '../../../generated/l10n.dart';
import '../../../models/cart/cart_base.dart';
import '../../../models/entities/product.dart';
import '../../../models/entities/product_variation.dart';
import '../../../modules/dynamic_layout/config/product_config.dart';
import '../../../services/service_config.dart';
import '../action_button_mixin.dart';
import '../dialog_add_to_cart.dart';
import 'cart_button_with_quantity.dart';

class CartIcon extends StatelessWidget with ActionButtonMixin {
  final Product product;
  final ProductConfig config;
  final int quantity;

  const CartIcon({
    Key? key,
    required this.product,
    required this.config,
    this.quantity = 1,
  }) : super(key: key);

  void _addToCart(context, enableBottomSheet) {
    addToCart(
      context,
      product: product,
      quantity: quantity,
      enableBottomAddToCart: enableBottomSheet,
    );
  }

  @override
  Widget build(BuildContext context) {
    if (!config.showCartIcon ||
        product.isEmptyProduct() ||
        !kEnableShoppingCart) {
      return const SizedBox();
    }

    var enableBottomSheet =
    config.enableBottomAddToCart ? true : !product.canBeAddedToCartFromList();


    if (config.showCartIconColor) {
      return Container(
        height: 36.0,
        width: 36.0,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(config.cartIconRadius),
          color: Theme
              .of(context)
              .primaryColor,
        ),
        child: IconButton(
          onPressed: () => _addToCart(context, enableBottomSheet),
          icon: const Icon(
            Icons.add,
            size: 18.0,
            color: Colors.white,
          ),
        ),
      );
    }

    return InkWell(
      // constraints: showQuantitySelector ? const BoxConstraints() : null,
      child: const Padding(
        padding: EdgeInsets.symmetric(horizontal: 2),
        child: Icon(
          Icons.add_shopping_cart,
          size: 18.0,
        ),
      ),
      onTap: () => _addToCart(context, enableBottomSheet),
    );
    //code end commented and added on 5-5-2023
    //   return SizedBox(
    //     width: MediaQuery.of(context).size.width/4,//112,
    //     height: 30,
    //     child: CartButtonWithQuantity(
    //       quantity: quantity,
    //       borderRadiusValue: 12,
    //       increaseQuantityFunction: () {
    //         addToCart1(
    //           context,
    //           quantity: 1, //quantity,//1,
    //           product: product,
    //         );
    //       },
    //       decreaseQuantityFunction: () => updateQuantity(
    //         context: context,
    //         quantity: quantity - 1,
    //         product: product,
    //       ),
    //     ),
    //   );
    // }
    // void addToCart1(
    //     BuildContext context, {
    //       int quantity = 1,
    //       bool enableBottomAddToCart = false,
    //       required Product product,
    //     }) {
    //   final String Function({
    //   dynamic context,
    //   dynamic isSaveLocal,
    //   Function notify,
    //   Map<String, dynamic> options,
    //   Product? product,
    //   int quantity,
    //   ProductVariation variation,
    //   }) addProductToCart =
    //       Provider.of<CartModel>(context, listen: false).addProductToCart;
    //   if (enableBottomAddToCart) {
    //     DialogAddToCart.show(context, product: product, quantity: quantity);
    //   } else {
    //     var message = addProductToCart(
    //       product: product,
    //       context: context,
    //       quantity: quantity,
    //     );
    //     _showFlashNotification(product, message, context);
    //   }
    // }
    // void _showFlashNotification(Product? product, String message, context) {
    //   if (message.isNotEmpty) {
    //     showFlash(
    //       context: context,
    //       duration: const Duration(seconds: 3),
    //       persistent: !Config().isBuilder,
    //       builder: (context, controller) {
    //         return Flash(
    //           borderRadius: BorderRadius.circular(3.0),
    //           backgroundColor: Theme.of(context).errorColor,
    //           controller: controller,
    //           behavior: FlashBehavior.floating,
    //           position: FlashPosition.top,
    //           horizontalDismissDirection: HorizontalDismissDirection.horizontal,
    //           child: FlashBar(
    //             icon: const Icon(
    //               Icons.check,
    //               color: Colors.white,
    //             ),
    //             content: Text(
    //               message,
    //               style: const TextStyle(
    //                 color: Colors.white,
    //                 fontSize: 18.0,
    //                 fontWeight: FontWeight.w700,
    //               ),
    //             ),
    //           ),
    //         );
    //       },
    //     );
    //   } else {
    //     showFlash(
    //       context: context,
    //       duration: const Duration(seconds: 3),
    //       persistent: !Config().isBuilder,
    //       builder: (context, controller) {
    //         return Flash(
    //           borderRadius: BorderRadius.circular(3.0),
    //           backgroundColor: Theme.of(context).primaryColor,
    //           controller: controller,
    //           behavior: FlashBehavior.floating,
    //           position: FlashPosition.top,
    //           horizontalDismissDirection: HorizontalDismissDirection.horizontal,
    //           child: FlashBar(
    //             icon: const Icon(
    //               Icons.check,
    //               color: Colors.white,
    //             ),
    //             title: Text(
    //               product!.name!,
    //               style: const TextStyle(
    //                 color: Colors.white,
    //                 fontWeight: FontWeight.w700,
    //                 fontSize: 15.0,
    //               ),
    //             ),
    //             content: Text(
    //               S.of(context).addToCartSucessfully,
    //               style: const TextStyle(
    //                 color: Colors.white,
    //                 fontSize: 15.0,
    //               ),
    //             ),
    //           ),
    //         );
    //       },
    //     );
    //   }
    //code end commented and added on 5-5-2023
  }
}
