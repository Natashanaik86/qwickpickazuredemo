import 'package:flutter/material.dart';

import '../../../common/constants.dart';

class CartButtonWithQuantity extends StatefulWidget {
  const CartButtonWithQuantity({
    Key? key,
    required this.quantity,
    this.borderRadiusValue = 0,
    required this.increaseQuantityFunction,
    required this.decreaseQuantityFunction,
  }) : super(key: key);

  final int quantity;
  final double borderRadiusValue;
  final VoidCallback increaseQuantityFunction;
  final VoidCallback decreaseQuantityFunction;

  @override
  _CartButtonWithQuantityState createState() => _CartButtonWithQuantityState();
}

class _CartButtonWithQuantityState extends State<CartButtonWithQuantity> {
  var _isShowQuantity = false;

  int get _quantity => widget.quantity;

  final _focusNode = FocusNode();

  @override
  void didUpdateWidget(covariant CartButtonWithQuantity oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (_quantity == 0) {
      _focusNode.unfocus();
    }
  }

  @override
  void dispose() {
    _focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Focus(
      focusNode: _focusNode,
      onFocusChange: (hasFocus) {
        if (hasFocus) {
          if (!_isShowQuantity) {
            showQuantity();
          }
        } else {
          if (_isShowQuantity) {
            hideQuantity();
          }
        }
      },
      child: Builder(
        builder: (BuildContext context) {
          final hasFocus = _focusNode.hasFocus;
          return GestureDetector(
              onTap: () {
                if (hasFocus) {
                  _focusNode.unfocus();
                } else {
                  _focusNode.requestFocus();
                }
              },
              child: AnimatedSwitcher(
                duration: const Duration(milliseconds: 150),
                child: _isShowQuantity
                    ? buildSelector()
                    : _quantity == 0
                        ? buildAddButton()
                        : buildSelector(),
                //: buildQuantity(),
              ));
        },
      ),
    );
  }

  //Container(),
  Widget buildSelector() {
    return Container(
      margin: const EdgeInsets.only(
        bottom: 6,
        right: 6,
      ),
      // height: 40,
      height: 30,
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
        //color: Colors.white,
        color: const Color.fromRGBO(248, 149, 31, 1),
        border: Border.all(
          color: Colors.transparent,
        ),
        borderRadius: BorderRadius.circular(widget.borderRadiusValue),
      ),
      child: Row(
       // mainAxisAlignment: MainAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: [
          _quantity == 0
              ? InkWell(
                  onTap: (){},
                  child: const Padding(
                    padding: EdgeInsets.symmetric(horizontal: 12.0,vertical: 4.0),
                    child: Icon(
                      Icons.remove,
                      size: 16, //20,
                      //  color: Theme.of(context).primaryColor,
                      color: Colors.black,
                    ),
                  ),
                )
              // ? IconButton(
              //     onPressed: (){},
              //     icon: const Icon(
              //       Icons.remove,
              //       size: 16, //20,
              //       //  color: Theme.of(context).primaryColor,
              //       color: Colors.black,
              //     ),
              //   )
              : InkWell(
                  onTap: decreaseQuantity,
                  child: const Padding(
                    padding: EdgeInsets.symmetric(horizontal: 12.0,vertical: 4.0),
                    child: Icon(
                      Icons.remove,
                      size: 16, //20,
                      //  color: Theme.of(context).primaryColor,
                      color: Colors.black,
                    ),
                  ),
                ),
          //   child: IconButton(
          //           onPressed: decreaseQuantity,
          //           icon: const Icon(
          //             Icons.remove,
          //             size: 16, //20,
          //             //  color: Theme.of(context).primaryColor,
          //             color: Colors.black,
          //           ),
          //         ),
          Text('$_quantity'),
          InkWell(
            onTap: increaseQuantity,
            child: const Padding(
              padding: EdgeInsets.symmetric(horizontal: 12.0,vertical: 4.0),
              child: Icon(
                Icons.add,
                size: 16, //20,
                //  color: Theme.of(context).primaryColor,
                color: Colors.black,
              ),
            ),
          ),
          // IconButton(
          //   onPressed: increaseQuantity,
          //   icon: const Icon(
          //     Icons.add,
          //     size: 16, //20,
          //     //color: Theme.of(context).primaryColor,
          //     color: Colors.black,
          //   ),
          // ),
        ],
      ),
    );
  }

  Widget buildAddButton() {
    return GestureDetector(
      onTap: () {
        _focusNode.requestFocus();
        increaseQuantity();
      },
      child: Container(
        margin: const EdgeInsets.only(bottom: 6, right: 6),
        height: 30,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          color: const Color.fromRGBO(
              255, 207, 66, 1), //const Color.fromRGBO(248, 149, 31,1),
          border: Border.all(
            color: Colors.transparent,
          ),
          borderRadius: BorderRadius.circular(widget.borderRadiusValue),
        ),
        child: const Center(
            child: Text(
          'Add',
          style: TextStyle(fontSize: 12),
        )),
      ),
    );
  }

  Widget buildAddButton1() {
    return ElevatedButton(
      onPressed: () {
        _focusNode.requestFocus();
        increaseQuantity();
      },
      style: ElevatedButton.styleFrom(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(widget.borderRadiusValue),
        ),
        // minimumSize: const Size.square(34),
        minimumSize: const Size(68, 24),
        padding: EdgeInsets.zero,
      ),
      // child: const Icon(
      //   Icons.add,
      //   size: 20,
      //   color: Colors.white,
      // ),
      child: const Text(
        'Add',
        style: TextStyle(fontSize: 12),
      ),
    );
  }

  Widget buildQuantity() {
    return OutlinedButton(
      onPressed: _focusNode.requestFocus,
      style: ElevatedButton.styleFrom(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(widget.borderRadiusValue),
        ),
        primary: Theme.of(context).backgroundColor,
        side: BorderSide(color: Theme.of(context).primaryColor),
        // minimumSize: const Size.square(28),
        minimumSize: const Size(40, 24),
      ),
      child: Text(
        '$_quantity',
        style: TextStyle(
          color: Theme.of(context).primaryColor,
          fontWeight: FontWeight.w600,
        ),
      ),
    );
  }

  void increaseQuantity() {
    widget.increaseQuantityFunction();
    WidgetsBinding.instance?.addPostFrameCallback((timeStamp) {
      if (_quantity == 0) {
        hideQuantity();
      }
    });
  }

  void decreaseQuantity() {
    widget.decreaseQuantityFunction();
  }

  void showQuantity() {
    setState(() {
      _isShowQuantity = true;
    });
  }

  void hideQuantity() {
    setState(() {
      _isShowQuantity = false;
    });
  }
}
