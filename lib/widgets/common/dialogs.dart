// import 'package:connectivity/connectivity.dart';
// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:inspireui/utils/logs.dart';

// import '../../common/theme/colors.dart';
// import '../../generated/l10n.dart';

// //code added for internet connectivity check on 13-7-2023 by natasha
// bool isLoading = false;
// Future<bool> checkInternetConnection() async {
//   printLog('in check internet connection');
//   var connectivityResult = await (Connectivity().checkConnectivity());
//   return connectivityResult != ConnectivityResult.none;
// }
// //code ended on 13-7-2023

// //code added for internet connectivity check on 13-7-2023 by natasha
// Future<dynamic> showDialogNotInternet(BuildContext context) {
//   return showDialog(
//     barrierDismissible: true,
//     context: context,
//     builder: (context) => WillPopScope(
//         onWillPop: () async {
//           return Future.value(false);
//         },
//         // builder: (subContext) {
//         child: CupertinoAlertDialog(
//           title: Center(
//             child: Row(
//               children: <Widget>[
//                 const Icon(
//                   Icons.warning,
//                 ),
//                 Text(S.of(context).noInternetConnection),
//               ],
//             ),
//           ),
//           content: Column(
//             children: [
//               Padding(
//                 padding: const EdgeInsets.all(5.0),
//                 child: Text(S.of(context).pleaseCheckInternet),
//               ),
//               const SizedBox(
//                 height: 10,
//               ),
//               ElevatedButton(
//                 style: ElevatedButton.styleFrom(
//                   elevation: 0,
//                   onPrimary: Colors.white,
//                   primary: Colors.grey,
//                 ),
//                 onPressed: () async {
//                   isLoading = true; // Show the loader

//                   //code added for internet connectivity check on 13-7-2023 by natasha
//                   var isConnected = await checkInternetConnection();
//                   isLoading = false;
//                   if (isConnected) {
//                     // Internet connection is available
//                     printLog('Connected to the internet');
//                     Navigator.pop(context);
//                   } else {
//                     // No internet connection
//                     print('retry');
//                     printLog('No internet connection');
//                   }
//                 },
//                 child: isLoading
//                     ? const CircularProgressIndicator()
//                     : const Text(
//                         'Retry',
//                         style: TextStyle(
//                           fontSize: 12,
//                           fontWeight: FontWeight.w600,
//                           color: kDarkBG,
//                           //  Theme.of(context).colorScheme.secondary,
//                         ),
//                       ),
//               ),
//             ],
//           ),
//         )),
//   );
// }

import 'package:connectivity/connectivity.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:inspireui/utils/logs.dart';

import '../../common/theme/colors.dart';
import '../../generated/l10n.dart';

class InternetDialog extends StatefulWidget {
  @override
  _InternetDialogState createState() => _InternetDialogState();
}

class _InternetDialogState extends State<InternetDialog> {
  bool isLoading = false;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return Future.value(false);
      },
      child: CupertinoAlertDialog(
        title: Center(
          child: Row(
            children: <Widget>[
              const Icon(
                Icons.warning,
              ),
              Text(S.of(context).noInternetConnection),
            ],
          ),
        ),
        content: Column(
          children: [
            const Padding(
              padding: EdgeInsets.all(5.0),
              child: Text('Please check your internet connection!'),//msg change by natasha on 27-7-2023
            ),
            const SizedBox(
              height: 10,
            ),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                elevation: 0,
                onPrimary: Colors.white,
                primary: Colors.grey,
              ),
              onPressed: () async {
                setState(() {
                  isLoading = true;
                });
                await Future.delayed(const Duration(seconds: 2));
                var isConnected = await checkInternetConnection();

                setState(() {
                  isLoading = false;
                });

                if (isConnected) {
                  // Internet connection is available
                  printLog('Connected to the internet');
                  Navigator.pop(context);
                } else {
                  // No internet connection
                  print('retry');
                  printLog('No internet connection');
                }
              },
              child: isLoading
                  ? const Padding(
                      padding: EdgeInsets.only(top: 2, bottom: 2),
                      child: CircularProgressIndicator(),
                    )
                  : const Text(
                      'Retry',
                      style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w600,
                        color: kDarkBG,
                        //  Theme.of(context).colorScheme.secondary,
                      ),
                    ),
            ),
          ],
        ),
      ),
    );
  }
}

// code added for internet connectivity check on 13-7-2023 by natasha
Future<bool> checkInternetConnection() async {
  printLog('in check internet connection');
  var connectivityResult = await (Connectivity().checkConnectivity());
  return connectivityResult != ConnectivityResult.none;
}
// code ended on 13-7-2023

Future<dynamic> showDialogNotInternet(BuildContext context) {
  return showDialog(
    barrierDismissible: true,
    context: context,
    builder: (context) => InternetDialog(),
  );
}
