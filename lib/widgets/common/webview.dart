// import 'package:flutter/material.dart';
// import 'package:flutter_inappwebview/flutter_inappwebview.dart';
// import 'package:url_launcher/url_launcher.dart';
// import 'package:webview_flutter/webview_flutter.dart' as flutter;
// import 'package:webview_flutter/webview_flutter.dart';

// import '../../common/config.dart';
// import '../../common/constants.dart';
// import '../../common/tools.dart';
// import '../../generated/l10n.dart';
// import '../html/index.dart';
// import 'webview_inapp.dart';

// class WebView extends StatefulWidget {
//   final String? url;
//   final String? title;
//   final AppBar? appBar;
//   final bool enableForward;

//   const WebView(
//       {Key? key,
//       this.title,
//       required this.url,
//       this.appBar,
//       this.enableForward = false})
//       : super(key: key);

//   @override
//   _WebViewState createState() => _WebViewState();
// }

// class _WebViewState extends State<WebView> {
//   bool isLoading = true;
//   String html = '';
//   late flutter.WebViewController _controller;
//   InAppWebViewController? webViewController;

//   @override
//   void initState() {
//     if (isMacOS || isWindow) {
//       httpGet(widget.url.toString().toUri()!).then((response) {
//         setState(() {
//           html = response.body;
//         });
//       });
//     }

//     if (isAndroid) flutter.WebView.platform = flutter.SurfaceAndroidWebView();

//     super.initState();
//   }

//   Future<NavigationDecision> getNavigationDelegate(
//       NavigationRequest request) async {
//     printLog('[WebView] navigate to ${request.url}');

//     /// open the normal web link
//     var isHttp = 'http';
//     if (request.url.contains(isHttp)) {
//       return NavigationDecision.navigate;
//     }

//     /// open external app link
//     if (await canLaunch(request.url)) {
//       await launch(request.url);
//     }

//     if (!request.isForMainFrame) {
//       return NavigationDecision.prevent;
//     }

//     return NavigationDecision.prevent;
//   }

//   @override
//   Widget build(BuildContext context) {
//     if (isMacOS || isWindow) {
//       return Scaffold(
//         appBar: widget.appBar ??
//             AppBar(
//               backgroundColor: Theme.of(context).backgroundColor,
//               elevation: 0.0,
//               title: Text(
//                 widget.title ?? '',
//                 style: Theme.of(context)
//                     .textTheme
//                     .headline6!
//                     .copyWith(fontWeight: FontWeight.w600),
//               ),
//             ),
//         body: SingleChildScrollView(
//           child: HtmlWidget(html),
//         ),
//       );
//     }

//     /// is Mobile or Web
//     if (!kIsWeb && (kAdvanceConfig['inAppWebView'] ?? false)) {
//       return WebViewInApp(url: widget.url!, title: widget.title);
//     }

//     return Scaffold(
//       appBar: widget.appBar ??
//           AppBar(
//             backgroundColor: Theme.of(context).backgroundColor,
//             elevation: 0.0,
//             title: Text(
//               widget.title ?? '',
//               style: Theme.of(context)
//                   .textTheme
//                   .headline6!
//                   .copyWith(fontWeight: FontWeight.w600),
//             ),
//             leadingWidth: 150,
//             leading: Builder(builder: (buildContext) {
//               return Row(
//                 children: [
//                   IconButton(
//                     icon: const Icon(Icons.arrow_back_ios, size: 20),
//                     onPressed: () async {
//                       var value = await _controller.canGoBack();
//                       if (value) {
//                         await _controller.goBack();
//                       } else if (Navigator.canPop(context)) {
//                         Navigator.of(context).pop();
//                       } else {
//                         Tools.showSnackBar(Scaffold.of(buildContext),
//                             S.of(context).noBackHistoryItem);
//                       }
//                     },
//                   ),
//                   if (widget.enableForward)
//                     IconButton(
//                       onPressed: () async {
//                         if (await _controller.canGoForward()) {
//                           await _controller.goForward();
//                         } else {
//                           Tools.showSnackBar(Scaffold.of(buildContext),
//                               S.of(context).noForwardHistoryItem);
//                         }
//                       },
//                       icon: const Icon(Icons.arrow_forward_ios, size: 20),
//                     ),
//                 ],
//               );
//             }),
//           ),
//       body: Builder(builder: (BuildContext context) {
//         return flutter.WebView(
//           initialUrl: widget.url!,
//           javascriptMode: flutter.JavascriptMode.unrestricted,
//           onPageFinished: (_) {
//             /// Demo the Javascript Style override
//             // var script =
//             //     "document.querySelector('body > div.wd-toolbar.wd-toolbar-label-show').style.display = 'none'";
//             // _controller.runJavascript(script);
//             //code added on 8-5-2023 for removing web header and bottom bar by natasha
//             // _controller.runJavascript("document.querySelector('.header-mobile-nav').style.display='none'");
//             //
//             // _controller.runJavascript("document.querySelector('.header-buttons hide-desktop').style.display='none'");
//             // code ended 0n 8-5-2023
//           },
//           navigationDelegate: getNavigationDelegate,
//           onWebViewCreated: (webViewController) {
//             _controller = webViewController;
//           },
//           gestureNavigationEnabled: true,
//         );
//       }),
//       // code added 0n 8-5-2023 for removing web header and bottom bar by natasha
//       // body: InAppWebView(
//       //   initialUrlRequest: URLRequest(url: Uri.parse(widget.url!)),
//       //   onWebViewCreated: (controller) {
//       //     webViewController = controller;
//       //   },
//       //   onLoadStop: (controller, url) {
//       //     if (webViewController != null) {
//       //       // Execute your JavaScript code here
//       //       webViewController!.evaluateJavascript(source: '''
//       //         var element = document.getElementsByClassName('.header-mobile-nav');
//       //         if (element) {
//       //           element.style.display = 'none';
//       //         }
//       //       ''');
//       //     }
//       //   },
//       // ),
//       // code ended 0n 8-5-2023
//     );
//   }
// }

// ignore_for_file: deprecated_member_use

import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:webview_flutter/webview_flutter.dart' as flutter;
import 'package:webview_flutter/webview_flutter.dart';

import '../../common/config.dart';
import '../../common/constants.dart';
import '../../common/tools.dart';
import '../../generated/l10n.dart';
import '../html/index.dart';
import 'webview_inapp.dart';

class WebView extends StatefulWidget {
  final String? url;
  final String? title;
  final AppBar? appBar;
  final bool enableForward;

  const WebView(
      {Key? key,
      this.title,
      required this.url,
      this.appBar,
      this.enableForward = false})
      : super(key: key);

  @override
  _WebViewState createState() => _WebViewState();
}

class _WebViewState extends State<WebView> {
  bool isLoading = true;
  String html = '';
  flutter.WebViewController? _controller;
  final bool _isNavBarVisible = true;

  @override
  void initState() {
    if (isMacOS || isWindow) {
      httpGet(widget.url.toString().toUri()!).then((response) {
        setState(() {
          html = response.body;
        });
      });
    }

    if (isAndroid) flutter.WebView.platform = flutter.SurfaceAndroidWebView();

    super.initState();
  }

  Future<NavigationDecision> getNavigationDelegate(
      NavigationRequest request) async {
    printLog('[WebView] navigate to ${request.url}');

    /// open the normal web link
    var isHttp = 'http';
    if (request.url.contains(isHttp)) {
      return NavigationDecision.navigate;
    }

    /// open external app link
    if (await canLaunch(request.url)) {
      await launch(request.url);
    }

    if (!request.isForMainFrame) {
      return NavigationDecision.prevent;
    }

    return NavigationDecision.prevent;
  }

  // @override
  // Widget build(BuildContext context) {
  //   if (isMacOS || isWindow) {
  //     return Scaffold(
  //       appBar: widget.appBar ??
  //           AppBar(
  //             backgroundColor: Theme.of(context).backgroundColor,
  //             elevation: 0.0,
  //             title: Text(
  //               widget.title ?? '',
  //               style: Theme.of(context)
  //                   .textTheme
  //                   .headline6!
  //                   .copyWith(fontWeight: FontWeight.w600),
  //             ),
  //           ),
  //       body: SingleChildScrollView(
  //         child: HtmlWidget(html),
  //       ),
  //     );
  //   }

  //   /// is Mobile or Web
  //   if (!kIsWeb && (kAdvanceConfig['inAppWebView'] ?? false)) {
  //     return WebViewInApp(url: widget.url!, title: widget.title);
  //   }

  //   return Scaffold(
  //     appBar: widget.appBar ??
  //         AppBar(
  //           backgroundColor: Theme.of(context).backgroundColor,
  //           elevation: 0.0,
  //           title: Text(
  //             widget.title ?? '',
  //             style: Theme.of(context)
  //                 .textTheme
  //                 .headline6!
  //                 .copyWith(fontWeight: FontWeight.w600),
  //           ),
  //           leadingWidth: 150,
  //           leading: Builder(builder: (buildContext) {
  //             return Row(
  //               children: [
  //                 IconButton(
  //                   icon: const Icon(Icons.arrow_back_ios, size: 20),
  //                   onPressed: () async {
  //                     var value = await _controller!.canGoBack();
  //                     if (value) {
  //                       await _controller!.goBack();
  //                     } else if (Navigator.canPop(context)) {
  //                       Navigator.of(context).pop();
  //                     } else {
  //                       Tools.showSnackBar(Scaffold.of(buildContext),
  //                           S.of(context).noBackHistoryItem);
  //                     }
  //                   },
  //                 ),
  //                 if (widget.enableForward)
  //                   IconButton(
  //                     onPressed: () async {
  //                       if (await _controller!.canGoForward()) {
  //                         await _controller!.goForward();
  //                       } else {
  //                         Tools.showSnackBar(Scaffold.of(buildContext),
  //                             S.of(context).noForwardHistoryItem);
  //                       }
  //                     },
  //                     icon: const Icon(Icons.arrow_forward_ios, size: 20),
  //                   ),
  //               ],
  //             );
  //           }),
  //         ),
  //     // body: Builder(builder: (BuildContext context) {
  //     //   return flutter.WebView(
  //     //     initialUrl: widget.url!,
  //     //     javascriptMode: flutter.JavascriptMode.unrestricted,
  //     //     navigationDelegate: getNavigationDelegate,
  //     //     onWebViewCreated: (webViewController) {
  //     //       _controller = webViewController;
  //     //     },
  //     //     onPageFinished: (url) {
  //     //       printLog('onPageFinished');
  //     //       printLog(_isNavBarVisible);
  //     //       if (_isNavBarVisible) {
  //     //         hideNavBar();
  //     //       }
  //     //     },
  //     //     // gestureNavigationEnabled: true,
  //     //   );
  //     // }),
  //     body: Column(
  //       children: [
  //         Expanded(
  //           child: flutter.WebView(
  //             initialUrl: widget.url,
  //             onWebViewCreated: (controller) {
  //               _controller = controller;
  //             },
  //             javascriptMode: JavascriptMode.unrestricted,
  //             onPageFinished: (url) {
  //               printLog('onPageFinished');
  //               printLog(_isNavBarVisible);
  //               if (_isNavBarVisible) {
  //                 hideNavBar();
  //               }
  //             },
  //           ),
  //         )
  //       ],
  //     ),
  //   );
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   title: const Text('WebView'),
      // ),
      appBar: widget.appBar ??
          AppBar(
           // centerTitle: true,
            backgroundColor: Theme.of(context).backgroundColor,
            elevation: 0.0,
            title: Text(
              widget.title ?? '',
              style: Theme.of(context)
                  .textTheme
                  .headline6!
                  .copyWith(fontWeight: FontWeight.w600),
            ),
            //leadingWidth: 150,
            leading: Builder(builder: (buildContext) {
              return Row(
                children: [
                  IconButton(
                    icon: Icon(isIos ? Icons.arrow_back_ios : Icons.arrow_back, size: 20),
                    onPressed: () async {
                      var value = await _controller!.canGoBack();
                      if (value) {
                        await _controller!.goBack();
                      } else if (Navigator.canPop(context)) {
                        Navigator.of(context).pop();
                      } else {
                        Tools.showSnackBar(Scaffold.of(buildContext),
                            S.of(context).noBackHistoryItem);
                      }
                    },
                  ),
                  if (widget.enableForward)
                    IconButton(
                      onPressed: () async {
                        if (await _controller!.canGoForward()) {
                          await _controller!.goForward();
                        } else {
                          Tools.showSnackBar(Scaffold.of(buildContext),
                              S.of(context).noForwardHistoryItem);
                        }
                      },
                      icon: const Icon(Icons.arrow_forward_ios, size: 20),
                    ),
                ],
              );
            }),
          ),
      body: Column(
        children: [
          Expanded(
            child: flutter.WebView(
              initialUrl: widget.url,
              onWebViewCreated: (controller) {
                _controller = controller;
              },
              javascriptMode: JavascriptMode.unrestricted,
              onPageFinished: (url) {
                if (_isNavBarVisible) {
                  hideNavBar();
                }
              },
            ),
          ),
          // RaisedButton(
          //   child: Text(_isNavBarVisible ? 'Hide Nav Bar' : 'Show Nav Bar'),
          //   onPressed: toggleNavBarVisibility,
          // ),
        ],
      ),
    );
  }

  //.site-header.mobile-nav-enable
  void hideNavBar() async {
    await _controller?.evaluateJavascript(
      // 'document.querySelector(".header-mobile-nav").style.display = "none";',
      'document.querySelector(".site-header.mobile-nav-enable").style.display = "none";',
    );
  }
}
