import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';

import '../../generated/l10n.dart';

class InternetLossPage extends StatefulWidget {
  //final FlutterErrorDetails? errorDetails;

 // const InternetLossPage(this.errorDetails);
  const InternetLossPage({Key? key}) : super(key: key);

  @override
  State<InternetLossPage> createState() => _InternetLossPageState();
}

class _InternetLossPageState extends State<InternetLossPage> {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
      const SizedBox(
      height: 220,
      child: FlareActor(
        'assets/images/no_internet.flr',
        animation: 'Untitled',
      ),
    ),
    Padding(
    padding: const EdgeInsets.only(top: 24.0),
    child: Text(
    S.of(context).noInternetConnection,
    style: Theme.of(context).textTheme.headline5,
    ),
    ),
      ]);
  }
}
