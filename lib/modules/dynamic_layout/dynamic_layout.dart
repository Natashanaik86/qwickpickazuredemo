import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';

import '../../app.dart';
import '../../common/constants.dart';
import '../../common/tools.dart';
import '../../models/index.dart';
import '../../routes/flux_navigate.dart';
import '../../screens/cart/cart_screen.dart';
import '../../services/index.dart';
import '../../widgets/common/dialogs.dart';
import 'banner/banner_animate_items.dart';
import 'banner/banner_group_items.dart';
import 'banner/banner_slider.dart';
import 'blog/blog_grid.dart';
import 'brand/brand_layout.dart';
import 'button/button.dart';
import 'category/category_icon.dart';
import 'category/category_image.dart';
import 'category/category_menu_with_products.dart';
import 'category/category_text.dart';
import 'config/brand_config.dart';
import 'config/index.dart';
import 'divider/divider.dart';
import 'header/header_search.dart';
import 'header/header_text.dart';
import 'instagram_story/instagram_story.dart';
import 'logo/logo.dart';
import 'product/product_list_simple.dart';
import 'product/product_recent_placeholder.dart';
import 'slider_testimonial/index.dart';
import 'spacer/spacer.dart';
import 'story/index.dart';
import 'testimonial/index.dart';
import 'video/index.dart';

class DynamicLayout extends StatelessWidget {
  final config;
  final bool cleanCache;

  const DynamicLayout(
      {this.config,
      this.cleanCache = false}); //change false to true on 25-5-2023

  @override
  Widget build(BuildContext context) {
    final appModel = Provider.of<AppModel>(context, listen: true);

    switch (config['layout']) {
      case 'logo':
        final themeConfig = appModel.themeConfig;
        return Logo(
          config: LogoConfig.fromJson(config),
          logo: themeConfig.logo,
          totalCart:
              Provider.of<CartModel>(context, listen: true).totalCartQuantity,
          notificationCount:
              Provider.of<NotificationModel>(context).unreadCount,
          key: config['key'] != null ? Key(config['key']) : UniqueKey(),
          onSearch: () => Navigator.of(context).pushNamed(RouteList.homeSearch),
          onCheckout: () {
            Navigator.push(
              context,
              MaterialPageRoute<void>(
                builder: (BuildContext context) => Scaffold(
                  backgroundColor: Theme.of(context).backgroundColor,
                  body: const CartScreen(isModal: true),
                ),
                fullscreenDialog: true,
              ),
            );
          },
          onTapNotifications: () {
            Navigator.of(context).pushNamed(RouteList.notify);
          },
          onTapDrawerMenu: () => NavigateTools.onTapOpenDrawerMenu(context),
        );

      case 'header_text':
        return HeaderText(
          config: HeaderConfig.fromJson(config),
          onSearch: () => Navigator.of(context).pushNamed(RouteList.homeSearch),
          key: config['key'] != null ? Key(config['key']) : UniqueKey(),
        );

      case 'header_search':
        return HeaderSearch(
          config: HeaderConfig.fromJson(config),
          onSearch: () {
            Navigator.of(App.fluxStoreNavigatorKey.currentContext!)
                .pushNamed(RouteList.homeSearch);
          },
          key: config['key'] != null ? Key(config['key']) : UniqueKey(),
        );
      case 'featuredVendors':
        return Services().widget.renderFeatureVendor(config);
      case 'category':
        if (config['type'] == 'image') {
          return CategoryImages(
            config: CategoryConfig.fromJson(config),
            key: config['key'] != null ? Key(config['key']) : UniqueKey(),
          );
        }
        return Consumer<CategoryModel>(builder: (context, model, child) {
          var _config = CategoryConfig.fromJson(config);
          var _listCategoryName =
              model.categoryList.map((key, value) => MapEntry(key, value.name));
          void _onShowProductList(CategoryItemConfig item) {
            FluxNavigate.pushNamed(
              RouteList.backdrop,
              arguments: BackDropArguments(
                config: item.toJson(),
                data: item.data,
              ),
            );
          }

          if (config['type'] == 'menuWithProducts') {
            return CategoryMenuWithProducts(
              config: _config,
              listCategoryName: _listCategoryName,
              onShowProductList: _onShowProductList,
              key: config['key'] != null ? Key(config['key']) : UniqueKey(),
            );
          }

          if (config['type'] == 'text') {
            return CategoryTexts(
              config: _config,
              listCategoryName: _listCategoryName,
              onShowProductList: _onShowProductList,
              key: config['key'] != null ? Key(config['key']) : UniqueKey(),
            );
          }

          return CategoryIcons(
            config: _config,
            listCategoryName: _listCategoryName,
            onShowProductList: _onShowProductList,
            key: config['key'] != null ? Key(config['key']) : UniqueKey(),
          );
        });
      case 'bannerAnimated':
        if (kIsWeb) return const SizedBox();
        return BannerAnimated(
          config: BannerConfig.fromJson(config),
          key: config['key'] != null ? Key(config['key']) : UniqueKey(),
        );

      case 'bannerImage':
        if (config['isSlider'] == true) {
          // return BannerSlider(
          //     config: BannerConfig.fromJson(config),
          //     onTap: (itemConfig) {
          //       NavigateTools.onTapNavigateOptions(
          //         context: context,
          //         config: itemConfig,
          //       );
          //     },
          //     key: config['key'] != null ? Key(config['key']) : UniqueKey());
          return FutureBuilder<String>(
            future: callBannerData(),
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return const Center(
                  child: CircularProgressIndicator(),
                );
              } else if (snapshot.hasError) {
                return Container(); //Text('Error: ${snapshot.error}'); //11-6-2023 hide socket exception issue for network issue
              } else {
                var bannerResponse = snapshot.data;
                var decodedResponse = json.decode(bannerResponse!);
                printLog('RESPONSE VARIABLE IS');
                printLog('CONFIG0 is');
                printLog(config['isSlider']);
                printLog(decodedResponse['isSlider']);
                printLog('CHECKING');
                return BannerSlider(
                    config: BannerConfig.fromJson(decodedResponse),
                    onTap: (itemConfig) async {
                      //code added for internet connectivity check on 14-7-2023 by natasha
                      var isConnected = await checkInternetConnection();
                      if (isConnected) {
                        // Internet connection is available
                        await NavigateTools.onTapNavigateOptions(
                          context: context,
                          config: itemConfig,
                        );
                      } else {
                        // No internet connection
                        printLog('No internet connection');
                        await showDialogNotInternet(context);
                      }
                      // NavigateTools.onTapNavigateOptions(
                      //   context: context,
                      //   config: itemConfig,
                      // );
                    },
                    key: config['key'] != null
                        ? Key(config['key'])
                        : UniqueKey());
              }
            },
          );
        }

        return BannerGroupItems(
          config: BannerConfig.fromJson(config),
          onTap: (itemConfig) {
            NavigateTools.onTapNavigateOptions(
              context: context,
              config: itemConfig,
            );
          },
          key: config['key'] != null ? Key(config['key']) : UniqueKey(),
        );

      case 'blog':
        return BlogGrid(
          config: BlogConfig.fromJson(config),
          key: config['key'] != null ? Key(config['key']) : UniqueKey(),
        );

      case 'video':
        return VideoLayout(
          config: config,
          key: config['key'] != null ? Key(config['key']) : UniqueKey(),
        );

      case 'story':
        return StoryWidget(
          config: config,
          onTapStoryText: (cfg) {
            NavigateTools.onTapNavigateOptions(context: context, config: cfg);
          },
        );
      case 'recentView':
        if (Config().isBuilder) {
          return ProductRecentPlaceholder();
        }
        return Services().widget.renderHorizontalListItem(config);
      case 'fourColumn':
      case 'threeColumn':
      case 'twoColumn':
      case 'staggered':
      case 'saleOff':
      case 'card':
      case 'listTile':
        printLog(' top pick data $config');
        return Services()
            .widget
            .renderHorizontalListItem(config, cleanCache: cleanCache);

      /// New product layout style.
      case 'largeCardHorizontalListItems':
      case 'largeCard':
        return Services().widget.renderLargeCardHorizontalListItems(config);
      case 'simpleVerticalListItems':
      case 'simpleList':
        return SimpleVerticalProductList(
          config: ProductConfig.fromJson(config),
          key: config['key'] != null ? Key(config['key']) : UniqueKey(),
        );

      case 'brand':
        return BrandLayout(
          config: BrandConfig.fromJson(config),
        );

      /// FluxNews
      case 'sliderList':
        return Services().widget.renderSliderList(config);
      case 'sliderItem':
        return Services().widget.renderSliderItem(config);

      case 'geoSearch':
        return Services().widget.renderGeoSearch(config);
      case 'divider':
        return DividerLayout(
          config: DividerConfig.fromJson(config),
          key: config['key'] != null ? Key(config['key']) : UniqueKey(),
        );
      case 'spacer':
        return SpacerLayout(
          config: SpacerConfig.fromJson(config),
          key: config['key'] != null ? Key(config['key']) : UniqueKey(),
        );
      case 'button':
        return ButtonLayout(
          config: ButtonConfig.fromJson(config),
          key: config['key'] != null ? Key(config['key']) : UniqueKey(),
        );
      case 'testimonial':
        return TestimonialLayout(
          config: TestimonialConfig.fromJson(config),
          key: config['key'] != null ? Key(config['key']) : UniqueKey(),
        );
      case 'sliderTestimonial':
        return SliderTestimonial(
          config: SliderTestimonialConfig.fromJson(config),
          key: config['key'] != null ? Key(config['key']) : UniqueKey(),
        );
      case 'instagramStory':
        return InstagramStory(
          config: InstagramStoryConfig.fromJson(config),
          key: config['key'] != null ? Key(config['key']) : UniqueKey(),
        );
      default:
        return const SizedBox();
    }
  }

  Future<String> callBannerData() async {
    printLog('calling banner data');

    var url = '$baseUrl/wp-json/wc/v2/flutter/get_banner_slider';
    // 'https://qwickpick.com/wp-json/wc/v2/flutter/get_banner_slider';
    final response = await http.get(Uri.parse(url));

    printLog('RESP CODE IS ${response.statusCode}');
    printLog('RESP BODY IS');
    printLog(response.body);

    return response.body;
  }
}
