

import 'package:flutter/foundation.dart';

class RazorPayStatus extends ChangeNotifier {
  bool _razorpayStatus = false;

  bool get razorPayStatus => _razorpayStatus;

  void updateVariable(bool newValue) {
    _razorpayStatus = newValue;
    notifyListeners();
  }
}
// For updating payment status // 04072023