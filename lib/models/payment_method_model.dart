import 'dart:io';

import 'package:flutter/material.dart';
import 'package:inspireui/utils/logs.dart';

import '../models/cart/cart_model.dart';
import '../services/index.dart';
import 'entities/payment_method.dart';
import 'entities/shipping_method.dart';

class PaymentMethodModel extends ChangeNotifier {
  final Services _service = Services();
  late List<PaymentMethod> paymentMethods;
  bool isLoading = true;
  String? message;

  Future<void> getPaymentMethods(
      {CartModel? cartModel,
        ShippingMethod? shippingMethod,
        String? token}) async {
    try {
      printLog('product success for method');
      paymentMethods = await _service.api.getPaymentMethods(
        cartModel: cartModel,
        shippingMethod: shippingMethod,
        token: token,
      )!;
      isLoading = false;
      message = null;
      notifyListeners();
    } catch (err) {
      isLoading = false;
      printLog('product error');
      printLog(err.toString());
      if (err is SocketException) {
        message = 'Internet connection lost. Please check your network settings and try again.';
      }  else {
        message = err.toString();
      }
      // message = err.toString(); //3-7-2023 removing exception word from error
      //  'There is an issue with the app during request the data, please contact admin for fixing the issues ' +
      // 'Please check '+
      // ''+
      notifyListeners();
    }
  }
}
