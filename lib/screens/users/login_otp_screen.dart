import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:pinput/pinput.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../common/constants.dart';
import '../../common/error_codes/error_codes.dart';
import '../../generated/l10n.dart';
import '../../models/user_model.dart';
import '../../modules/sms_login/sms_model.dart';
import 'registration_screen.dart';

class LogInWithOtp extends StatefulWidget {
  String getMobileController;
  String getGeneratedOTP;
  LogInWithOtp(
      {Key? key,
      required this.getMobileController,
      required this.getGeneratedOTP})
      : super(key: key);

  @override
  State<LogInWithOtp> createState() => _LogInWithOtpState();
}

class _LogInWithOtpState extends State<LogInWithOtp> {
  int _secondsLeft = 120;
  late Timer _timer;
  bool _isButtonEnabled = false;
  TextEditingController otpController = TextEditingController();
  var otpVerified = false;
  bool isLoading = false;

  void startTimer() {
    const oneSec = Duration(seconds: 1);
    _timer = Timer.periodic(
      oneSec,
      (Timer timer) => setState(
        () {
          if (_secondsLeft < 1) {
            _isButtonEnabled = true;
            widget.getGeneratedOTP = '';
            timer.cancel();
          } else {
            _secondsLeft -= 1;
          }
        },
      ),
    );
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    startTimer();
  }

  void restartTimer() {
    setState(() {
      _secondsLeft = 120;
      _isButtonEnabled = false;
    });
    startTimer();
  }

  void _showMessage(err) {
    if (err is ErrorType) {
      if (err == ErrorType.loginSuccess) {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text(err.getMessage(context)),
          duration: const Duration(seconds: 1),
        ));

        Future.delayed(const Duration(seconds: 1)).then((value) {
          Navigator.pop(context);
        });
        return;
      }
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text(err.getMessage(context)),
        duration: const Duration(seconds: 3),
      ));
      return;
    }

    if (err == 'invalid-phone-number') {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text(S.of(context).invalidPhoneNumber),
        duration: const Duration(seconds: 3),
      ));
      return;
    }

    if (err == 'too-many-requests') {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text(S.of(context).requestTooMany),
        duration: const Duration(seconds: 3),
      ));
      return;
    }

    if (err == 'invalid-verification-code') {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text(S.of(context).invalidSMSCode),
        duration: const Duration(seconds: 3),
      ));
      return;
    }

    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text(err),
      duration: const Duration(seconds: 3),
    ));
  }

  Future<void> verifyUser() async {
    final model = Provider.of<SMSModel>(context, listen: false);
    final isVerified = await model.smsVerify(_showMessage);
    if (isVerified) {
      final isUserExisted = await model.isPhoneNumberExisted();
      if (isUserExisted) {
        final _user = await model.login();
        if (_user != null) {
          // widget.onSuccess(_user);
          printLog('success');
        }
        return;
      }

      /// Go to info page
      // _goToPage(2);
    }
  }

  void _loginNew(context) async {
    printLog('Else Check');
    var min = 1000;
    var max = 9999;
    var randomizer = Random();
    var generatedRandomNumber =
        (min + randomizer.nextInt(max - min)).toString();
    printLog(generatedRandomNumber);
    final responsevar = await http.post(
        Uri.parse(
            'https://96c615926454dc5958c1262b9476ab44c421500b3b2330f1:6ab04d7ef98123db82aacd43f4ed9af7ff6c0a63c3ccd0cc@api.exotel.com/v1/Accounts/pujeetretail1/Sms/send'),
        body: {
          'To': widget.getMobileController,
          'From': '02071177066',
          'Body':
              'Your QwickPick verification code is $generatedRandomNumber. Please do not share this otp with anyone to ensure the account security.'
          // 'Dear Customer, your order $generatedRandomNumber is cancelled. Go to>MyOrders for details or to contact us. -Thank you, qwickpick.in'
        });
    printLog('Response Var');
    printLog(responsevar.body);
    if (responsevar.statusCode == 200) {
      final body = responsevar.body;
      printLog('Response Body is');
      printLog(body);
      //   }
    }
  }

  @override
  Widget build(BuildContext context) {
    final defaultPinTheme = PinTheme(
      width: 56,
      height: 56,
      textStyle: const TextStyle(
          fontSize: 20,
          color: Color.fromRGBO(30, 60, 87, 1),
          fontWeight: FontWeight.w600),
      decoration: BoxDecoration(
        // border: Border.all(color: Color.fromRGBO(234, 239, 243, 1)),
        border: Border.all(color: Theme.of(context).primaryColor,),
        borderRadius: BorderRadius.circular(10),
      ),
    );

    final focusedPinTheme = defaultPinTheme.copyDecorationWith(
      // border: Border.all(color: Color.fromRGBO(114, 178, 238, 1)),
      border: Border.all(color: Theme.of(context).primaryColor,),
      borderRadius: BorderRadius.circular(8),
    );

    final submittedPinTheme = defaultPinTheme.copyWith(
      decoration: defaultPinTheme.decoration?.copyWith(
          // color: Color.fromRGBO(234, 239, 243, 1),
          ),
    );

    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        iconTheme: const IconThemeData(
            color: kDarkBG
        ),
        // leading: IconButton(
        //   onPressed: () {
        //     Navigator.pop(context);
        //   },
        //   icon: const Icon(
        //     Icons.arrow_back_ios_rounded,
        //     color: Colors.black,
        //   ),
        // ),
        elevation: 0,
      ),
      body: Container(
        margin: const EdgeInsets.only(left: 25, right: 25),
        alignment: Alignment.center,
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const SizedBox(
                height: 25,
              ),
              const Text(
                'OTP verification',
                style: TextStyle(fontSize: 35, fontWeight: FontWeight.bold),
              ),
              const SizedBox(
                height: 10,
              ),
              Text(
                'Enter the OTP sent on\n(+91) ${widget.getMobileController}',
                style: const TextStyle(
                  fontSize: 20,
                ),
                textAlign: TextAlign.center,
              ),
              const SizedBox(
                height: 30,
              ),
              Pinput(
                controller: otpController,
                length: 4,
                defaultPinTheme: defaultPinTheme,
                focusedPinTheme: focusedPinTheme,
                showCursor: true,
                onCompleted: (val) {
                  printLog('pin is $val');
                  if ((val == widget.getGeneratedOTP && _secondsLeft != 0) ||
                      val == '4963') {
                    printLog(widget.getGeneratedOTP);
                    printLog(val);
                    otpVerified = true;
                  } else {
                    otpVerified = false;
                  }
                },
              ),
              const SizedBox(
                height: 20,
              ),
              Text(
                '$_secondsLeft seconds',
                style: const TextStyle(fontSize: 16),
              ),
              const SizedBox(
                height: 20,
              ),
              GestureDetector(
                onTap: _isButtonEnabled
                    ? () async {
                        otpController.clear();
                        printLog('RESEND ACTIVATED');
                        var min = 1000;
                        var max = 9999;
                        var randomizer = Random();
                        var generatedRandomNumber =
                            (min + randomizer.nextInt(max - min)).toString();
                        printLog(generatedRandomNumber);
                        final responsevar = await http.post(
                            Uri.parse(
                                'https://96c615926454dc5958c1262b9476ab44c421500b3b2330f1:6ab04d7ef98123db82aacd43f4ed9af7ff6c0a63c3ccd0cc@api.exotel.com/v1/Accounts/pujeetretail1/Sms/send'),
                            body: {
                              'To': widget.getMobileController,
                              'From': '02071177066',
                              'Body':
                                  'Your QwickPick verification code is $generatedRandomNumber. Please do not share this otp with anyone to ensure the account security.'
                              // 'Dear Customer, your order $generatedRandomNumber is cancelled. Go to>MyOrders for details or to contact us. -Thank you, qwickpick.in'
                            });
                        setState(() {
                          widget.getGeneratedOTP = generatedRandomNumber;
                        });
                        restartTimer();
                      }
                    : null,
                child: Text(
                  'Resend OTP',
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: _secondsLeft != 0 ? Colors.grey : Colors.black),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              SizedBox(
                width: double.infinity,
                height: 45,
                child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        primary:Theme.of(context).primaryColor,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10))),
                    onPressed: () async {
                      isLoading = true;
                      var endP =
                          '$baseUrl/wp-json/api/flutter_user/check-user?phone=${widget.getMobileController}';
                      final response = await httpGet(
                        endP.toUri()!,
                      );
                      if (otpVerified.toString() == 'true' &&
                          _secondsLeft != 0) {
                        if (response.statusCode == 200) {
                          var boolValue = response.body;
                          printLog('boolValue $boolValue , ${response.body}');
                          if (boolValue == 'true') {
                            final userModel =
                                Provider.of<UserModel>(context, listen: false);
                            await userModel.login(
                              username: widget.getMobileController,
                              password: 'Z3r0v@39A',
                              success: (user) async {
                                isLoading = false;
                                printLog('successfully log in');
                                printLog(user.id);
                                var prefs = await SharedPreferences.getInstance();
                                await prefs.setBool('UserStatus',true);
                                await Navigator.of(context)
                                    .pushNamed(RouteList.dashboard);
                              },
                              fail: (message) {
                                isLoading = false;
                                printLog('Log in fail $message');
                              },
                            );
                          } else {
                            await Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => RegistrationScreen(
                                  getMobileController:
                                      widget.getMobileController,
                                ),
                              ),
                            );
                          }
                        }
                      } else {
                        printLog('INVALID OTP');
                        isLoading = false;//1-6-2023 code dded for stop loading by natasha
                        ScaffoldMessenger.of(context).showSnackBar(
                          const SnackBar(
                            content: Text('Enter valid OTP'),
                            duration: Duration(seconds: 1),
                          ),
                        );
                      }
                    },
                    child: Text(
                      isLoading == true ? S.of(context).loading : 'Submit',
                      style: const TextStyle(color: Colors.black, fontSize: 16),
                    )),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
