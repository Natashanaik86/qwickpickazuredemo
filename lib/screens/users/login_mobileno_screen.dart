import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';


class LogInWithMobileNoScreen extends StatefulWidget {
  const LogInWithMobileNoScreen({Key? key}) : super(key: key);

  @override
  State<LogInWithMobileNoScreen> createState() => _LogInWithMobileNoScreenState();
}

class _LogInWithMobileNoScreenState extends State<LogInWithMobileNoScreen> {
  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Scaffold(
      backgroundColor: theme.backgroundColor,
      appBar: AppBar(
        backgroundColor: theme.backgroundColor,
        elevation: 0.0,
      ),
      body: Container(
          margin: const EdgeInsets.symmetric(horizontal: 16.0),
          child: PageView(
            physics: const NeverScrollableScrollPhysics(),
            children: [

            ],
          )),
    );
  }
}
