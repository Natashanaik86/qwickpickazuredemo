import 'dart:convert';
import 'dart:math';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:inspireui/inspireui.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:the_apple_sign_in/the_apple_sign_in.dart';

import '../../app.dart';
import '../../common/config.dart';
import '../../common/constants.dart';
import '../../common/tools.dart';
import '../../generated/l10n.dart';
import '../../models/index.dart';
import '../../modules/sms_login/sms_login.dart';
import '../../services/index.dart';
import '../../widgets/common/login_animation.dart';
import '../../widgets/common/webview.dart';
import '../base_screen.dart';
import '../index.dart';
import 'forgot_password_screen.dart';
import 'login_otp_screen.dart';

typedef LoginSocialFunction = Future<void> Function({
  required Function(User user) success,
  required Function(String) fail,
  BuildContext context,
});

typedef LoginFunction = Future<void> Function({
  required String username,
  required String password,
  required Function(User user) success,
  required Function(String) fail,
});

class LoginScreen extends StatefulWidget {
  final LoginFunction login;
  final LoginSocialFunction loginFB;
  final LoginSocialFunction loginApple;
  final LoginSocialFunction loginGoogle;
  final VoidCallback? loginSms;

  const LoginScreen({
    required this.login,
    required this.loginFB,
    required this.loginApple,
    required this.loginGoogle,
    this.loginSms,
  });

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends BaseScreen<LoginScreen>
    with TickerProviderStateMixin {
  late AnimationController _loginButtonController;
  final TextEditingController username = TextEditingController();
  final TextEditingController password = TextEditingController();
  final TextEditingController mobileController = TextEditingController();

  final usernameNode = FocusNode();
  final passwordNode = FocusNode();

  late var parentContext;
  bool isLoading = false;
  bool isAvailableApple = false;
  bool isActiveAudio = false;

  void generateOtpAPi() async {
    setState(() {
      isLoading = true;
    });
    // Map<String, dynamic> map = await http.post(
    //     'generate_otp',
    //     jsonEncode({
    //       'phone': mobileController.text.trim(),
    //     }),
    //     '');
    // // Map<String, dynamic> map = (await http.post(
    // //     Uri.parse(''),
    // //     // headers: <String, String>{
    // //     //   'Content-Type': 'application/json; charset=UTF-8',
    // //     // },
    // //     jsonEncode({
    // //       'phone': mobileController.text.trim(),
    // //     }),
    //     )) as Map<String, dynamic>;
    var response = await http.post(
      Uri.parse(''),
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode({
        'phone': mobileController.text.trim(),
      }),
    );
    if (response.statusCode == 200) {
      Map<String, dynamic> data = jsonDecode(response.body);
      print("success");
      // Use the data here
    } else {
      throw Exception('Failed to generate OTP');
    }
    // if (response.isEmpty) {
    //   // Fluttertoast.showToast(
    //   //     msg: 'Try again',
    //   //     toastLength: Toast.LENGTH_SHORT,
    //   //     gravity: ToastGravity.TOP,
    //   //     backgroundColor: Colors.black45,
    //   //     textColor: Colors.white,
    //   //     fontSize: 16.0);
    // } else {
    //   if (map['status'] == true) {
    //     print('done');
    //
    //     // Fluttertoast.showToast(
    //     //     msg: '${map['OTP']}',
    //     //     toastLength: Toast.LENGTH_LONG,
    //     //     gravity: ToastGravity.TOP,
    //     //     backgroundColor: Colors.black45,
    //     //     textColor: Colors.white,
    //     //     fontSize: 16.0);
    //     print('otp is ${map['OTP']}');
    //     Navigator.pushNamed(context, VerifyOtpScreen.routeName, arguments: {
    //       'phone': mobileController.text,
    //       'otp': map['OTP'],
    //     });
    //     print('map data is $map');
    //   } else {
    //     // Fluttertoast.showToast(
    //     //     msg: map['message'],
    //     //     toastLength: Toast.LENGTH_SHORT,
    //     //     gravity: ToastGravity.TOP,
    //     //     backgroundColor: Colors.black45,
    //     //     textColor: Colors.white,
    //     //     fontSize: 16.0);
    //   }
    // }

    setState(() {
      isLoading = false;
    });
  }

  AudioManager get audioPlayerService => injector<AudioManager>();

  @override
  void initState() {
    super.initState();
    _loginButtonController = AnimationController(
        duration: const Duration(milliseconds: 3000), vsync: this);
  }

  @override
  Future<void> afterFirstLayout(BuildContext context) async {
    if (audioPlayerService.isStickyAudioWidgetActive) {
      isActiveAudio = true;
      audioPlayerService
        ..pause()
        ..hideStickyAudioWidget();
    }
    try {
      isAvailableApple =
          (await TheAppleSignIn.isAvailable()) || Config().isBuilder;
      setState(() {});
    } catch (e) {
      printLog('[Login] afterFirstLayout error');
    }
  }

  @override
  void dispose() async {
    _loginButtonController.dispose();
    username.dispose();
    password.dispose();
    usernameNode.dispose();
    passwordNode.dispose();
    super.dispose();
  }

  Future _playAnimation() async {
    try {
      setState(() {
        isLoading = true;
      });
      await _loginButtonController.forward();
    } on TickerCanceled {
      printLog('[_playAnimation] error');
    }
  }

  Future _stopAnimation() async {
    try {
      await _loginButtonController.reverse();
      setState(() {
        isLoading = false;
      });
    } on TickerCanceled {
      printLog('[_stopAnimation] error');
    }
  }

  Future _welcomeMessage(user) async {
    final canPop = ModalRoute.of(context)!.canPop;
    if (canPop) {
      // When not required login
      Navigator.of(context).pop();
    } else {
      // When required login
      await Navigator.of(App.fluxStoreNavigatorKey.currentContext!)
          .pushReplacementNamed(RouteList.dashboard);
    }
  }

  void _failMessage(String message) {
    /// Showing Error messageSnackBarDemo
    /// Ability so close message
    if (message.isEmpty) return;

    var _message = message;
    if (kReleaseMode) {
      _message = S
          .of(context)
          .UserNameInCorrect;
    }

    final snackBar = SnackBar(
      content: Text(S.of(context).warning(_message)),
      duration: const Duration(seconds: 30),
      action: SnackBarAction(
        label: S
            .of(context)
            .close,
        onPressed: () {
          // Some code to undo the change.
        },
      ),
    );

    ScaffoldMessenger.of(context)
      ..removeCurrentSnackBar()
      ..showSnackBar(snackBar);
  }

  void _loginFacebook(context) async {
    //showLoading();
    await _playAnimation();
    await widget.loginFB(
      success: (user) {
        //hideLoading();
        _stopAnimation();
        _welcomeMessage(user);
      },
      fail: (message) {
        //hideLoading();
        _stopAnimation();
        _failMessage(message);
      },
      context: context,
    );
  }

  void _loginApple(context) async {
    await _playAnimation();
    await widget.loginApple(
        success: (user) {
          _stopAnimation();
          _welcomeMessage(user);
        },
        fail: (message) {
          _stopAnimation();
          _failMessage(message);
        },
        context: context);
  }

  @override
  Widget build(BuildContext context) {
    parentContext = context;
    final appModel = Provider.of<AppModel>(context);
    final screenSize = MediaQuery
        .of(context)
        .size;
    final themeConfig = appModel.themeConfig;

    var forgetPasswordUrl = Config().forgetPassword;

    Future launchForgetPassworddWebView(String url) async {
      await Navigator.push(
        context,
        MaterialPageRoute<void>(
          builder: (BuildContext context) =>
              WebView(url: url, title: S
                  .of(context)
                  .resetPassword),
          fullscreenDialog: true,
        ),
      );
    }

    void launchForgetPasswordURL(String? url) async {
      if (url != null && url != '') {
        /// show as webview
        await launchForgetPassworddWebView(url);
      } else {
        /// show as native
        await Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => ForgotPasswordScreen()),
        );
      }
    }

    void _login(context) async {
      if (username.text.isEmpty || password.text.isEmpty) {
        Tools.showSnackBar(Scaffold.of(context), S
            .of(context)
            .pleaseInput);
      } else {
        await _playAnimation();
        await widget.login(
          username: username.text.trim(),
          password: password.text.trim(),
          success: (user) {
            _stopAnimation();
            _welcomeMessage(user);
          },
          fail: (message) {
            _stopAnimation();
            _failMessage(message);
          },
        );
      }
    }

    void _loginNew(context) async {
      await _playAnimation();
      if (mobileController.text.isEmpty || mobileController.text.length < 10) {
        await _stopAnimation();
        Tools.showSnackBar(Scaffold.of(context), S
            .of(context)
            .pleaseInput);
      } else {
        print('Else Check');
        // await _playAnimation();
        // await widget.login(
        //   username: username.text.trim(),
        //   password: password.text.trim(),
        //   success: (user) {
        //     _stopAnimation();
        //     _welcomeMessage(user);
        //   },
        //   fail: (message) {
        //     _stopAnimation();
        //     _failMessage(message);
        //   },
        // );
        //await Navigator.of(context).pushNamed(RouteList.loginOtp);
        var min = 1000;
        var max = 9999;
        var randomizer = Random();
        var generatedRandormNumber =
        (min + randomizer.nextInt(max - min)).toString();
        print(generatedRandormNumber);
        final responsevar = await http.post(
            Uri.parse(
                'https://96c615926454dc5958c1262b9476ab44c421500b3b2330f1:6ab04d7ef98123db82aacd43f4ed9af7ff6c0a63c3ccd0cc@api.exotel.com/v1/Accounts/pujeetretail1/Sms/send'),
            body: {
              'To': mobileController.text,
              'From': '02071177066',
              'Body':
              'Your QwickPick verification code is $generatedRandormNumber. Please do not share this otp with anyone to ensure the account security.'
              // 'Dear Customer, your order $generatedRandormNumber is cancelled. Go to>MyOrders for details or to contact us. -Thank you, qwickpick.in'
            });
        print("Response Var");
        print(responsevar.body);
        if (responsevar.statusCode == 200) {
          final body = responsevar.body;
          print('Response Body is');
          print(body);
          await _stopAnimation();
        }
        await Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) =>
                LogInWithOtp(
                  getMobileController: mobileController.text,
                  getGeneratedOTP: generatedRandormNumber,
                ),
          ),
        );
      }
    }

    void _loginSMS(context) {
      if (widget.loginSms != null) {
        widget.loginSms!();
        return;
      }
      final supportedPlatforms = [
        'wcfm',
        'dokan',
        'delivery',
        'vendorAdmin',
        'woo',
        'wordpress'
      ].contains(serverConfig['type']);
      if (supportedPlatforms &&
          (kAdvanceConfig['EnableNewSMSLogin'] ?? false)) {
        final model = Provider.of<UserModel>(context, listen: false);
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (_) =>
                SMSLoginScreen(
                  onSuccess: (user) async {
                    await model.saveSMSUser(user);
                    Navigator.of(context).pop();
                    await _welcomeMessage(user);
                  },
                ),
          ),
        );
        return;
      }

      Navigator.of(context).pushNamed(RouteList.loginSMS);
    }

    void _loginGoogle(context) async {
      await _playAnimation();
      await widget.loginGoogle(
          success: (user) {
            //hideLoading();
            _stopAnimation();
            _welcomeMessage(user);
          },
          fail: (message) {
            //hideLoading();
            _stopAnimation();
            _failMessage(message);
          },
          context: context);
    }

    return Scaffold(
      backgroundColor: Theme
          .of(context)
          .backgroundColor,
      appBar: AppBar(
        systemOverlayStyle: SystemUiOverlayStyle.light,
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        actions: [
          Padding(
            padding: const EdgeInsets.only(top: 16.0),
            child: InkWell(
              onTap: () async {
                var prefs = await SharedPreferences.getInstance();
                await prefs.setBool('seen', true);
                await Navigator.of(context)
                    .pushReplacementNamed(RouteList.dashboard);
              },
              child: const Text(
                'Skip',
                style: TextStyle(
                  fontSize: 16,
                  color: kDarkBG,
                  fontWeight: FontWeight.w800
                ),
              ),
            ),
          ),
          const SizedBox(width: 20),
        ],
      ),
      body: SafeArea(
        child: AutoHideKeyboard(
          child: Center(
            child: Stack(
              children: [
                Consumer<UserModel>(builder: (context, model, child) {
                  return SingleChildScrollView(
                    padding: const EdgeInsets.symmetric(horizontal: 30.0),
                    child: Container(
                      alignment: Alignment.center,
                      width: screenSize.width /
                          (2 / (screenSize.height / screenSize.width)),
                      constraints: const BoxConstraints(maxWidth: 700),
                      child: AutofillGroup(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            const SizedBox(height: 0.0),
                            // Column(
                            //   children: <Widget>[
                            //     Row(
                            //       mainAxisAlignment: MainAxisAlignment.center,
                            //       children: <Widget>[
                            //         SizedBox(
                            //           height: 40.0,
                            //           child: FluxImage(
                            //             imageUrl: themeConfig.logo,
                            //           ),
                            //         ),
                            //       ],
                            //     ),
                            //   ],
                            // ),
                            Image.asset(
                              'assets/images/Myproject.png',
                              width: 200,
                              height: 200,
                            ),
                            const SizedBox(height: 20.0),
                            const Text(
                              'Welcome to Qwickpick',
                              style: TextStyle(
                                  fontSize: 22, fontWeight: FontWeight.bold),
                            ),
                            // const SizedBox(
                            //   height: 10,
                            // ),
                            // const Text(
                            //   'Lorem ipsum dolor sit amet,\nConnectEDU advising elite.',
                            //   style: TextStyle(
                            //     fontSize: 16,
                            //   ),
                            //   textAlign: TextAlign.center,
                            // ),
                            const SizedBox(
                              height: 15,
                            ),
                            const Text(
                              'Sign Up / Log In',
                              style: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                              ),
                              textAlign: TextAlign.center,
                            ),
                            const SizedBox(
                              height: 15,
                            ),

                            // CustomTextField(
                            //   key: const Key('loginEmailField'),
                            //   controller: username,
                            //   autofillHints: const [AutofillHints.email],
                            //   showCancelIcon: true,
                            //   autocorrect: false,
                            //   enableSuggestions: false,
                            //   textInputAction: TextInputAction.next,
                            //   keyboardType: TextInputType.emailAddress,
                            //   nextNode: passwordNode,
                            //   decoration: InputDecoration(
                            //     labelText: S.of(parentContext).username,
                            //     hintText: S
                            //         .of(parentContext)
                            //         .enterYourEmailOrUsername,
                            //   ),
                            // ),
                            // CustomTextField(
                            //   key: const Key('loginPasswordField'),
                            //   autofillHints: const [AutofillHints.password],
                            //   obscureText: true,
                            //   showEyeIcon: true,
                            //   textInputAction: TextInputAction.done,
                            //   controller: password,
                            //   focusNode: passwordNode,
                            //   decoration: InputDecoration(
                            //     labelText: S.of(parentContext).password,
                            //     hintText: S.of(parentContext).enterYourPassword,
                            //   ),
                            // ),
                            // if ((kLoginSetting['isResetPasswordSupported'] ??
                            //     false))
                            //   Padding(
                            //     padding:
                            //         const EdgeInsets.symmetric(vertical: 12.0),
                            //     child: GestureDetector(
                            //       onTap: () {
                            //         launchForgetPasswordURL(forgetPasswordUrl);
                            //       },
                            //       behavior: HitTestBehavior.opaque,
                            //       child: Padding(
                            //         padding: const EdgeInsets.all(12.0),
                            //         child: Text(
                            //           S.of(context).resetPassword,
                            //           style: TextStyle(
                            //             color: Theme.of(context).primaryColor,
                            //             decoration: TextDecoration.underline,
                            //           ),
                            //         ),
                            //       ),
                            //     ),
                            //   ),
                            Padding(
                              padding:
                              const EdgeInsets.symmetric(horizontal: 10.0),
                              child: Container(
                                // height: 55,
                                decoration: BoxDecoration(
                                    border: Border.all(
                                        width: 1, color: Theme.of(context).primaryColor),
                                    borderRadius: BorderRadius.circular(10)),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    const SizedBox(
                                      width: 20,
                                    ),
                                    const SizedBox(
                                      width: 40,
                                      child: Text('+91',
                                        style: TextStyle(
                                          color: kDarkBG,
                                          fontWeight: FontWeight.w700,
                                        ),),
                                    ),
                                    const SizedBox(
                                      width: 10,
                                    ),
                                    Expanded(
                                        child: TextField(
                                          controller: mobileController,
                                          keyboardType: TextInputType.phone,
                                          maxLength: 10,
                                          decoration: const InputDecoration(
                                            border: InputBorder.none,
                                            hintText: 'Enter mobile number',
                                            counterText: '',
                                          ),
                                        ))
                                  ],
                                ),
                              ),
                            ),
                            const SizedBox(
                              height: 15,
                            ),
                            // if (kLoginSetting['isResetPasswordSupported'] !=
                            //     true)
                            //   const SizedBox(height: 50.0),
                            StaggerAnimation(
                              key: const Key('loginSubmitButton'),
                              titleButton: 'Continue',
                              buttonController: _loginButtonController.view
                              as AnimationController,
                              onTap: () {
                                if (!isLoading) {
                                  //_login(context);
                                  //print("hiii");
                                  _loginNew(context);
                                }
                              },
                            ),
                            const SizedBox(height: 10,),
                            if (kLoginSetting['showAppleLogin'] &&
                                isAvailableApple)
                              InkWell(
                                onTap: () => _loginApple(context),
                                child: Container(
                                  padding: const EdgeInsets.all(12),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(40),
                                    color: Colors.black87,
                                  ),
                                  child: Image.asset(
                                    'assets/icons/logins/apple.png',
                                    width: 26,
                                    height: 26,
                                  ),
                                ),
                              ),
                            // StaggerAnimation(
                            //   key: const Key('loginSubmitButton'),
                            //   titleButton: S.of(context).signInWithEmail,
                            //   buttonController: _loginButtonController.view
                            //       as AnimationController,
                            //   onTap: () {
                            //     if (!isLoading) {
                            //       _login(context);
                            //     }
                            //   },
                            // ),
                            // Stack(
                            //   alignment: AlignmentDirectional.center,
                            //   children: <Widget>[
                            //     SizedBox(
                            //         height: 50.0,
                            //         width: 200.0,
                            //         child:
                            //             Divider(color: Colors.grey.shade300)),
                            //     Container(
                            //         height: 30,
                            //         width: 40,
                            //         color: Theme.of(context).backgroundColor),
                            //     if (kLoginSetting['showFacebook'] ||
                            //         kLoginSetting['showSMSLogin'] ||
                            //         kLoginSetting['showGoogleLogin'] ||
                            //         kLoginSetting['showAppleLogin'])
                            //       Text(
                            //         S.of(context).or,
                            //         style: TextStyle(
                            //             fontSize: 12,
                            //             color: Colors.grey.shade400),
                            //       )
                            //   ],
                            // ),
                            // Row(
                            //   mainAxisAlignment: MainAxisAlignment.spaceAround,
                            //   children: <Widget>[
                            //     if (kLoginSetting['showAppleLogin'] &&
                            //         isAvailableApple)
                            //       InkWell(
                            //         onTap: () => _loginApple(context),
                            //         child: Container(
                            //           padding: const EdgeInsets.all(12),
                            //           decoration: BoxDecoration(
                            //             borderRadius: BorderRadius.circular(40),
                            //             color: Colors.black87,
                            //           ),
                            //           child: Image.asset(
                            //             'assets/icons/logins/apple.png',
                            //             width: 26,
                            //             height: 26,
                            //           ),
                            //         ),
                            //       ),
                            //     if (kLoginSetting['showFacebook'])
                            //       InkWell(
                            //         onTap: () => _loginFacebook(context),
                            //         child: Container(
                            //           padding: const EdgeInsets.all(8),
                            //           decoration: BoxDecoration(
                            //             borderRadius: BorderRadius.circular(40),
                            //             color: const Color(0xFF4267B2),
                            //           ),
                            //           child: const Icon(
                            //             Icons.facebook_rounded,
                            //             color: Colors.white,
                            //             size: 34.0,
                            //           ),
                            //         ),
                            //       ),
                            //     if (kLoginSetting['showGoogleLogin'])
                            //       InkWell(
                            //         onTap: () => _loginGoogle(context),
                            //         child: Container(
                            //           padding: const EdgeInsets.all(12),
                            //           decoration: BoxDecoration(
                            //             borderRadius: BorderRadius.circular(40),
                            //             color: Colors.grey.shade300,
                            //           ),
                            //           child: Image.asset(
                            //             'assets/icons/logins/google.png',
                            //             width: 28,
                            //             height: 28,
                            //           ),
                            //         ),
                            //       ),
                            //     if (kLoginSetting['showSMSLogin'])
                            //       InkWell(
                            //         onTap: () => _loginSMS(context),
                            //         child: Container(
                            //           padding: const EdgeInsets.all(12),
                            //           decoration: BoxDecoration(
                            //             borderRadius: BorderRadius.circular(40),
                            //             color: Colors.lightBlue.shade50,
                            //           ),
                            //           child: Image.asset(
                            //             'assets/icons/logins/sms.png',
                            //             width: 28,
                            //             height: 28,
                            //           ),
                            //         ),
                            //       ),
                            //   ],
                            // ),
                            //const SizedBox(height: 30.0),
                            // Column(
                            //   children: <Widget>[
                            //     Row(
                            //       mainAxisAlignment: MainAxisAlignment.center,
                            //       children: <Widget>[
                            //         Text(S.of(context).dontHaveAccount),
                            //         GestureDetector(
                            //           onTap: () {
                            //             if (kAdvanceConfig[
                            //                     'EnableMembershipUltimate'] ==
                            //                 true) {
                            //               Navigator.of(context).pushNamed(
                            //                   RouteList
                            //                       .memberShipUltimatePlans);
                            //             } else if (kAdvanceConfig[
                            //                     'EnablePaidMembershipPro'] ==
                            //                 true) {
                            //               Navigator.of(context).pushNamed(
                            //                   RouteList.paidMemberShipProPlans);
                            //             } else {
                            //               Navigator.of(context)
                            //                   .pushNamed(RouteList.register);
                            //             }
                            //           },
                            //           child: Text(
                            //             ' ${S.of(context).signup}',
                            //             style: TextStyle(
                            //               fontWeight: FontWeight.bold,
                            //               color: Theme.of(context).primaryColor,
                            //             ),
                            //           ),
                            //         ),
                            //       ],
                            //     ),
                            //   ],
                            // ),
                            const SizedBox(height: 90.0),
                          ],
                        ),
                      ),
                    ),
                  );
                }),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void showLoading() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Center(
            child: Container(
              padding: const EdgeInsets.all(50.0),
              child: kLoadingWidget(context),
            ));
      },
    );
  }

  void hideLoading() {
    Navigator.of(context).pop();
  }


}
// import 'package:flutter/foundation.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter/services.dart';
// import 'package:inspireui/inspireui.dart';
// import 'package:provider/provider.dart';
// import 'package:the_apple_sign_in/the_apple_sign_in.dart';
//
// import '../../app.dart';
// import '../../common/config.dart';
// import '../../common/constants.dart';
// import '../../common/tools.dart';
// import '../../generated/l10n.dart';
// import '../../models/index.dart';
// import '../../modules/sms_login/sms_login.dart';
// import '../../services/index.dart';
// import '../../widgets/common/custom_text_field.dart';
// import '../../widgets/common/flux_image.dart';
// import '../../widgets/common/login_animation.dart';
// import '../../widgets/common/webview.dart';
// import '../base_screen.dart';
// import 'forgot_password_screen.dart';
//
// typedef LoginSocialFunction = Future<void> Function({
// required Function(User user) success,
// required Function(String) fail,
// BuildContext context,
// });
//
// typedef LoginFunction = Future<void> Function({
// required String username,
// required String password,
// required Function(User user) success,
// required Function(String) fail,
// });
//
// class LoginScreen extends StatefulWidget {
//   final LoginFunction login;
//   final LoginSocialFunction loginFB;
//   final LoginSocialFunction loginApple;
//   final LoginSocialFunction loginGoogle;
//   final VoidCallback? loginSms;
//
//   const LoginScreen({
//     required this.login,
//     required this.loginFB,
//     required this.loginApple,
//     required this.loginGoogle,
//     this.loginSms,
//   });
//
//   @override
//   _LoginPageState createState() => _LoginPageState();
// }
//
// class _LoginPageState extends BaseScreen<LoginScreen>
//     with TickerProviderStateMixin {
//   late AnimationController _loginButtonController;
//   final TextEditingController username = TextEditingController();
//   final TextEditingController password = TextEditingController();
//
//   final usernameNode = FocusNode();
//   final passwordNode = FocusNode();
//
//   late var parentContext;
//   bool isLoading = false;
//   bool isAvailableApple = false;
//   bool isActiveAudio = false;
//
//   AudioManager get audioPlayerService => injector<AudioManager>();
//
//   @override
//   void initState() {
//     super.initState();
//     _loginButtonController = AnimationController(
//         duration: const Duration(milliseconds: 3000), vsync: this);
//   }
//
//   @override
//   Future<void> afterFirstLayout(BuildContext context) async {
//     if (audioPlayerService.isStickyAudioWidgetActive) {
//       isActiveAudio = true;
//       audioPlayerService
//         ..pause()
//         ..hideStickyAudioWidget();
//     }
//     try {
//       isAvailableApple =
//           (await TheAppleSignIn.isAvailable()) || Config().isBuilder;
//       setState(() {});
//     } catch (e) {
//       printLog('[Login] afterFirstLayout error');
//     }
//   }
//
//   @override
//   void dispose() async {
//     _loginButtonController.dispose();
//     username.dispose();
//     password.dispose();
//     usernameNode.dispose();
//     passwordNode.dispose();
//     super.dispose();
//   }
//
//   Future _playAnimation() async {
//     try {
//       setState(() {
//         isLoading = true;
//       });
//       await _loginButtonController.forward();
//     } on TickerCanceled {
//       printLog('[_playAnimation] error');
//     }
//   }
//
//   Future _stopAnimation() async {
//     try {
//       await _loginButtonController.reverse();
//       setState(() {
//         isLoading = false;
//       });
//     } on TickerCanceled {
//       printLog('[_stopAnimation] error');
//     }
//   }
//
//   Future _welcomeMessage(user) async {
//     final canPop = ModalRoute.of(context)!.canPop;
//     if (canPop) {
//       // When not required login
//       Navigator.of(context).pop();
//     } else {
//       // When required login
//       await Navigator.of(App.fluxStoreNavigatorKey.currentContext!)
//           .pushReplacementNamed(RouteList.dashboard);
//     }
//   }
//
//   void _failMessage(String message) {
//     /// Showing Error messageSnackBarDemo
//     /// Ability so close message
//     if (message.isEmpty) return;
//
//     var _message = message;
//     if (kReleaseMode) {
//       _message = S.of(context).UserNameInCorrect;
//     }
//
//     final snackBar = SnackBar(
//       content: Text(S.of(context).warning(_message)),
//       duration: const Duration(seconds: 30),
//       action: SnackBarAction(
//         label: S.of(context).close,
//         onPressed: () {
//           // Some code to undo the change.
//         },
//       ),
//     );
//
//     ScaffoldMessenger.of(context)
//       ..removeCurrentSnackBar()
//       ..showSnackBar(snackBar);
//   }
//
//   void _loginFacebook(context) async {
//     //showLoading();
//     await _playAnimation();
//     await widget.loginFB(
//       success: (user) {
//         //hideLoading();
//         _stopAnimation();
//         _welcomeMessage(user);
//       },
//       fail: (message) {
//         //hideLoading();
//         _stopAnimation();
//         _failMessage(message);
//       },
//       context: context,
//     );
//   }
//
//   void _loginApple(context) async {
//     await _playAnimation();
//     await widget.loginApple(
//         success: (user) {
//           _stopAnimation();
//           _welcomeMessage(user);
//         },
//         fail: (message) {
//           _stopAnimation();
//           _failMessage(message);
//         },
//         context: context);
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     parentContext = context;
//     final appModel = Provider.of<AppModel>(context);
//     final screenSize = MediaQuery.of(context).size;
//     final themeConfig = appModel.themeConfig;
//
//     var forgetPasswordUrl = Config().forgetPassword;
//
//     Future launchForgetPassworddWebView(String url) async {
//       await Navigator.push(
//         context,
//         MaterialPageRoute<void>(
//           builder: (BuildContext context) =>
//               WebView(url: url, title: S.of(context).resetPassword),
//           fullscreenDialog: true,
//         ),
//       );
//     }
//
//     void launchForgetPasswordURL(String? url) async {
//       if (url != null && url != '') {
//         /// show as webview
//         await launchForgetPassworddWebView(url);
//       } else {
//         /// show as native
//         await Navigator.push(
//           context,
//           MaterialPageRoute(builder: (context) => ForgotPasswordScreen()),
//         );
//       }
//     }
//
//     void _login(context) async {
//       if (username.text.isEmpty || password.text.isEmpty) {
//         Tools.showSnackBar(Scaffold.of(context), S.of(context).pleaseInput);
//       } else {
//         await _playAnimation();
//         await widget.login(
//           username: username.text.trim(),
//           password: password.text.trim(),
//           success: (user) {
//             _stopAnimation();
//             _welcomeMessage(user);
//           },
//           fail: (message) {
//             _stopAnimation();
//             _failMessage(message);
//           },
//         );
//       }
//     }
//
//     void _loginSMS(context) {
//       if (widget.loginSms != null) {
//         widget.loginSms!();
//         return;
//       }
//       final supportedPlatforms = [
//         'wcfm',
//         'dokan',
//         'delivery',
//         'vendorAdmin',
//         'woo',
//         'wordpress'
//       ].contains(serverConfig['type']);
//       if (supportedPlatforms &&
//           (kAdvanceConfig['EnableNewSMSLogin'] ?? false)) {
//         final model = Provider.of<UserModel>(context, listen: false);
//         Navigator.of(context).push(
//           MaterialPageRoute(
//             builder: (_) => SMSLoginScreen(
//               onSuccess: (user) async {
//                 await model.saveSMSUser(user);
//                 Navigator.of(context).pop();
//                 await _welcomeMessage(user);
//               },
//             ),
//           ),
//         );
//         return;
//       }
//
//       Navigator.of(context).pushNamed(RouteList.loginSMS);
//     }
//
//     void _loginGoogle(context) async {
//       await _playAnimation();
//       await widget.loginGoogle(
//           success: (user) {
//             //hideLoading();
//             _stopAnimation();
//             _welcomeMessage(user);
//           },
//           fail: (message) {
//             //hideLoading();
//             _stopAnimation();
//             _failMessage(message);
//           },
//           context: context);
//     }
//
//     return Scaffold(
//       backgroundColor: Theme.of(context).backgroundColor,
//       appBar: AppBar(
//         systemOverlayStyle: SystemUiOverlayStyle.light,
//         backgroundColor: Colors.transparent,
//         elevation: 0.0,
//       ),
//       body: SafeArea(
//         child: AutoHideKeyboard(
//           child: Center(
//             child: Stack(
//               children: [
//                 Consumer<UserModel>(builder: (context, model, child) {
//                   return SingleChildScrollView(
//                     padding: const EdgeInsets.symmetric(horizontal: 30.0),
//                     child: Container(
//                       alignment: Alignment.center,
//                       width: screenSize.width /
//                           (2 / (screenSize.height / screenSize.width)),
//                       constraints: const BoxConstraints(maxWidth: 700),
//                       child: AutofillGroup(
//                         child: Column(
//                           children: <Widget>[
//                             const SizedBox(height: 40.0),
//                             Column(
//                               children: <Widget>[
//                                 Row(
//                                   mainAxisAlignment: MainAxisAlignment.center,
//                                   children: <Widget>[
//                                     SizedBox(
//                                       height: 40.0,
//                                       child: FluxImage(
//                                         imageUrl: themeConfig.logo,
//                                       ),
//                                     ),
//                                   ],
//                                 ),
//                               ],
//                             ),
//                             const SizedBox(height: 80.0),
//                             CustomTextField(
//                               key: const Key('loginEmailField'),
//                               controller: username,
//                               autofillHints: const [AutofillHints.email],
//                               showCancelIcon: true,
//                               autocorrect: false,
//                               enableSuggestions: false,
//                               textInputAction: TextInputAction.next,
//                               keyboardType: TextInputType.emailAddress,
//                               nextNode: passwordNode,
//                               decoration: InputDecoration(
//                                 labelText: S.of(parentContext).username,
//                                 hintText: S
//                                     .of(parentContext)
//                                     .enterYourEmailOrUsername,
//                               ),
//                             ),
//                             CustomTextField(
//                               key: const Key('loginPasswordField'),
//                               autofillHints: const [AutofillHints.password],
//                               obscureText: true,
//                               showEyeIcon: true,
//                               textInputAction: TextInputAction.done,
//                               controller: password,
//                               focusNode: passwordNode,
//                               decoration: InputDecoration(
//                                 labelText: S.of(parentContext).password,
//                                 hintText: S.of(parentContext).enterYourPassword,
//                               ),
//                             ),
//                             if ((kLoginSetting['isResetPasswordSupported'] ??
//                                 false))
//                               Padding(
//                                 padding:
//                                 const EdgeInsets.symmetric(vertical: 12.0),
//                                 child: GestureDetector(
//                                   onTap: () {
//                                     launchForgetPasswordURL(forgetPasswordUrl);
//                                   },
//                                   behavior: HitTestBehavior.opaque,
//                                   child: Padding(
//                                     padding: const EdgeInsets.all(12.0),
//                                     child: Text(
//                                       S.of(context).resetPassword,
//                                       style: TextStyle(
//                                         color: Theme.of(context).primaryColor,
//                                         decoration: TextDecoration.underline,
//                                       ),
//                                     ),
//                                   ),
//                                 ),
//                               ),
//                             if (kLoginSetting['isResetPasswordSupported'] !=
//                                 true)
//                               const SizedBox(height: 50.0),
//                             StaggerAnimation(
//                               key: const Key('loginSubmitButton'),
//                               titleButton: S.of(context).signInWithEmail,
//                               buttonController: _loginButtonController.view
//                               as AnimationController,
//                               onTap: () {
//                                 if (!isLoading) {
//                                   _login(context);
//                                 }
//                               },
//                             ),
//                             Stack(
//                               alignment: AlignmentDirectional.center,
//                               children: <Widget>[
//                                 SizedBox(
//                                     height: 50.0,
//                                     width: 200.0,
//                                     child:
//                                     Divider(color: Colors.grey.shade300)),
//                                 Container(
//                                     height: 30,
//                                     width: 40,
//                                     color: Theme.of(context).backgroundColor),
//                                 if (kLoginSetting['showFacebook'] ||
//                                     kLoginSetting['showSMSLogin'] ||
//                                     kLoginSetting['showGoogleLogin'] ||
//                                     kLoginSetting['showAppleLogin'])
//                                   Text(
//                                     S.of(context).or,
//                                     style: TextStyle(
//                                         fontSize: 12,
//                                         color: Colors.grey.shade400),
//                                   )
//                               ],
//                             ),
//                             Row(
//                               mainAxisAlignment: MainAxisAlignment.spaceAround,
//                               children: <Widget>[
//                                 if (kLoginSetting['showAppleLogin'] &&
//                                     isAvailableApple)
//                                   InkWell(
//                                     onTap: () => _loginApple(context),
//                                     child: Container(
//                                       padding: const EdgeInsets.all(12),
//                                       decoration: BoxDecoration(
//                                         borderRadius: BorderRadius.circular(40),
//                                         color: Colors.black87,
//                                       ),
//                                       child: Image.asset(
//                                         'assets/icons/logins/apple.png',
//                                         width: 26,
//                                         height: 26,
//                                       ),
//                                     ),
//                                   ),
//                                 if (kLoginSetting['showFacebook'])
//                                   InkWell(
//                                     onTap: () => _loginFacebook(context),
//                                     child: Container(
//                                       padding: const EdgeInsets.all(8),
//                                       decoration: BoxDecoration(
//                                         borderRadius: BorderRadius.circular(40),
//                                         color: const Color(0xFF4267B2),
//                                       ),
//                                       child: const Icon(
//                                         Icons.facebook_rounded,
//                                         color: Colors.white,
//                                         size: 34.0,
//                                       ),
//                                     ),
//                                   ),
//                                 if (kLoginSetting['showGoogleLogin'])
//                                   InkWell(
//                                     onTap: () => _loginGoogle(context),
//                                     child: Container(
//                                       padding: const EdgeInsets.all(12),
//                                       decoration: BoxDecoration(
//                                         borderRadius: BorderRadius.circular(40),
//                                         color: Colors.grey.shade300,
//                                       ),
//                                       child: Image.asset(
//                                         'assets/icons/logins/google.png',
//                                         width: 28,
//                                         height: 28,
//                                       ),
//                                     ),
//                                   ),
//                                 if (kLoginSetting['showSMSLogin'])
//                                   InkWell(
//                                     onTap: () => _loginSMS(context),
//                                     child: Container(
//                                       padding: const EdgeInsets.all(12),
//                                       decoration: BoxDecoration(
//                                         borderRadius: BorderRadius.circular(40),
//                                         color: Colors.lightBlue.shade50,
//                                       ),
//                                       child: Image.asset(
//                                         'assets/icons/logins/sms.png',
//                                         width: 28,
//                                         height: 28,
//                                       ),
//                                     ),
//                                   ),
//                               ],
//                             ),
//                             const SizedBox(height: 30.0),
//                             Column(
//                               children: <Widget>[
//                                 Row(
//                                   mainAxisAlignment: MainAxisAlignment.center,
//                                   children: <Widget>[
//                                     Text(S.of(context).dontHaveAccount),
//                                     GestureDetector(
//                                       onTap: () {
//                                         if (kAdvanceConfig[
//                                         'EnableMembershipUltimate'] ==
//                                             true) {
//                                           Navigator.of(context).pushNamed(
//                                               RouteList
//                                                   .memberShipUltimatePlans);
//                                         } else if (kAdvanceConfig[
//                                         'EnablePaidMembershipPro'] ==
//                                             true) {
//                                           Navigator.of(context).pushNamed(
//                                               RouteList.paidMemberShipProPlans);
//                                         } else {
//                                           Navigator.of(context)
//                                               .pushNamed(RouteList.register);
//                                         }
//                                       },
//                                       child: Text(
//                                         ' ${S.of(context).signup}',
//                                         style: TextStyle(
//                                           fontWeight: FontWeight.bold,
//                                           color: Theme.of(context).primaryColor,
//                                         ),
//                                       ),
//                                     ),
//                                   ],
//                                 ),
//                               ],
//                             )
//                           ],
//                         ),
//                       ),
//                     ),
//                   );
//                 }),
//               ],
//             ),
//           ),
//         ),
//       ),
//     );
//   }
//
//   void showLoading() {
//     showDialog(
//       context: context,
//       barrierDismissible: false,
//       builder: (BuildContext context) {
//         return Center(
//             child: Container(
//               padding: const EdgeInsets.all(50.0),
//               child: kLoadingWidget(context),
//             ));
//       },
//     );
//   }
//
//   void hideLoading() {
//     Navigator.of(context).pop();
//   }
// }
