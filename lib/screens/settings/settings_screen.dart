import 'dart:async';

import 'package:flutter/cupertino.dart'
    show
    CupertinoAlertDialog,
    CupertinoDialogAction,
    CupertinoIcons,
    showCupertinoDialog;
import 'package:flutter/material.dart';
import 'package:inspireui/icons/icon_picker.dart';
import 'package:localstorage/localstorage.dart';
import 'package:provider/provider.dart';
import 'package:rate_my_app/rate_my_app.dart';

import '../../app.dart';
import '../../common/config.dart';
import '../../common/config/configuration_utils.dart';
import '../../common/constants.dart';
import '../../common/tools.dart';
import '../../generated/l10n.dart';
import '../../models/index.dart'
    show AppModel, CartModel, User, UserModel;
import '../../routes/flux_navigate.dart';
import '../../services/index.dart';
import '../../widgets/common/index.dart';
import '../../widgets/general/index.dart';
import '../common/app_bar_mixin.dart';
import '../index.dart';
import '../posts/post_screen.dart';
import '../users/user_point_screen.dart';


const itemPadding = 15.0;

class SettingScreen extends StatefulWidget {
  final List<dynamic>? settings;
  final Map? subGeneralSetting;
  final String? background;
  final Map? drawerIcon;
  final VoidCallback? onLogout;
  final bool hideUser;

  const SettingScreen({
    this.onLogout,
    this.settings,
    this.subGeneralSetting,
    this.background,
    this.drawerIcon,
    this.hideUser = false,
  });

  @override
  SettingScreenState createState() {
    return SettingScreenState();
  }
}

class SettingScreenState extends State<SettingScreen>
    with
        TickerProviderStateMixin,
        AutomaticKeepAliveClientMixin<SettingScreen>,
        AppBarMixin {
  @override
  bool get wantKeepAlive => true;

  User? get user => Provider.of<UserModel>(context, listen: false).user;
  bool isAbleToPostManagement = false;
  bool isLoading = false;
  var id;
  String userIdUrl = '';

  final bannerHigh = 150.0;
  final RateMyApp _rateMyApp = RateMyApp(
    // rate app on store
      minDays: 7,
      minLaunches: 10,
      remindDays: 7,
      remindLaunches: 10,
      googlePlayIdentifier: kStoreIdentifier['android'],
      appStoreIdentifier: kStoreIdentifier['ios']);

  void showRateMyApp() {
    _rateMyApp.showRateDialog(
      context,
      title: S.of(context).rateTheApp,
      // The dialog title.
      message: S.of(context).rateThisAppDescription,
      // The dialog message.
      rateButton: S.of(context).rate.toUpperCase(),
      // The dialog 'rate' button text.
      noButton: S.of(context).noThanks.toUpperCase(),
      // The dialog 'no' button text.
      laterButton: S.of(context).maybeLater.toUpperCase(),
      // The dialog 'later' button text.
      listener: (button) {
        // The button click listener (useful if you want to cancel the click event).
        switch (button) {
          case RateMyAppDialogButton.rate:
            break;
          case RateMyAppDialogButton.later:
            break;
          case RateMyAppDialogButton.no:
            break;
        }

        return true; // Return false if you want to cancel the click event.
      },
      // Set to false if you want to show the native Apple app rating dialog on iOS.
      dialogStyle: const DialogStyle(),
      // Custom dialog styles.
      // Called when the user dismissed the dialog (either by taping outside or by pressing the 'back' button).
      // actionsBuilder: (_) => [], // This one allows you to use your own buttons.
    );
  }

  void checkAddPostRole() {
    for (var legitRole in addPostAccessibleRoles) {
      if (user!.role == legitRole) {
        setState(() {
          isAbleToPostManagement = true;
        });
      }
    }
  }

  @override
  void initState() {
    super.initState();
    if (isMobile) {
      if (!kStoreIdentifier['disable']) {
        _rateMyApp.init().then((_) {
          // state of rating the app
          if (_rateMyApp.shouldOpenDialog) {
            showRateMyApp();
          }
        });
      }
    }
  }

  /// Render the Delivery Menu.
  /// Currently support WCFM
  Widget renderDeliveryBoy() {
    var isDelivery = user?.isDeliveryBoy ?? false;

    if (!isDelivery) {
      return const SizedBox();
    }

    return Card(
      color: Theme.of(context).backgroundColor,
      margin: const EdgeInsets.only(bottom: 2.0),
      elevation: 0,
      child: ListTile(
        onTap: () {
          FluxNavigate.push(
            MaterialPageRoute(
              builder: (context) =>
              Services().widget.getDeliveryScreen(context, user)!,
            ),
            forceRootNavigator: true,
          );
        },
        leading: Icon(
          CupertinoIcons.cube_box,
          size: 24,
          color: Theme.of(context).colorScheme.secondary,
        ),
        title: Text(
          S.of(context).deliveryManagement,
          style: const TextStyle(fontSize: 16),
        ),
        trailing: Icon(
          Icons.arrow_forward_ios,
          size: 18,
          color: Theme.of(context).colorScheme.secondary,
        ),
      ),
    );
  }

  /// Render the Admin Vendor Menu.
  /// Currently support WCFM & Dokan. Will support WooCommerce soon.
  Widget renderVendorAdmin() {
    var isVendor = user?.isVender ?? false;

    if (!isVendor || serverConfig['type'] == 'listeo') {
      return const SizedBox();
    }

    return Card(
      color: Theme.of(context).backgroundColor,
      margin: const EdgeInsets.only(bottom: 2.0),
      elevation: 0,
      child: ListTile(
        onTap: () {
          FluxNavigate.pushNamed(
            RouteList.vendorAdmin,
            arguments: user,
            forceRootNavigator: true,
          );
        },
        leading: Icon(
          Icons.dashboard,
          size: 24,
          color: Theme.of(context).colorScheme.secondary,
        ),
        title: Text(
          S.of(context).vendorAdmin,
          style: const TextStyle(fontSize: 16),
        ),
        trailing: Icon(
          Icons.arrow_forward_ios,
          size: 18,
          color: Theme.of(context).colorScheme.secondary,
        ),
      ),
    );
  }

  Widget renderVendorVacation() {
    var isVendor = user?.isVender ?? false;

    if ((kFluxStoreMV.contains(serverConfig['type']) && !isVendor) ||
        serverConfig['type'] != 'wcfm' ||
        !kVendorConfig['DisableNativeStoreManagement']) {
      return const SizedBox();
    }

    return Card(
      color: Theme.of(context).backgroundColor,
      margin: const EdgeInsets.only(bottom: 2.0),
      elevation: 0,
      child: ListTile(
        onTap: () {
          FluxNavigate.push(
            MaterialPageRoute(
              builder: (context) => Services().widget.renderVacationVendor(
                  user!.id!, user!.cookie!,
                  isFromMV: true),
            ),
            forceRootNavigator: true,
          );
        },
        leading: Icon(
          Icons.house,
          size: 24,
          color: Theme.of(context).colorScheme.secondary,
        ),
        title: Text(
          S.of(context).storeVacation,
          style: const TextStyle(fontSize: 16),
        ),
        trailing: Icon(
          Icons.arrow_forward_ios,
          size: 18,
          color: Theme.of(context).colorScheme.secondary,
        ),
      ),
    );
  }

  /// Render the custom profile link via Webview
  /// Example show some special profile on the woocommerce site: wallet, wishlist...
  Widget renderWebViewProfile() {
    if (user == null) {
      return Container();
    }

    var base64Str = EncodeUtils.encodeCookie(user!.cookie!);
    var profileURL = '${serverConfig['url']}/my-account?cookie=$base64Str';

    return Card(
      color: Theme.of(context).backgroundColor,
      margin: const EdgeInsets.only(bottom: 2.0),
      elevation: 0,
      child: ListTile(
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => WebView(
                  url: profileURL, title: S.of(context).updateUserInfor),
            ),
          );
        },
        leading: Icon(
          CupertinoIcons.profile_circled,
          size: 24,
          color: Theme.of(context).colorScheme.secondary,
        ),
        title: Text(
          S.of(context).updateUserInfor,
          style: const TextStyle(fontSize: 16),
        ),
        trailing: Icon(
          Icons.arrow_forward_ios,
          color: Theme.of(context).colorScheme.secondary,
          size: 18,
        ),
      ),
    );
  }

  Widget renderItem(value) {
    IconData icon;
    String title;
    Widget trailing;
    Function() onTap;
    var isMultiVendor = kFluxStoreMV.contains(serverConfig['type']);
    var subGeneralSetting = widget.subGeneralSetting != null
        ? ConfigurationUtils.loadSubGeneralSetting(widget.subGeneralSetting!)
        : kSubGeneralSetting;
    var item = subGeneralSetting[value];

    if (value.contains('web')) {
      return GeneralWebWidget(item: item);
    }

    if (value.contains('post-')) {
      return GeneralPostWidget(item: item);
    }

    if (value.contains('title')) {
      return GeneralTitleWidget(item: item, itemPadding: itemPadding);
    }

    if (value.contains('button')) {
      return GeneralButtonWidget(item: item);
    }

    switch (value) {
      case 'products':
        {
          return Container();
          // if (!(user != null ? user!.isVender : false) || !isMultiVendor) {
          //   return const SizedBox();
          // }
          // trailing = const Icon(
          //   Icons.arrow_forward_ios,
          //   size: 18,
          //   color: kGrey600,
          // );
          // title = S.of(context).myProducts;
          // icon = CupertinoIcons.cube_box;
          // onTap = () => Navigator.pushNamed(context, RouteList.productSell);
          // break;
        }

      case 'chat':
        {
          return Container();
          // if (user == null || Config().isListingType || !isMultiVendor) {
          //   return Container();
          // }
          // trailing = const Icon(
          //   Icons.arrow_forward_ios,
          //   size: 18,
          //   color: kGrey600,
          // );
          // title = S.of(context).conversations;
          // icon = CupertinoIcons.chat_bubble_2;
          // onTap = () => Navigator.pushNamed(context, RouteList.listChat);
          // break;
        }
      case 'wallet':
        {
          return Container();
          // if (user == null || !Config().isWooType) {
          //   return Container();
          // }
          // trailing = const Icon(
          //   Icons.arrow_forward_ios,
          //   size: 18,
          //   color: kGrey600,
          // );
          // title = S.of(context).myWallet;
          // icon = CupertinoIcons.square_favorites_alt;
          // onTap = () => FluxNavigate.pushNamed(
          //       RouteList.myWallet,
          //       forceRootNavigator: true,
          //     );
          // break;
        }
      case 'wishlist':
        {
          return Container();
          // trailing = Row(
          //   mainAxisSize: MainAxisSize.min,
          //   children: [
          //     Consumer<ProductWishListModel>(builder: (context, model, child) {
          //       if (model.products.isNotEmpty) {
          //         return Text(
          //           '${model.products.length} ${S.of(context).items}',
          //           style: TextStyle(
          //               fontSize: 14, color: Theme.of(context).primaryColor),
          //         );
          //       } else {
          //         return const SizedBox();
          //       }
          //     }),
          //     const SizedBox(width: 5),
          //     const Icon(Icons.arrow_forward_ios, size: 18, color: kGrey600)
          //   ],
          // );
          //
          // title = S.of(context).myWishList;
          // icon = CupertinoIcons.heart;
          // onTap = () => Navigator.of(context).pushNamed(RouteList.wishlist);
          // break;
        }
      case 'notifications':
        {
          return Container();
          //   return Consumer<NotificationModel>(builder: (context, model, child) {
          //     return Column(
          //       children: [
          //         Card(
          //           margin: const EdgeInsets.only(bottom: 2.0),
          //           elevation: 0,
          //           child: SwitchListTile(
          //             secondary: Icon(
          //               CupertinoIcons.bell,
          //               color: Theme.of(context).colorScheme.secondary,
          //               size: 24,
          //             ),
          //             value: model.enable,
          //             activeColor: const Color(0xFF0066B4),
          //             onChanged: (bool enableNotification) {
          //               if (enableNotification) {
          //                 model.enableNotification();
          //               } else {
          //                 model.disableNotification();
          //               }
          //             },
          //             title: Text(
          //               S.of(context).getNotification,
          //               style: const TextStyle(fontSize: 16),
          //             ),
          //           ),
          //         ),
          //         const Divider(
          //           color: Colors.black12,
          //           height: 1.0,
          //           indent: 75,
          //           //endIndent: 20,
          //         ),
          //         if (model.enable) ...[
          //           Card(
          //             margin: const EdgeInsets.only(bottom: 2.0),
          //             elevation: 0,
          //             child: GestureDetector(
          //               onTap: () {
          //                 Navigator.of(context).pushNamed(RouteList.notify);
          //               },
          //               child: ListTile(
          //                 leading: Icon(
          //                   CupertinoIcons.list_bullet,
          //                   size: 22,
          //                   color: Theme.of(context).colorScheme.secondary,
          //                 ),
          //                 title: Text(S.of(context).listMessages),
          //                 trailing: const Icon(
          //                   Icons.arrow_forward_ios,
          //                   size: 18,
          //                   color: kGrey600,
          //                 ),
          //               ),
          //             ),
          //           ),
          //           const Divider(
          //             color: Colors.black12,
          //             height: 1.0,
          //             indent: 75,
          //             //endIndent: 20,
          //           ),
          //         ],
          //       ],
          //     );
          //   });
        }
      case 'language':
        {
          return Container();
          // icon = CupertinoIcons.globe;
          // title = S.of(context).language;
          // trailing = const Icon(
          //   Icons.arrow_forward_ios,
          //   size: 18,
          //   color: kGrey600,
          // );
          // onTap = () => Navigator.of(context).pushNamed(RouteList.language);
          // break;
        }
      case 'currencies':
        {
          return Container();
          // if (Config().isListingType) {
          //   return Container();
          // }
          // icon = CupertinoIcons.money_dollar_circle;
          // title = S.of(context).currencies;
          // trailing =
          //     const Icon(Icons.arrow_forward_ios, size: 18, color: kGrey600);
          // onTap = () => Navigator.of(context).pushNamed(RouteList.currencies);
          // break;
        }
      case 'darkTheme':
        {
          return Container();
          // return Column(
          //   children: [
          //     Card(
          //       margin: const EdgeInsets.only(bottom: 2.0),
          //       elevation: 0,
          //       child: SwitchListTile(
          //         secondary: Icon(
          //           Provider.of<AppModel>(context).darkTheme
          //               ? CupertinoIcons.sun_min
          //               : CupertinoIcons.moon,
          //           color: Theme.of(context).colorScheme.secondary,
          //           size: 24,
          //         ),
          //         value: Provider.of<AppModel>(context).darkTheme,
          //         activeColor: const Color(0xFF0066B4),
          //         onChanged: (bool value) {
          //           if (value) {
          //             Provider.of<AppModel>(context, listen: false)
          //                 .updateTheme(true);
          //           } else {
          //             Provider.of<AppModel>(context, listen: false)
          //                 .updateTheme(false);
          //           }
          //         },
          //         title: Text(
          //           S.of(context).darkTheme,
          //           style: const TextStyle(fontSize: 16),
          //         ),
          //       ),
          //     ),
          //     const Divider(
          //       color: Colors.black12,
          //       height: 1.0,
          //       indent: 75,
          //       //endIndent: 20,
          //     ),
          //   ],
          // );
        }
      case 'order':
        {
          final storage = LocalStorage(LocalStorageKey.dataOrder);
          var items = storage.getItem('orders');
          if (user == null && items == null) {
            return Container();
          }
          if (Config().isListingType) {
            return const SizedBox();
          }
          icon = CupertinoIcons.time;
          title = S.of(context).orderHistory;
          trailing =
          const Icon(Icons.arrow_forward_ios, size: 18, color: kGrey600);
          onTap = () async {
            //code added for internet connectivity check on 14-7-2023 by natasha
            var isConnected = await checkInternetConnection();
            if (isConnected) {
              // Internet connection is available
              final user = Provider.of<UserModel>(context, listen: false).user;
              await FluxNavigate.pushNamed(
                RouteList.orders,
                arguments: user,
              );
            } else {
              // No internet connection
              printLog('No internet connection');
              await showDialogNotInternet(context);
            }
          };
          break;
        }
      case 'point':
        {
          if (!(kAdvanceConfig['EnablePointReward'] == true && user != null)) {
            return const SizedBox();
          }
          if (Config().isListingType) {
            return const SizedBox();
          }
          icon = CupertinoIcons.bag_badge_plus;
          title = S.of(context).myPoints;
          trailing =
          const Icon(Icons.arrow_forward_ios, size: 18, color: kGrey600);
          onTap = () => Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => UserPointScreen(),
            ),
          );
          break;
        }
      case 'rating':
        {
          icon = CupertinoIcons.doc_text;
          title = 'Terms and Conditions';
          trailing =
          const Icon(Icons.arrow_forward_ios, size: 18, color: kGrey600);
          onTap = () {
            const aboutUrl = 'https://qwickpick.com/terms-conditions';

            if (kIsWeb) {
              return Tools.launchURL(aboutUrl);
            }
            return FluxNavigate.push(
              MaterialPageRoute(
                builder: (context) =>
                const WebView(url: aboutUrl, title: 'Terms and Conditions'),
              ),
              forceRootNavigator: true,
            );
          };
          break;
        }
    // {
    //   icon = CupertinoIcons.star;
    //   title = S.of(context).rateTheApp;
    //   trailing =
    //       const Icon(Icons.arrow_forward_ios, size: 18, color: kGrey600);
    //   onTap = showRateMyApp;
    //   break;
    // }
      case 'privacy':
      // {
      //   icon = CupertinoIcons.doc_text;
      //   title = S.of(context).agreeWithPrivacy;
      //   trailing =
      //       const Icon(Icons.arrow_forward_ios, size: 18, color: kGrey600);
      //   onTap = () {
      //     final privacy = subGeneralSetting['privacy'];
      //     final pageId =
      //         privacy?.pageId ?? kAdvanceConfig['PrivacyPoliciesPageId'];
      //     String? pageUrl =
      //         privacy?.webUrl ?? kAdvanceConfig['PrivacyPoliciesPageUrl'];
      //     if (pageId != null) {
      //       Navigator.push(
      //           context,
      //           MaterialPageRoute(
      //             builder: (context) => PostScreen(
      //                 pageId: pageId,
      //                 pageTitle: S.of(context).agreeWithPrivacy),
      //           ));
      //       return;
      //     }
      //     if (pageUrl?.isNotEmpty ?? false) {
      //       ///Display multiple languages WebView
      //       var locale =
      //           Provider.of<AppModel>(context, listen: false).langCode;
      //       if (pageUrl != null && locale != null) {
      //         pageUrl += '?lang=$locale';
      //       }
      //       Navigator.push(
      //         context,
      //         MaterialPageRoute(
      //           builder: (context) => WebView(
      //             url: pageUrl,
      //             title: S.of(context).agreeWithPrivacy,
      //           ),
      //         ),
      //       );
      //     }
      //   };
      //   break;
      // }
        {
          icon = CupertinoIcons.lock_shield;
          // title = S.of(context).agreeWithPrivacy;
          title = 'Privacy Policy';
          trailing =
          const Icon(Icons.arrow_forward_ios, size: 18, color: kGrey600);
          onTap = () {
            final privacy = subGeneralSetting['privacy'];
            final pageId =
                privacy?.pageId ?? kAdvanceConfig['PrivacyPoliciesPageId'];
            String? pageUrl =
                privacy?.webUrl ?? kAdvanceConfig['PrivacyPoliciesPageUrl'];
            if (pageId != null) {
              printLog('First if');
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => PostScreen(
                        pageId: pageId,
                        pageTitle: S.of(context).agreeWithPrivacy),
                  ));
              return;
            }
            if (pageUrl?.isNotEmpty ?? false) {
              printLog('Second if');

              ///Display multiple languages WebView
              var locale =
                  Provider.of<AppModel>(context, listen: false).langCode;
              if (pageUrl != null && locale != null) {
                printLog('Third if');
                pageUrl += '?lang=$locale';
              }
              // Navigator.push(
              //   context,
              //   MaterialPageRoute(
              //     builder: (context) => WebView(
              //       url: pageUrl,
              //       title: S.of(context).agreeWithPrivacy,
              //     ),
              //   ),
              // );
              FluxNavigate.push(
                MaterialPageRoute(
                  builder: (context) => WebView(
                    // url: pageUrl, title: S.of(context).agreeWithPrivacy),
                      url: pageUrl,
                      title: 'Privacy Policy'),
                ),
                forceRootNavigator: true,
              );
            }
          };
          break;
        }
      case 'about':
        {
          icon = CupertinoIcons.info;
          title = S.of(context).aboutUs;
          trailing =
          const Icon(Icons.arrow_forward_ios, size: 18, color: kGrey600);
          onTap = () {
            final about = subGeneralSetting['about'];
            final aboutUrl = about?.webUrl ?? SettingConstants.aboutUsUrl;

            if (kIsWeb) {
              return Tools.launchURL(aboutUrl);
            }
            return FluxNavigate.push(
              MaterialPageRoute(
                builder: (context) =>
                    WebView(url: aboutUrl, title: S.of(context).aboutUs),
              ),
              forceRootNavigator: true,
            );
          };
          break;
        }
      case 'post':
        {
          if (user != null) {
            trailing = const Icon(
              Icons.arrow_forward_ios,
              size: 18,
              color: kGrey600,
            );
            title = S.of(context).postManagement;
            icon = CupertinoIcons.chat_bubble_2;
            onTap = () {
              Navigator.of(context).pushNamed(RouteList.postManagement);
            };
          } else {
            return const SizedBox();
          }

          break;
        }
      default:
        {
          trailing =
          const Icon(Icons.arrow_forward_ios, size: 18, color: kGrey600);
          icon = Icons.error;
          title = S.of(context).dataEmpty;
          onTap = () {};
        }
    }
    return Column(
      children: [
        Card(
          margin: const EdgeInsets.only(bottom: 2.0),
          elevation: 0,
          child: ListTile(
            leading: Icon(
              icon,
              color: Theme.of(context).colorScheme.secondary,
              size: 24,
            ),
            title: Text(
              title,
              style: const TextStyle(fontSize: 16),
            ),
            trailing: trailing,
            onTap: onTap,
          ),
        ),
        const Divider(
          color: Colors.black12,
          height: 1.0,
          indent: 75,
          //endIndent: 20,
        ),
      ],
    );
  }

  Widget renderDrawerIcon() {
    var icon = Icons.blur_on;
    if (widget.drawerIcon != null) {
      icon = iconPicker(
          widget.drawerIcon!['icon'], widget.drawerIcon!['fontFamily']) ??
          Icons.blur_on;
    }
    return Icon(
      icon,
      //color: Colors.white70,
      color: Colors.transparent, //16-3-2023
    );
  }

  Widget renderUser() {
    const textStyle = TextStyle(fontSize: 16);

    return ListenableProvider.value(
      value: Provider.of<UserModel>(context),
      child: Consumer<UserModel>(
        builder: (context, model, child) {
          final user = model.user;
          final loggedIn = model.loggedIn;
          return Column(children: [
            const SizedBox(height: 10.0),
            if (user != null && user.name != null)
              ListTile(
                leading: (user.picture?.isNotEmpty ?? false)
                    ? CircleAvatar(
                  backgroundImage: NetworkImage(user.picture!),
                )
                    : const Icon(Icons.face),
                title: Text(
                  user.name!.replaceAll('fluxstore', ''),
                  style: textStyle,
                ),
              ),
            // if (user != null && user.email != null && user.email!.isNotEmpty)
            //   ListTile(
            //     leading: const Icon(Icons.email),
            //     title: Text(
            //       user.email!,
            //       style: const TextStyle(fontSize: 16),
            //     ),
            //   ),
            if (user != null &&
                user.username != null &&
                user.username!.isNotEmpty)
              ListTile(
                leading:  Icon(Icons.mobile_friendly,color: Theme.of(context).colorScheme.secondary,),
                title: Text(
                  user.username!,
                  style: const TextStyle(fontSize: 16),
                ),
              ),
            // if (user != null && !Config().isWordPress)
            //   Card(
            //     color: Theme.of(context).backgroundColor,
            //     margin: const EdgeInsets.only(bottom: 2.0),
            //     elevation: 0,
            //     child: ListTile(
            //       leading: Icon(
            //         Icons.portrait,
            //         color: Theme.of(context).colorScheme.secondary,
            //         size: 25,
            //       ),
            //       title: Text(
            //         S.of(context).updateUserInfor,
            //         style: const TextStyle(fontSize: 15),
            //       ),
            //       trailing: const Icon(
            //         Icons.arrow_forward_ios,
            //         size: 18,
            //         color: kGrey600,
            //       ),
            //       onTap: () async {
            //         final hasChangePassword = await FluxNavigate.pushNamed(
            //           RouteList.updateUser,
            //           forceRootNavigator: true,
            //         ) as bool?;
            //
            //         /// If change password with Shopify
            //         /// need to log out and log in again
            //         if (Config().isShopify && (hasChangePassword ?? false)) {
            //           _showDialogLogout();
            //         }
            //       },
            //     ),
            //   ),
            if (user == null)
              Card(
                color: Theme.of(context).backgroundColor,
                margin: const EdgeInsets.only(bottom: 2.0),
                elevation: 0,
                child: ListTile(
                  onTap: () {
                    if (!loggedIn) {
                      Navigator.of(
                        App.fluxStoreNavigatorKey.currentContext!,
                      ).pushNamed(RouteList.login);
                      return;
                    }
                    Provider.of<UserModel>(context, listen: false).logout();
                    if (kLoginSetting['IsRequiredLogin'] ?? false) {
                      Navigator.of(
                        App.fluxStoreNavigatorKey.currentContext!,
                      ).pushNamedAndRemoveUntil(
                        RouteList.login,
                            (route) => false,
                      );
                    }
                  },
                  leading: const Icon(Icons.person),
                  title: Text(
                    loggedIn
                        ? S.of(context).logout
                        : 'Login / Sign-in', //S.of(context).login,
                    style: const TextStyle(fontSize: 16),
                  ),
                  trailing: const Icon(Icons.arrow_forward_ios,
                      size: 18, color: kGrey600),
                ),
              ),
            // if (user != null)
            //   Card(
            //     color: Theme.of(context).backgroundColor,
            //     margin: const EdgeInsets.only(bottom: 2.0),
            //     elevation: 0,
            //     child: ListTile(
            //       onTap: _onLogout,
            //       leading: Icon(
            //         Icons.logout,
            //         size: 20,
            //         color: Theme.of(context).colorScheme.secondary,
            //       ),
            //
            //       // Image.asset(
            //       //   'assets/icons/profile/icon-logout.png',
            //       //   width: 24,
            //       //   color: Theme.of(context).colorScheme.secondary,
            //       // ),
            //       title: Text(
            //         S.of(context).logout,
            //         style: const TextStyle(fontSize: 16),
            //       ),
            //       trailing: const Icon(Icons.arrow_forward_ios,
            //           size: 18, color: kGrey600),
            //     ),
            // ),
          ]);
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    var settings = widget.settings ?? kDefaultSettings;
    var background = widget.background ?? kProfileBackground;
    id = user?.id;
    userIdUrl =
    //  'https://qwickpick.com/wp-json/api/flutter_user/delete_account?id=$id';
    '$baseUrl/wp-json/api/flutter_user/delete_account?id=$id';

    final appBar = (showAppBar(RouteList.profile))
        ? sliverAppBarWidget
        : SliverAppBar(
      backgroundColor: Theme.of(context).primaryColor,
      leading: IconButton(
        icon: renderDrawerIcon(),
        onPressed: () => NavigateTools.onTapOpenDrawerMenu(context),
      ),
      expandedHeight: 100, //bannerHigh,
      floating: true,
      pinned: true,
      flexibleSpace: const FlexibleSpaceBar(
        title: Text(
          // S.of(context).settings,
          'Profile',
          style: TextStyle(
              fontSize: 18,
              color: Colors.white,
              fontWeight: FontWeight.w600),
        ),
        // background: FluxImage(
        //   imageUrl: background,
        //   fit: BoxFit.cover,
        // ),
      ),
      actions: Navigator.canPop(context)
          ? [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: GestureDetector(
            onTap: () => Navigator.pop(context),
            child: const Icon(
              Icons.close,
              color: Colors.white,
            ),
          ),
        ),
      ]
          : null,
    );

    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: CustomScrollView(
        slivers: <Widget>[
          appBar,
          SliverList(
            delegate: SliverChildListDelegate(
              <Widget>[
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 15.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      if (!widget.hideUser) renderUser(),
                      const SizedBox(height: 30.0),
                      const Text(
                        //S.of(context).generalSetting,
                        'General Settings',
                        style: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.w600),
                      ),
                      const SizedBox(height: 10.0),
                      renderVendorAdmin(),

                      /// Render some extra menu for Vendor.
                      /// Currently support WCFM & Dokan. Will support WooCommerce soon.
                      if (kFluxStoreMV.contains(serverConfig['type']) &&
                          (user?.isVender ?? false)) ...[
                        Services().widget.renderVendorOrder(context),
                        renderVendorVacation(),
                      ],

                      renderDeliveryBoy(),

                      /// Render custom Wallet feature
                      // renderWebViewProfile(),

                      /// render some extra menu for Listing
                      if (user != null && Config().isListingType) ...[
                        Services().widget.renderNewListing(context),
                        Services().widget.renderBookingHistory(context),
                      ],

                      const SizedBox(height: 10.0),
                      if (user != null)
                        const Divider(
                          color: Colors.black12,
                          height: 1.0,
                          indent: 75,
                          //endIndent: 20,
                        ),
                    ],
                  ),
                ),

                /// render list of dynamic menu
                /// this could be manage from the Fluxbuilder
                ...List.generate(
                  settings.length,
                      (index) {
                    var item = settings[index];
                    var isTitle = item.contains('title');
                    return Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: isTitle ? 0.0 : itemPadding),
                      child: renderItem(item),
                    );
                  },
                ),
                if (user != null)
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 15.0),
                    child: Card(
                      color: Theme.of(context).backgroundColor,
                      margin: const EdgeInsets.only(bottom: 2.0),
                      elevation: 0,
                      child: ListTile(
                        onTap: _showDialogLogout,//_onLogout,
                        leading: Icon(
                          Icons.logout,
                          size: 20,
                          color: Theme.of(context).colorScheme.secondary,
                        ),

                        // Image.asset(
                        //   'assets/icons/profile/icon-logout.png',
                        //   width: 24,
                        //   color: Theme.of(context).colorScheme.secondary,
                        // ),
                        title: Text(
                          S.of(context).logout,
                          style: const TextStyle(fontSize: 16),
                        ),
                        trailing: const Icon(Icons.arrow_forward_ios,
                            size: 18, color: kGrey600),
                      ),
                    ),
                  ),
                if (user != null)
                  const Divider(
                    color: Colors.black12,
                    height: 1.0,
                    indent: 75,
                    //endIndent: 20,
                  ),
                const SizedBox(height: 3.0),
                if (user != null)
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 15.0),
                    child: Card(
                      color: Theme.of(context).backgroundColor,
                      margin: const EdgeInsets.only(bottom: 2.0),
                      elevation: 0,
                      child: ListTile(
                        onTap: () async {
                          await showDialog(
                            context: context,
                            builder: (context) => AlertDialog(
                              insetPadding: const EdgeInsets.all(20.0),
                              title: Row(
                                mainAxisAlignment:
                                MainAxisAlignment.spaceBetween,
                                children: [
                                  const Text('Delete Account'),
                                  TextButton(
                                    onPressed: () => Navigator.pop(context),
                                    child: CircleAvatar(
                                        radius: 14,
                                        backgroundColor: Theme.of(context).primaryColor,
                                        child: const Icon(Icons.clear,size: 15,color: kDarkBG,)),
                                  ),
                                ],
                              ),
                              content: SizedBox(
                                width: MediaQuery.of(context).size.width * 0.8,
                                child: SingleChildScrollView(
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    children: const [
                                      Text(
                                        'Close Your QwickPick Account ',
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 16),
                                      ),
                                      SizedBox(
                                        height: 8,
                                      ),
                                      Text('Please read this carefully '),
                                      SizedBox(
                                        height: 8,
                                      ),
                                      Text(
                                          'You are about to permanently close your Qwick Pick account and delete your data. '
                                              'Once your account is closed, all of the products and services accessed through your account will no longer be available to you, '
                                              'across any Qwick Pick sites globally. For example, closing your account through this app will also close your account on Qwick Pick.com, Qwick Pick applications across all plat'
                                              'forms, and all other global sites to the extent'
                                              ' you use the same credentials to access services and products offered through those sites. '),
                                      SizedBox(
                                        height: 8,
                                      ),
                                      Text(
                                          'If you proceed with this, you will not be able to access products and services associated with your closed account.'),
                                      SizedBox(
                                        height: 8,
                                      ),
                                      Text(
                                        'Account Closure Is A Permanent Action ',
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold),
                                      ),
                                      SizedBox(
                                        height: 8,
                                      ),
                                      Text(
                                          'Please note account closure is a permanent action and once your account is closed it will no longer be available to you and cannot be restored. If you decide later that you want to start ordering from us again or if you would like to use products and services that require an account, you will need to create a new account. '),
                                      SizedBox(
                                        height: 8,
                                      ),
                                      Text(
                                        'Note ',
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold),
                                      ),
                                      SizedBox(
                                        height: 8,
                                      ),
                                      Text(
                                          'Please be advised that Qwick Pick (GreenWash Agro Private Limited) is legally required or entitled to retain some types of data, such as order history. We do this in line with applicable laws including for tax and accounting and fraud prevention purposes.'),
                                      //  SizedBox(height: 8,),
                                    ],
                                  ),
                                ),
                              ),
                              actions: [
                                Row(
                                  children: [
                                    Expanded(
                                      child: ButtonTheme(
                                        height: 45,
                                        child: OutlinedButton(
                                          style: OutlinedButton.styleFrom(
                                            shape:
                                            const RoundedRectangleBorder(),
                                          ),
                                          onPressed: () async {
                                            printLog('userid');
                                            printLog(user?.id);
                                            //code added for internet connectivity check on 14-7-2023 by natasha
                                            var isConnected = await checkInternetConnection();
                                            if (isConnected) {
                                              // Internet connection is available
                                              printLog('Connected to the internet');
                                              await showDialog(
                                                context: context,
                                                builder: (context) => AlertDialog(
                                                  title: Text(
                                                      S.of(context).areYouSure),
                                                  content: const Text(
                                                      'You want to delete your account'),
                                                  actions: <Widget>[
                                                    TextButton(
                                                      onPressed: () =>
                                                          Navigator.pop(context),
                                                      child:
                                                      Text(S.of(context).no),
                                                    ),
                                                    TextButton(
                                                      onPressed: () async {
                                                        var id = user?.id;
                                                        Provider.of<CartModel>(
                                                            context,
                                                            listen: false)
                                                            .clearAddress();
                                                        await Provider.of<
                                                            UserModel>(
                                                            context,
                                                            listen: false)
                                                            .logout();
                                                        printLog('user id $id');
                                                        var endP = userIdUrl;
                                                        final response =
                                                        await httpGet(
                                                          endP.toUri()!,
                                                        );
                                                        printLog('Response Var');
                                                        printLog(response.body);
                                                        if (response.statusCode ==
                                                            200) {
                                                          var boolValue =
                                                              response.body;
                                                          if (boolValue ==
                                                              'true') {
                                                            printLog('success');
                                                            Navigator.pop(
                                                                context);
                                                            Navigator.pop(
                                                                context);
                                                            ScaffoldMessenger.of(
                                                                context)
                                                                .showSnackBar(
                                                              const SnackBar(
                                                                content: Text(
                                                                    'Your account deleted successfully!'),
                                                              ),
                                                            );
                                                          } else {
                                                            ScaffoldMessenger.of(
                                                                context)
                                                                .showSnackBar(
                                                              const SnackBar(
                                                                content: Text(
                                                                    'There was an error. Please try again.'),
                                                              ),
                                                            );
                                                          }
                                                        } else {
                                                          ScaffoldMessenger.of(
                                                              context)
                                                              .showSnackBar(
                                                            const SnackBar(
                                                              content: Text(
                                                                  'There was an error. Please try again.'),
                                                            ),
                                                          );
                                                        }
                                                      },
                                                      child:
                                                      Text(S.of(context).yes),
                                                    ),
                                                  ],
                                                ),
                                              );
                                            }else{
                                              printLog('No internet connection');
                                              await showDialogNotInternet(context);
                                            }
                                            // await showDialog(
                                            //   context: context,
                                            //   builder: (context) => AlertDialog(
                                            //     title: Text(
                                            //         S.of(context).areYouSure),
                                            //     content: const Text(
                                            //         'You want to delete your account'),
                                            //     actions: <Widget>[
                                            //       TextButton(
                                            //         onPressed: () =>
                                            //             Navigator.pop(context),
                                            //         child:
                                            //             Text(S.of(context).no),
                                            //       ),
                                            //       TextButton(
                                            //         onPressed: () async {
                                            //           var id = user?.id;
                                            //           Provider.of<CartModel>(
                                            //                   context,
                                            //                   listen: false)
                                            //               .clearAddress();
                                            //           await Provider.of<
                                            //                       UserModel>(
                                            //                   context,
                                            //                   listen: false)
                                            //               .logout();
                                            //           print('user id $id');
                                            //           // var endP =
                                            //           //     'https://qwickpick.com/wp-json/api/flutter_user/delete_account/?id=${id}';
                                            //           var endP = userIdUrl;
                                            //           final response =
                                            //               await httpGet(
                                            //             endP.toUri()!,
                                            //           );
                                            //           print('Response Var');
                                            //           print(response.body);
                                            //           if (response.statusCode ==
                                            //               200) {
                                            //             var boolValue =
                                            //                 response.body;
                                            //             if (boolValue ==
                                            //                 'true') {
                                            //               print('success');
                                            //               Navigator.pop(
                                            //                   context);
                                            //               Navigator.pop(
                                            //                   context);
                                            //               ScaffoldMessenger.of(
                                            //                       context)
                                            //                   .showSnackBar(
                                            //                 const SnackBar(
                                            //                   content: Text(
                                            //                       'Your account deleted successfully!'),
                                            //                 ),
                                            //               );
                                            //             } else {
                                            //               ScaffoldMessenger.of(
                                            //                       context)
                                            //                   .showSnackBar(
                                            //                 const SnackBar(
                                            //                   content: Text(
                                            //                       'There was an error. Please try again.'),
                                            //                 ),
                                            //               );
                                            //             }
                                            //           } else {
                                            //             ScaffoldMessenger.of(
                                            //                     context)
                                            //                 .showSnackBar(
                                            //               const SnackBar(
                                            //                 content: Text(
                                            //                     'There was an error. Please try again.'),
                                            //               ),
                                            //             );
                                            //           }
                                            //         },
                                            //         child:
                                            //             Text(S.of(context).yes),
                                            //       ),
                                            //     ],
                                            //   ),
                                            // );
                                          },
                                          child: const Text(
                                            'Delete My Account',
                                            style: TextStyle(color: Colors.red),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          );
                        },
                        leading: Icon(
                          Icons.delete,
                          size: 20,
                          color: Theme.of(context).colorScheme.secondary,
                        ),
                        title: const Text(
                          'Delete account', //S.of(context).login,
                          style: TextStyle(fontSize: 16),
                        ),
                        trailing: const Icon(Icons.arrow_forward_ios,
                            size: 18, color: kGrey600),
                      ),
                    ),
                  ),
                const SizedBox(height: 100),
              ],
            ),
          ),
        ],
      ),
    );
  }

  void _onLogout() {
    Provider.of<CartModel>(context, listen: false).clearAddress();
    Provider.of<UserModel>(context, listen: false).logout();
    if (kLoginSetting['IsRequiredLogin'] ?? false) {
      Navigator.of(App.fluxStoreNavigatorKey.currentContext!)
          .pushNamedAndRemoveUntil(
        RouteList.login,
            (route) => false,
      );
    }
  }

  void _showDialogLogout(){
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text(
            S.of(context).areYouSure),
        content: const Text(
            'You want to logout your account'),
        actions: <Widget>[
          TextButton(
            onPressed: () =>
                Navigator.pop(context),
            child:
            Text(S.of(context).no),
          ),
          TextButton(
            onPressed: () async {
              Navigator.of(context).pop();
              Future.delayed(const Duration(milliseconds: 200), _onLogout);
              // ScaffoldMessenger.of(
              //     context)
              //     .showSnackBar(
              //   const SnackBar(
              //     content: Text(
              //         'Your account deleted successfully!'),
              //   ),
              // );
            },
            child:
            Text(S.of(context).yes),
          ),
        ],
      ),
    );
  }

  void _showDialogLogout1() {
    showCupertinoDialog(
      context: context,
      builder: (ctx) => CupertinoAlertDialog(
        title: const Text('Notice'),
        content: const Text('You need to login again to effect update.'),
        actions: [
          CupertinoDialogAction(
            isDefaultAction: true,
            onPressed: () {
              Navigator.of(ctx).pop();
              Future.delayed(const Duration(milliseconds: 200), _onLogout);
            },
            child: const Text('OK'),
          )
        ],
      ),
    );
  }
}
