import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../common/constants.dart';
import '../../generated/l10n.dart';
import '../../models/cart/cart_base.dart';
import '../../models/entities/user.dart';
import '../../models/user_model.dart';


class DeleteAccountScreen extends StatefulWidget {
  const DeleteAccountScreen({Key? key}) : super(key: key);

  @override
  State<DeleteAccountScreen> createState() => _DeleteAccountScreenState();
}

class _DeleteAccountScreenState extends State<DeleteAccountScreen> {

  User? get user => Provider.of<UserModel>(context, listen: false).user;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: AppBar(
        title: const Text(
          //S.of(context).listMessages,
          'Delete Account',
          style: TextStyle(color: Colors.white),
        ),
        leading: Center(
          child: GestureDetector(
            onTap: () => Navigator.pop(context),
            child: const Icon(
              Icons.arrow_back_ios,
              color: Colors.white,
            ),
          ),
        ),
        centerTitle: true,
        backgroundColor: Theme.of(context).primaryColor,
        elevation: 0,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0,vertical: 26),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text('Close Your QwickPick Account ',style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16),),
              const SizedBox(height: 8,),
              const Text('Please read this carefully '),
              const SizedBox(height: 8,),
              const Text('You are about to permanently close your Qwick Pick account and delete your data. '
                  'Once your account is closed, all of the products and services accessed through your account will no longer be available to you, '
                  'across any Qwick Pick sites globally. For example, closing your account through this app will also close your account on Qwick Pick.com, Qwick Pick applications across all plat'
                  'forms, and all other global sites to the extent'
                  ' you use the same credentials to access services and products offered through those sites. '),
              const SizedBox(height: 8,),
              const Text('If you proceed with this, you will not be able to access products and services associated with your closed account.'),
              const SizedBox(height: 8,),
              const Text('Account Closure Is A Permanent Action ',style: TextStyle(fontWeight: FontWeight.bold),),
              const SizedBox(height: 8,),
              const Text('Please note account closure is a permanent action and once your account is closed it will no longer be available to you and cannot be restored. If you decide later that you want to start ordering from us again or if you would like to use products and services that require an account, you will need to create a new account. '),
              const SizedBox(height: 8,),
              const Text('Note ',style: TextStyle(fontWeight: FontWeight.bold),),
              const SizedBox(height: 8,),
              const Text('Please be advised that Qwick Pick (GreenWash Agro Private Limited) is legally required or entitled to retain some types of data, such as order history. We do this in line with applicable laws including for tax and accounting and fraud prevention purposes.'),
              const SizedBox(height: 18,),
              Row(
                children: [
                  Expanded(
                    child: ButtonTheme(
                      height: 45,
                      child: OutlinedButton(
                        style: OutlinedButton.styleFrom(
                          shape: const RoundedRectangleBorder(),
                        ),
                        onPressed: () async{
                          printLog('userid');
                          printLog(user?.id);
                          await showDialog(
                            context: context,
                            builder: (context) => AlertDialog(
                              title: Text(S.of(context).areYouSure),
                              content:const Text('You want to delete your account'),
                              actions: <Widget>[
                                TextButton(
                                  onPressed: () => Navigator.pop(context),
                                  child: Text(S.of(context).no),
                                ),
                              TextButton(
                                  onPressed: () async{
                                    var id = user?.id;
                                    Provider.of<CartModel>(context, listen: false).clearAddress();
                                    await Provider.of<UserModel>(context, listen: false).logout();
                                    printLog('user id $id');
                                    var endP =
                                        'https://qwickpick.com/wp-json/api/flutter_user/delete_account/?id=$id';
                                    final response = await httpGet(
                                      endP.toUri()!,
                                    );
                                    printLog('Response Var');
                                   printLog(response.body);
                                   if (response.statusCode == 200) {
                                       var boolValue = response.body;
                                      if (boolValue == 'true') {
                                        printLog('success');
                                        ScaffoldMessenger.of(context).showSnackBar(
                                          const SnackBar(
                                            content: Text('Your account deleted successfully!'),
                                          ),
                                        );
                                      }else{
                                        ScaffoldMessenger.of(context).showSnackBar(
                                          const SnackBar(
                                            content: Text('There was an error. Please try again.'),
                                          ),
                                        );
                                      }
                                   }else{
                                     ScaffoldMessenger.of(context).showSnackBar(
                                       const SnackBar(
                                         content: Text('There was an error. Please try again.'),
                                       ),
                                     );
                                   }
                                  },
                                  child: Text(S.of(context).yes),
                                ),
                              ],
                            ),
                          );
                        },
                        child: const Text(
                          'Delete My Account',
                          style: TextStyle(
                              color: Colors.red),
                        ),
                      ),
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
