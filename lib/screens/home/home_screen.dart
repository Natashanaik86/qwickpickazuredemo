import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:package_info_plus/package_info_plus.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../common/config.dart';
import '../../common/constants.dart';
import '../../models/app_model.dart';
import '../../modules/dynamic_layout/index.dart';
import '../../services/index.dart';
import '../../widgets/home/index.dart';
import '../../widgets/home/preview_reload.dart';
import '../../widgets/home/wrap_status_bar.dart';
import '../base_screen.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen();

  @override
  State<StatefulWidget> createState() {
    return _HomeScreenState();
  }
}

class _HomeScreenState extends BaseScreen<HomeScreen>
    with AutomaticKeepAliveClientMixin<HomeScreen> {
  @override
  bool get wantKeepAlive => true;
  var playStoreUrl =
      'https://play.google.com/store/apps/details?id=com.qwickpickapp';
  var appStoreUrl = 'https://apps.apple.com/in/app/qwick-pick/id1495548498';

  @override
  void dispose() {
    printLog('[Home] dispose');
    super.dispose();
  }

  @override
  void initState() {
    printLog('[Home] initState');
    super.initState();
     callVersionCheckFunction(); //code comment on 12-7-2023 for stop pop up force update
  }

  Future callVersionCheckFunction() async {
    var packageInfo = await PackageInfo.fromPlatform();
    var packageVersion = packageInfo.version; //current install version
    var packageNumber = packageInfo.buildNumber;
    var osType;
    printLog('packageInfo.version');
    printLog(packageInfo.version);
    printLog(packageInfo.buildNumber);

    if (Platform.isAndroid) {
      osType = 'android';
    } else if (Platform.isIOS) {
      osType = 'iOS';
    }

    final jsonBody = {
      'buildVersionCode': packageNumber,
      'buildVersionName': packageVersion,
      'osType': osType
    };

    printLog('Json Body is');
    printLog(jsonBody);

    var url =
        // 'https://qwickpick.com/wp-json/api/flutter_woo/force_update?buildVersionCode=$packageNumber&buildVersionName=$packageVersion&osType=$osType';
        'https://qwickpick.com/wp-json/api/flutter_woo/force_update';
    printLog(url);
    final response = await http.post(
      Uri.parse(url),
      body: jsonEncode(jsonBody),
    );
    if (response.statusCode == 200) {
      printLog('resp is');
      printLog(response.body);
      if (response.body == 'true') {
        if (Platform.isAndroid) {
          customShowUpdateDialog(context, playStoreUrl);
        } else if (Platform.isIOS) {
          customShowUpdateDialog(context, appStoreUrl);
        }
      }
    }
  }

  void customShowUpdateDialog(BuildContext context, String getStoreUrl) {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) => WillPopScope(
        onWillPop: () async {
          return Future.value(false);
        },
        child: AlertDialog(
          title: const Text('Please update the app.'),
          content: SingleChildScrollView(
            child: ListBody(
              children: const <Widget>[
                Text('A new upgrade for Qwickpick app is available!'),
                Text('Please update the app to continue shopping.'),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: const Text('Update'),
              onPressed: () async {
                await launch(getStoreUrl);
                Navigator.of(context).pop();
              },
            ),
          ],
        ),
      ),
    ).then((value) {
      printLog('VALUE OBTAINED IS $value');
      customShowUpdateDialog(context, playStoreUrl);
    });
  }

  @override
  Future<void> afterFirstLayout(BuildContext context) async {
    /// init dynamic link
    if (!kIsWeb) {
      Services().firebase.initDynamicLinkService(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    printLog('[Home] build');
    return Consumer<AppModel>(
      builder: (context, value, child) {
        if (value.appConfig == null) {
          return kLoadingWidget(context);
        }

        return PreviewReload(
          configs: value.appConfig!.jsonData,
          builder: (json) {
            var isStickyHeader = value.appConfig!.settings.stickyHeader;
            var appConfig = AppConfig.fromJson(json);
            final horizontalLayoutList = List.from(json['HorizonLayout']);
            final isShowAppbar = horizontalLayoutList.isNotEmpty &&
                horizontalLayoutList.first['layout'] == 'logo';
            return Scaffold(
              backgroundColor: Theme.of(context).backgroundColor,
              body: Stack(
                children: <Widget>[
                  if (appConfig.background != null)
                    isStickyHeader
                        ? SafeArea(
                            child: HomeBackground(config: appConfig.background),
                          )
                        : HomeBackground(config: appConfig.background),
                  HomeLayout(
                    isPinAppBar: isStickyHeader,
                    isShowAppbar: isShowAppbar,
                    showNewAppBar:
                        appConfig.appBar?.shouldShowOn(RouteList.home) ?? false,
                    configs: json,
                    key: UniqueKey(),
                  ),
                  if (Config().isBuilder) const WrapStatusBar(),
                ],
              ),
            );
          },
        );
      },
    );
  }
}
