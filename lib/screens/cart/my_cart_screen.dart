import 'dart:async';

import 'package:connectivity/connectivity.dart';
import 'package:flash/flash.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:inspireui/inspireui.dart' show AutoHideKeyboard, printLog;
import 'package:provider/provider.dart';

import '../../common/config.dart';
import '../../common/constants.dart';
import '../../common/tools.dart';
import '../../generated/l10n.dart';
import '../../menu/index.dart' show MainTabControlDelegate;
import '../../models/index.dart'
    show
        AppModel,
        CartModel,
        //PaymentMethodModel,
        Product,
        //Tax,
        // TaxModel,
        UserModel;
import '../../routes/flux_navigate.dart';
import '../../services/index.dart';
import '../../widgets/common/dialogs.dart';
import '../../widgets/product/cart_item.dart';
import '../../widgets/product/product_bottom_sheet.dart';
import '../checkout/checkout_screen.dart';
import '../checkout/widgets/payment_methods.dart';
import 'widgets/empty_cart.dart';
import 'widgets/wishlist.dart';

CartModel? cartModel;

class MyCart extends StatefulWidget {
  final bool? isModal;
  final bool? isBuyNow;

  const MyCart({
    this.isModal,
    this.isBuyNow = false,
  });

  @override
  _MyCartState createState() => _MyCartState();
}

class _MyCartState extends State<MyCart> with SingleTickerProviderStateMixin {
  bool isLoading = false;
  String errMsg = '';
  bool isLoading1 = true;
  // code comment on 19-7-2023 for user can checkout without checking product error by natasha
  // bool isLoading2 = false; //checkout text change
  // bool isLoading3 = false; // shopping cart text change
  // code comment end on 19-7-2023 for user can checkout without checking product error by natasha
  //shopping cart text

  CartModel get cartModel => Provider.of<CartModel>(context, listen: false);

  Map<String, dynamic>? defaultCurrency = kAdvanceConfig['DefaultCurrency'];

  List<Widget> _createShoppingCartRows(CartModel model, BuildContext context) {
    // code added for remove from cart with 0 quantity item by natasha
    List toRemove = [];

    for (var key in model.productsInCart.keys) {
      if (model.productsInCart[key] == 0) {
        printLog('remove 0 quantity1');
        // model.removeItemFromCart(key);
        toRemove.add(key);
      }
    }

    SchedulerBinding.instance?.addPostFrameCallback((_) {
      for (var key in toRemove) {
        if (model.productsInCart[key] == 0) {
          printLog('remove 0 quantity2');
          model.removeItemFromCart(key);
          // toRemove.add(key);
        }
      }
    });

    if (isLoading1) {
      model.getErrorData();
      // model.getErrorDataInCart();//22-5-2023
      isLoading1 = false;
    }

    return model.productsInCart.keys.map(
      (key) {
        // print( model.productsInCart[key]);
        // print(key);
        var productId = Product.cleanProductID(key);
        var product = model.getProductById(productId);
        if (product != null) {
          // printLog('product.stockQuantity');
          // printLog(product.stockQuantity);
          return ShoppingCartRow(
            product: product,
            //  errorProduct: model.errMessage1,//code added on 28-4-2023 // code commenting at 3-5-2023 for now need to create api for this section
            addonsOptions: model.productAddonsOptionsInCart[key],
            variation: model.getProductVariationById(key),
            quantity: model.productsInCart[key],
            options: model.productsMetaDataInCart[key],
            onRemove: () {
              model.removeItemFromCart(key);
              // printLog('model.productsInCart[key]');
              // printLog(key);
            },
            onChangeQuantity: (val) {
              printLog('value $val');
              var message =
                  model.updateQuantity(product, key, val, context: context);
              if (message.isNotEmpty) {
                final snackBar = SnackBar(
                  content: Text(message),
                  duration: const Duration(seconds: 1),
                );
                Future.delayed(
                  const Duration(milliseconds: 300),
                  // ignore: deprecated_member_use
                  () => Scaffold.of(context).showSnackBar(snackBar),
                );
              }
            },
          );
        }
        return const SizedBox();
      },
    ).toList();
  }

  void _loginWithResult(BuildContext context) async {
    await FluxNavigate.pushNamed(
      RouteList.login,
      forceRootNavigator: true,
    ).then((value) {
      final user = Provider.of<UserModel>(context, listen: false).user;
      if (user != null && user.name != null) {
        Tools.showSnackBar(
            Scaffold.of(context), S.of(context).welcome + ' ${user.name} !');
        setState(() {});
      }
    });
  }

  Future<void> _refreshData() async {
    // Simulate a delay to fetch new data
    await Future.delayed(const Duration(seconds: 1));
    setState(() {});
  }

  //code added for internet connectivity check on 13-7-2023 by natasha
  Future<bool> checkInternetConnection() async {
    printLog('in check internet connection');
    var connectivityResult = await (Connectivity().checkConnectivity());
    return connectivityResult != ConnectivityResult.none;
  }
  //code ended on 13-7-2023

  @override
  Widget build(BuildContext context) {
    printLog('[Cart] build');
    // printLog('isRefreshCart $isRefreshCart');
    // if(isRefreshCart == true){
      _refreshData();
    // }
    final localTheme = Theme.of(context);
    final screenSize = MediaQuery.of(context).size;
    var layoutType = Provider.of<AppModel>(context).productDetailLayout;
    final ModalRoute<dynamic>? parentRoute = ModalRoute.of(context);
    final canPop = parentRoute?.canPop ?? false;
    final currency = Provider.of<AppModel>(context).currency;
    final currencyRate = Provider.of<AppModel>(context).currencyRate;
    final smallAmountStyle = TextStyle(
        color: Theme.of(context).colorScheme.secondary,
        fontWeight: FontWeight.w500);
    final largeAmountStyle = TextStyle(
        color: Theme.of(context).colorScheme.secondary,
        fontSize: 18,
        fontWeight: FontWeight.w600);

    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      floatingActionButton: Selector<CartModel, bool>(
        selector: (_, cartModel) => cartModel.calculatingDiscount,
        builder: (context, calculatingDiscount, child) {
          return FloatingActionButton.extended(
            onPressed: calculatingDiscount
                ? null
                : () async {
                    //13-7-2023 code added for check internet connectivity by natasha
                    var isConnected = await checkInternetConnection();
                    if (isConnected) {
                      // Internet connection is available
                      printLog('Connected to the internet');
                      if (kAdvanceConfig['AlwaysShowTabBar'] ?? false) {
                        MainTabControlDelegate.getInstance().changeTab('cart');
                        // return;
                      }
                      // setState(() {
                      //   isRefreshCart = false;
                      // });
                      // code comment on 19-7-2023 for user can checkout without checking product error by natasha
                      // setState(() {
                      //   isLoading2 = true;
                      //   isLoading3 = true;
                      // });
                      // code comment end on 19-7-2023 for user can checkout without checking product error by natasha
                      await onCheckout(cartModel);
                    } else {
                      // No internet connection
                      printLog('No internet connection');
                      await showDialogNotInternet(context);
                      // setState(() {
                      //   isRefreshCart = false;
                      // });
                    }
                    // if (kAdvanceConfig['AlwaysShowTabBar'] ?? false) {
                    //   MainTabControlDelegate.getInstance().changeTab('cart');
                    //   // return;
                    // }
                    // setState(() {
                    //   isLoading2 = true;
                    //   isLoading3 = true;
                    // });
                    // onCheckout(cartModel);
                    //13-7-2023
                  },
            elevation: 0,
            isExtended: true,
            extendedTextStyle: const TextStyle(
              letterSpacing: 0.8,
              fontSize: 11,
              fontWeight: FontWeight.w600,
            ),
            extendedPadding:
                const EdgeInsets.symmetric(vertical: 0, horizontal: 60),
            backgroundColor: Theme.of(context).primaryColor,
            foregroundColor: Colors.white,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(9.0),
            ),
            materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
            label: child!,
          );
        },
        child: Selector<CartModel, int>(
          selector: (_, carModel) => cartModel.totalCartQuantity,
          builder: (context, totalCartQuantity, child) {
            return Row(
              children: [
                totalCartQuantity > 0
                    ?
                    // code comment on 19-7-2023 for user can checkout without checking product error by natasha
                    // ((isLoading2 && isLoading3) //isLoading //12-5-2023
                    //         ? Text(S.of(context).loading,
                    //   style: const TextStyle(fontWeight: FontWeight.w700,color: kDarkBG,fontSize: 14),):
                    // code comment end on 19-7-2023 for user can checkout without checking product error by natasha
                    Text(
                        S.of(context).checkout,
                        style: const TextStyle(
                            fontWeight: FontWeight.w700,
                            color: kDarkBG,
                            fontSize: 14),
                      )
                    //)// code comment on 19-7-2023 for user can checkout without checking product error by natasha
                    : Text(
                        S.of(context).startShopping,
                        style: const TextStyle(
                            fontWeight: FontWeight.w600,
                            color: kDarkBG,
                            fontSize: 16),
                      ),
                const SizedBox(width: 3),
                // const Icon(CupertinoIcons.right_chevron, size: 12),
              ],
            );
          },
        ),
      ),
      floatingActionButtonLocation:
          FloatingActionButtonLocation.miniCenterFloat,
      body: isErrorLoading ? Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: const [
          Center(child: CircularProgressIndicator()),
        ],//code added for show loading when coming from payment page by natasha on 26-7-23
      ) : RefreshIndicator(
        onRefresh: _refreshData,//_refreshData related code added by gaurav for refresh cart page when when error occur
        child: CustomScrollView(
          slivers: [
            SliverAppBar(
              pinned: true,
              centerTitle: false,
              leading: widget.isModal == true
                  ? CloseButton(
                      onPressed: () {
                        if (widget.isBuyNow!) {
                          Navigator.of(context).pop();
                          return;
                        }

                        if (Navigator.of(context).canPop() &&
                            layoutType != 'simpleType') {
                          Navigator.of(context).pop();
                        } else {
                          ExpandingBottomSheet.of(context, isNullOk: true)
                              ?.close();
                        }
                      },
                    )
                  : canPop
                      ? const BackButton()
                      : null,
              backgroundColor: Theme.of(context).backgroundColor,
              title: Text(
                S.of(context).myCart,
                //style: const TextStyle(fontWeight: FontWeight.w700,color: kDarkBG),
                style: TextStyle(
                    color: Theme.of(context).colorScheme.secondary,
                    fontSize: 20,
                    fontWeight: FontWeight.w500),
              ),
            ),
            SliverToBoxAdapter(
              child: Consumer<CartModel>(
                builder: (context, model, child) {
                  return AutoHideKeyboard(
                    child: Container(
                      decoration: BoxDecoration(
                          color: Theme.of(context).backgroundColor),
                      child: SingleChildScrollView(
                        child: Padding(
                          padding: const EdgeInsets.only(bottom: 80.0),
                          child: Column(
                            children: [
                              if (model.totalCartQuantity > 0)
                                Container(
                                  // decoration: BoxDecoration(
                                  //     color: Theme.of(context).primaryColorLight),
                                  padding: const EdgeInsets.only(
                                    right: 15.0,
                                    top: 4.0,
                                  ),
                                  child: SizedBox(
                                    width: screenSize.width,
                                    child: SizedBox(
                                      width: screenSize.width /
                                          (2 /
                                              (screenSize.height /
                                                  screenSize.width)),
                                      child: Row(
                                        children: [
                                          const SizedBox(width: 25.0),
                                          Text(
                                            S.of(context).total.toUpperCase(),
                                            style: localTheme
                                                .textTheme.subtitle2!
                                                .copyWith(
                                              fontWeight: FontWeight.w600,
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .secondary,
                                              // Theme.of(context).primaryColor,
                                              // fontSize: 14,
                                            ),
                                          ),
                                          const SizedBox(width: 8.0),
                                          Text(
                                            model.totalCartQuantity == 1
                                                ? '${model.totalCartQuantity} ${S.of(context).item}'
                                                : '${model.totalCartQuantity} ${S.of(context).items}',
                                            style: TextStyle(
                                              // color: kDarkBG,
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .secondary,
                                              fontWeight: FontWeight.w600,
                                              // color: Theme.of(context)
                                              //      .primaryColor
                                            ),
                                          ),
                                          Expanded(
                                            child: Align(
                                              alignment: Tools.isRTL(context)
                                                  ? Alignment.centerLeft
                                                  : Alignment.centerRight,
                                              child: TextButton(
                                                onPressed: () {
                                                  if (model.totalCartQuantity >
                                                      0) {
                                                    showDialog(
                                                      context: context,
                                                      useRootNavigator: false,
                                                      builder: (BuildContext
                                                          context) {
                                                        return AlertDialog(
                                                          content: Text(S
                                                              .of(context)
                                                              .confirmClearTheCart),
                                                          actions: [
                                                            TextButton(
                                                              onPressed: () {
                                                                Navigator.of(
                                                                        context)
                                                                    .pop();
                                                              },
                                                              child: Text(S
                                                                  .of(context)
                                                                  .keep),
                                                            ),
                                                            ElevatedButton(
                                                              onPressed: () {
                                                                Navigator.of(
                                                                        context)
                                                                    .pop();
                                                                model
                                                                    .clearCart();
                                                              },
                                                              child: Text(
                                                                S
                                                                    .of(context)
                                                                    .clear,
                                                                style:
                                                                    const TextStyle(
                                                                  color: Colors
                                                                      .white,
                                                                ),
                                                              ),
                                                            ),
                                                          ],
                                                        );
                                                      },
                                                    );
                                                  }
                                                },
                                                child: Text(
                                                  S
                                                      .of(context)
                                                      .clearCart
                                                      .toUpperCase(),
                                                  style: const TextStyle(
                                                      color: Colors.redAccent,
                                                      fontSize: 12,
                                                      fontWeight:
                                                          FontWeight.w600),
                                                ),
                                              ),
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              if (model.totalCartQuantity > 0)
                                const Divider(
                                  height: 1,
                                  // indent: 25,
                                ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  const SizedBox(height: 16.0),
                                  if (model.totalCartQuantity > 0)
                                    Column(
                                      children: _createShoppingCartRows(
                                          model, context),
                                    ),
                                  if (model.totalCartQuantity > 0)
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                        horizontal: 15.0,
                                        vertical: 10.0,
                                      ),
                                      child: Container(
                                        decoration: BoxDecoration(
                                            color: Theme.of(context)
                                                .primaryColorLight),
                                        child: Padding(
                                          padding: const EdgeInsets.symmetric(
                                            vertical: 12.0,
                                            horizontal: 15.0,
                                          ),
                                          child: Column(
                                            children: [
                                              Row(
                                                children: [
                                                  Expanded(
                                                    child: Text(
                                                      S.of(context).products,
                                                      style: smallAmountStyle,
                                                    ),
                                                  ),
                                                  Text(
                                                    cartModel.totalCartQuantity ==
                                                            1
                                                        ? '${cartModel.totalCartQuantity} item'
                                                        : '${cartModel.totalCartQuantity} items',
                                                    style: smallAmountStyle,
                                                  ),
                                                ],
                                              ),
                                              if (cartModel.rewardTotal >
                                                  0) ...[
                                                const SizedBox(height: 10),
                                                Row(
                                                  children: [
                                                    Expanded(
                                                      child: Text(
                                                          S
                                                              .of(context)
                                                              .cartDiscount,
                                                          style:
                                                              smallAmountStyle),
                                                    ),
                                                    Text(
                                                      PriceTools
                                                          .getCurrencyFormatted(
                                                              cartModel
                                                                  .rewardTotal,
                                                              currencyRate,
                                                              currency:
                                                                  currency)!,
                                                      style: smallAmountStyle,
                                                    ),
                                                  ],
                                                ),
                                              ],
                                              const SizedBox(height: 10),
                                              Row(
                                                children: [
                                                  Expanded(
                                                    child: Text(
                                                      //'${S.of(context).total}:',
                                                      'Subtotal:',
                                                      style: largeAmountStyle,
                                                    ),
                                                  ),
                                                  cartModel.calculatingDiscount
                                                      ? const SizedBox(
                                                          width: 20,
                                                          height: 20,
                                                          child:
                                                              CircularProgressIndicator(
                                                            strokeWidth: 2.0,
                                                          ),
                                                        )
                                                      : Text(
                                                          PriceTools.getCurrencyFormatted(
                                                              cartModel
                                                                      .getTotal()! -
                                                                  cartModel
                                                                      .getShippingCost()!,
                                                              currencyRate,
                                                              currency: cartModel
                                                                      .isWalletCart()
                                                                  ? defaultCurrency![
                                                                      'currencyCode']
                                                                  : currency)!,
                                                          style:
                                                              largeAmountStyle,
                                                        ),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                  if (model.totalCartQuantity == 0) EmptyCart(),
                                  if (errMsg.isNotEmpty)
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                        horizontal: 15,
                                        vertical: 10,
                                      ),
                                      child: Text(
                                        errMsg,
                                        style:
                                            const TextStyle(color: Colors.red),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                  const SizedBox(height: 4.0),
                                  WishList()
                                ],
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  );
                },
              ),
            )
          ],
        ),
      ),
    );
  }

  Future<void> onCheckout(CartModel model) async {
    var isLoggedIn = Provider.of<UserModel>(context, listen: false).loggedIn;
    final currencyRate =
        Provider.of<AppModel>(context, listen: false).currencyRate;
    final currency = Provider.of<AppModel>(context, listen: false).currency;
    var message;

    if (isLoading) return;

    if (kCartDetail['minAllowTotalCartValue'] != null) {
      if (kCartDetail['minAllowTotalCartValue'].toString().isNotEmpty) {
        var totalValue = model.getSubTotal() ?? 0;
        var minValue = PriceTools.getCurrencyFormatted(
            kCartDetail['minAllowTotalCartValue'], currencyRate,
            currency: currency);
        if (totalValue < kCartDetail['minAllowTotalCartValue'] &&
            model.totalCartQuantity > 0) {
          message = '${S.of(context).totalCartValue} $minValue';
        }
      }
    }

    if ((kVendorConfig['DisableMultiVendorCheckout'] ?? false) &&
        Config().isVendorType()) {
      if (!model.isDisableMultiVendorCheckoutValid(
          model.productsInCart, model.getProductById)) {
        message = S.of(context).youCanOnlyOrderSingleStore;
      }
    }

    if (message != null) {
      await showFlash(
        context: context,
        duration: const Duration(seconds: 3),
        persistent: !Config().isBuilder,
        builder: (context, controller) {
          return SafeArea(
            child: Flash(
              borderRadius: BorderRadius.circular(3.0),
              backgroundColor: Theme.of(context).errorColor,
              controller: controller,
              behavior: FlashBehavior.fixed,
              position: FlashPosition.top,
              horizontalDismissDirection: HorizontalDismissDirection.horizontal,
              child: FlashBar(
                icon: const Icon(
                  Icons.check,
                  color: Colors.white,
                ),
                content: Text(
                  message,
                  style: const TextStyle(
                    color: Colors.white,
                    fontSize: 18.0,
                    fontWeight: FontWeight.w700,
                  ),
                ),
              ),
            ),
          );
        },
      );

      return;
    }

    if (model.totalCartQuantity == 0) {
      // code comment on 19-7-2023 for user can checkout without checking product error by natasha
      // setState(() {
      //   isLoading3 = false;
      // });
      // code comment end on 19-7-2023 for user can checkout without checking product error by natasha
      if (widget.isModal == true) {
        try {
          ExpandingBottomSheet.of(context)!.close();
        } catch (e) {
          await Navigator.of(context).pushNamed(RouteList.dashboard);
        }
      } else {
        final modalRoute = ModalRoute.of(context);
        if (modalRoute?.canPop ?? false) {
          Navigator.of(context).pop();
          return;
        }
        MainTabControlDelegate.getInstance().tabAnimateTo(0);
      }
    } else if (isLoggedIn || kPaymentConfig['GuestCheckout'] == true) {
      await doCheckout();
      // code comment on 19-7-2023 for user can checkout without checking product error by natasha
      // code added on 8-4-2023 for display error on tap checkout button by natasha
      // await model.getErrorData();
      // printLog('model.errMessage');
      // printLog(model.errMessage);
      // if (model.errMessage == '') {
      //   await doCheckout();
      //   printLog('go to checkout');
      // } else {
      //   setState(() {
      //     isLoading3 = false;
      //   });
      //   printLog('checkout issue');
      // }
      // code comment end  on 19-7-2023 for user can checkout without checking product error by natasha
      // code ended on 8-4-2023 for display error on tap checkout button
    } else {
      _loginWithResult(context);
    }
  }

  Future<void> doCheckout() async {
    showLoading();
    await Services().widget.doCheckout(
      context,
      success: () async {
        hideLoading('');
        printLog('hideLoading');
        // code comment on 19-7-2023 for user can checkout without checking product error by natasha
        //code added on 12-5-2023 for show text loading for not to press button by user when function is under process by natasha
        // setState(() {
        //   isLoading2 = false;
        // });
        // printLog('isLoading2 $isLoading2');
        //code added end on 12-5-2023 for show text loading for not to press button by user when function is under process by natasha
        //code ended 12-5-2023
        await FluxNavigate.pushNamed(
          RouteList.checkout,
          arguments: CheckoutArgument(isModal: widget.isModal),
          forceRootNavigator: true,
        );
      },
      error: (message) async {
        if (message ==
            Exception('Token expired. Please logout then login again')
                .toString()) {
          setState(() {
            isLoading = false;
          });
          //logout
          final userModel = Provider.of<UserModel>(context, listen: false);
          await userModel.logout();
          Services().firebase.signOut();

          _loginWithResult(context);
        } else {
          hideLoading(message);
          Future.delayed(const Duration(seconds: 3), () {
            setState(() => errMsg = '');
          });
        }
      },
      loading: (isLoading) {
        setState(() {
          this.isLoading = isLoading;
        });
      },
    );
  }

  void showLoading() {
    printLog('on tap checkout');
    printLog(isLoading);
    setState(() {
      isLoading = true;
      errMsg = '';
    });
  }

  void hideLoading(error) {
    setState(() {
      isLoading = false;
      errMsg = error;
    });
  }
}
