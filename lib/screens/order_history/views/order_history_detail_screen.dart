import 'dart:async';
import 'dart:convert';

import 'package:connectivity/connectivity.dart';
import 'package:country_pickers/country_pickers.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_linkify/flutter_linkify.dart';
import 'package:http/http.dart' as http;
import 'package:inspireui/utils/logs.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import '../../../common/config.dart';
import '../../../common/constants.dart';
import '../../../common/tools.dart';
import '../../../generated/l10n.dart';
import '../../../models/index.dart' show AppModel;
import '../../../models/order/order.dart';
import '../../../services/index.dart';
import '../../../widgets/common/box_comment.dart';
import '../../../widgets/common/webview.dart';
import '../../../widgets/html/index.dart';
import '../../base_screen.dart';
import '../models/order_history_detail_model.dart';
import 'widgets/order_price.dart';
import 'widgets/product_order_item.dart';

const bool enableOrderNoteHtml = false;

class OrderHistoryDetailScreen extends StatefulWidget {
  const OrderHistoryDetailScreen();

  @override
  _OrderHistoryDetailScreenState createState() =>
      _OrderHistoryDetailScreenState();
}

class _OrderHistoryDetailScreenState
    extends BaseScreen<OrderHistoryDetailScreen> {
  OrderHistoryDetailModel get orderHistoryModel =>
      Provider.of<OrderHistoryDetailModel>(context, listen: false);

  late String getOrderId;
  var timeSlot = '';
  var deliveryDate = '';
  bool isLoading = true;

  //code added for internet connectivity check on 13-7-2023 by natasha
  Future<bool> checkInternetConnection() async {
    printLog('in check internet connection');
    var connectivityResult = await (Connectivity().checkConnectivity());
    return connectivityResult != ConnectivityResult.none;
  }
  //code ended on 13-7-2023

  @override
  void initState() {
    super.initState();
    checkInternetConnection();//13-7-2023
    Future.delayed(const Duration(seconds: 2), () {
      setState(() {
        isLoading = false;
      });
    });
  }

  @override
  void afterFirstLayout(BuildContext context) {
    super.afterFirstLayout(context);
    // orderHistoryModel.getTracking();
    orderHistoryModel.getOrderNote();
  }

  void cancelOrder() {
    orderHistoryModel.cancelOrder();
  }

  void _onNavigate(context, OrderHistoryDetailModel model) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => WebView(
          url:
              "${afterShip['tracking_url']}/${model.order.aftershipTracking!.slug}/${model.order.aftershipTracking!.trackingNumber}",
          appBar: AppBar(
            backgroundColor: Theme.of(context).backgroundColor,
            systemOverlayStyle: SystemUiOverlayStyle.light,
            leading: GestureDetector(
              onTap: () {
                Navigator.of(context).pop();
              },
              child: Icon(
                isIos ? Icons.arrow_back_ios : Icons.arrow_back,
              ),
            ),
            title: Text(S.of(context).trackingPage),
          ),
        ),
      ),
    );
  }

  Future<List> callFutureFunction(var getModel) async {
    // var baseUrl = 'https://zerovaega.in/gwa/wp-json/wc/v3/';
    // var consumerKey = 'ck_e9de88de27730f010182a37d3748b66f09ebf0f0';
    // var consumerSecret = 'cs_932c40c62d5fcc4565de088633629833be52d8d7';
    var baseUrl = 'https://qwickpick.com/wp-json/wc/v3/';
    var consumerKey = 'ck_707b43c19cd3b9ed3dc98f3be0dfe665f284028e';
    var consumerSecret = 'cs_278ff8913a9ef43691e5337ec17949fcbbfff5f1';
    final response = await http.get(
      Uri.parse(baseUrl + 'orders/' + getOrderId),
      headers: {
        'Authorization': 'Basic ' +
            base64Encode(
              utf8.encode('$consumerKey:$consumerSecret'),
            ),
      },
    );

    final jsonData = jsonDecode(response.body);
    var metaData = jsonData['meta_data'];
    final data = metaData;
    final productData = jsonData['line_items'];
    printLog('product data ${productData.length}');
    var prices = [];
    for (var i = 0; i < productData.length; i++) {
      final productDataP = productData[i]['product_data']['price'];
      prices.add(productDataP);
      printLog('productDataP $i is $productDataP');
    }
    printLog('Final LIST IS $prices');

    setState(() {
      for (var item in data) {
        if (item['key'] == 'Delivery Date') {
          deliveryDate = item['value'];
          break;
        }
        print('Delivery Date: $deliveryDate');
      }
      for (var item in data) {
        if (item['key'] == 'Time Slot') {
          // printLog('TimeSlot Values are');
          // printLog(item['value']);
          if (item['value'] == '09:00 - 11:00') {
            String varValue = item['value'];
            varValue =
                varValue.replaceAll('09:00 - 11:00', '9:00 am to 11:00 am');
            print('varValue of 9 to 11');
            print(varValue);
            timeSlot = varValue;
          }
          if (item['value'] == '11:00 - 13:00') {
            String varValue = item['value'];
            varValue =
                varValue.replaceAll('11:00 - 13:00', '11:00 am to 1:00 pm');
            printLog('varValue');
            printLog(varValue);
            timeSlot = varValue;
          }
          if (item['value'] == '13:00 - 15:00') {
            String varValue = item['value'];
            varValue =
                varValue.replaceAll('13:00 - 15:00', '1:00 pm to 3:00 pm');
            // varValue = varValue.replaceAll('15:00', '03:00');
            printLog('varValue');
            printLog(varValue);
            timeSlot = varValue;
          }
          if (item['value'] == '15:00 - 17:00') {
            String varValue = item['value'];
            varValue =
                varValue.replaceAll('15:00 - 17:00', '3:00 pm to 5:00 pm');
            // varValue = varValue.replaceAll('17:00', '05:00');
            printLog('varValue');
            printLog(varValue);
            timeSlot = varValue;
          }
          if (item['value'] == '17:00 - 19:00') {
            String varValue = item['value'];
            varValue =
                varValue.replaceAll('17:00 - 19:00', '5:00 pm to 7:00 pm');
            // varValue = varValue.replaceAll('19:00', '07:00');
            printLog('varValue');
            printLog(varValue);
            timeSlot = varValue;
          }

          // timeSlot = item['value'];
          break;
        }
      }
    });
    // printLog(timeSlot);

    return Future.value(prices);
  }

  var futurePrices;

  @override
  Widget build(BuildContext context) {
    final currencyRate = Provider.of<AppModel>(context).currencyRate;

    return Consumer<OrderHistoryDetailModel>(builder: (context, model, child) {
      final order = model.order;
      getOrderId = order.number!;
      // callFutureFunction(order);
      callFutureFunction(order).then((value) {
        futurePrices = value;
      });
      printLog('FUTURE PRICES ARE $futurePrices');
      return futurePrices != null
          ? Scaffold(
              backgroundColor: Theme.of(context).backgroundColor,
              appBar: AppBar(
                systemOverlayStyle: SystemUiOverlayStyle.light,
                leading: IconButton(
                    icon: Icon(
                      isIos ? Icons.arrow_back_ios : Icons.arrow_back,
                      size: 20,
                      color: Theme.of(context).colorScheme.secondary,
                    ),
                    onPressed: () {
                      Navigator.of(context).pop();
                    }),
                // actions: [
                //   Center(child: Services().widget.reOrderButton(order)),
                // ],
                title: Text(
                  S.of(context).orderNo + ' #${order.number}',
                  style:
                      TextStyle(color: Theme.of(context).colorScheme.secondary),
                ),
                backgroundColor: Theme.of(context).backgroundColor,
                elevation: 0.0,
              ),
              body: SingleChildScrollView(
                padding: const EdgeInsets.symmetric(horizontal: 15.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    ...List.generate(
                      order.lineItems.length,
                      (index) {
                        final _item = order.lineItems[index];
                        return ProductOrderItem(
                          orderId: order.id!,
                          orderStatus: order.status!,
                          product: _item,
                          index: index,
                          storeDeliveryDates: order.storeDeliveryDates,
                          productPrices: futurePrices[index].toString(),
                        );
                      },
                    ),
                    Container(
                      decoration: BoxDecoration(
                        color: Theme.of(context).primaryColorLight,
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      padding: const EdgeInsets.all(15),
                      margin: const EdgeInsets.symmetric(vertical: 10),
                      child: Column(
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text(
                                S.of(context).subtotal,
                                style: Theme.of(context)
                                    .textTheme
                                    .subtitle1!
                                    .copyWith(
                                      fontWeight: FontWeight.w400,
                                    ),
                              ),
                              Text(
                                PriceTools.getCurrencyFormatted(
                                    order.lineItems.fold(
                                        0,
                                        (dynamic sum, e) =>
                                            sum +
                                            double.parse(e.total!) +
                                            double.parse(e
                                                .totalTax!)), //5-6-2023 adding totalTax price
                                    currencyRate)!,
                                style: Theme.of(context)
                                    .textTheme
                                    .subtitle1!
                                    .copyWith(
                                      fontWeight: FontWeight.w700,
                                    ),
                              )
                            ],
                          ),
                          const SizedBox(height: 10),
                          if (order.totalShipping != null)
                            Row(
                              children: <Widget>[
                                Text(S.of(context).shipping,
                                    style: Theme.of(context)
                                        .textTheme
                                        .subtitle1!
                                        .copyWith(
                                          fontWeight: FontWeight.w400,
                                        )),
                                const SizedBox(width: 8),
                                Expanded(
                                  child: Text(
                                    PriceTools.getCurrencyFormatted(
                                        order.totalShipping, currencyRate)!,
                                    textAlign: TextAlign.right,
                                    style: Theme.of(context)
                                        .textTheme
                                        .subtitle1!
                                        .copyWith(
                                          fontWeight: FontWeight.w700,
                                        ),
                                  ),
                                )
                              ],
                            ),
                          const SizedBox(height: 10),
                          Divider(
                            height: 20,
                            color: Theme.of(context).colorScheme.secondary,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text(
                                S.of(context).total,
                                style: Theme.of(context)
                                    .textTheme
                                    .subtitle1!
                                    .copyWith(
                                      fontWeight: FontWeight.bold,
                                      // color: Theme.of(context).//Theme.of(context).primaryColor,
                                    ),
                              ),
                              OrderPrice(
                                order: order,
                                currencyRate: currencyRate,
                              ),
                            ],
                          ),
                          const SizedBox(height: 6),
                          if (order.paymentMethodTitle != null)
                            Row(
                              children: <Widget>[
                                Text(S.of(context).paymentMethod,
                                    style: Theme.of(context)
                                        .textTheme
                                        .subtitle1!
                                        .copyWith(
                                          fontWeight: FontWeight.w400,
                                        )),
                                const SizedBox(width: 8),
                                Expanded(
                                  child: Text(
                                    order.paymentMethodTitle!,
                                    textAlign: TextAlign.right,
                                    style: Theme.of(context)
                                        .textTheme
                                        .subtitle1!
                                        .copyWith(
                                          fontWeight: FontWeight.w700,
                                        ),
                                  ),
                                )
                              ],
                            ),
                        ],
                      ),
                    ),
                    isLoading
                        ? const Center(
                            child: CircularProgressIndicator()) //6-6-2023
                        : Container(
                            decoration: BoxDecoration(
                              color: Theme.of(context).primaryColorLight,
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                            padding: const EdgeInsets.all(15),
                            margin: const EdgeInsets.symmetric(vertical: 10),
                            child: Column(
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text(
                                      'Delivery Date',
                                      style: Theme.of(context)
                                          .textTheme
                                          .subtitle1
                                          ?.copyWith(
                                            fontWeight: FontWeight.w400,
                                          ),
                                    ),
                                    Text(
                                      deliveryDate,
                                      style: Theme.of(context)
                                          .textTheme
                                          .subtitle1
                                          ?.copyWith(
                                            fontWeight: FontWeight.bold,
                                          ),
                                    ),
                                  ],
                                ),
                                const SizedBox(height: 10),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text(
                                      'Delivery Slot',
                                      style: Theme.of(context)
                                          .textTheme
                                          .subtitle1
                                          ?.copyWith(
                                            fontWeight: FontWeight.w400,
                                          ),
                                    ),
                                    Text(
                                      timeSlot,
                                      style: Theme.of(context)
                                          .textTheme
                                          .subtitle1
                                          ?.copyWith(
                                            fontWeight: FontWeight.bold,
                                          ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                    if (model.order.aftershipTracking != null)
                      Padding(
                        padding: const EdgeInsets.only(top: 20.0),
                        child: GestureDetector(
                          onTap: () => _onNavigate(context, model),
                          child: Align(
                            alignment: Alignment.topLeft,
                            child: Row(
                              children: <Widget>[
                                Text('${S.of(context).trackingNumberIs} '),
                                Text(
                                  model
                                      .order.aftershipTracking!.trackingNumber!,
                                  style: TextStyle(
                                    color: Theme.of(context).primaryColor,
                                    decoration: TextDecoration.underline,
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                    Services()
                        .widget
                        .renderOrderTimelineTracking(context, order),
                    const SizedBox(height: 20),

                    /// Render the Cancel and Refund
                    if (kPaymentConfig['EnableRefundCancel'])
                      Services().widget.renderButtons(
                          context, order, cancelOrder, refundOrder),

                    const SizedBox(height: 20),
                    if (order.shipping != null) ...[
                      Text(S.of(context).shippingAddress,
                          style: const TextStyle(
                              fontSize: 18, fontWeight: FontWeight.bold)),
                      const SizedBox(height: 10),
                      Text(
                        ((order.shipping!.apartment?.isEmpty ?? true)
                                ? ''
                                : '${order.shipping!.apartment} ') +
                            ((order.shipping!.block?.isEmpty ?? true)
                                ? ''
                                : '${(order.shipping!.apartment?.isEmpty ?? true) ? '' : '- '} ${order.shipping!.block}, ') +
                            order.shipping!.street!,
                      ),
                    ],
                    if (order.status == OrderStatus.processing &&
                        kPaymentConfig['EnableRefundCancel'])
                      Column(
                        children: <Widget>[
                          const SizedBox(height: 30),
                          Row(
                            children: [
                              Expanded(
                                child: ButtonTheme(
                                  height: 45,
                                  child: ElevatedButton(
                                      style: ElevatedButton.styleFrom(
                                        onPrimary: Colors.white,
                                        primary: HexColor('#056C99'),
                                      ),
                                      onPressed: refundOrder,
                                      child: Text(
                                          S
                                              .of(context)
                                              .refundRequest
                                              .toUpperCase(),
                                          style: const TextStyle(
                                              fontWeight: FontWeight.w700))),
                                ),
                              )
                            ],
                          ),
                          const SizedBox(height: 10),
                        ],
                      ),
                    if (kPaymentConfig['ShowOrderNotes'] ?? true)
                      Padding(
                        padding: const EdgeInsets.only(top: 20.0),
                        child: Builder(
                          builder: (context) {
                            final listOrderNote = model.listOrderNote;
                            if (listOrderNote?.isEmpty ?? true) {
                              return const SizedBox();
                            }
                            return Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  S.of(context).orderNotes,
                                  style: const TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold),
                                ),
                                const SizedBox(height: 10),
                                // SizedBox(
                                //  // height:200,
                                //   child: SingleChildScrollView( // code added 11-6-2023 for order notes section scrollable by natasha
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    ...List.generate(
                                      listOrderNote!.length,
                                      (index) {
                                        return Padding(
                                          padding:
                                              const EdgeInsets.only(bottom: 15),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                              CustomPaint(
                                                painter: BoxComment(
                                                    color: Theme.of(context)
                                                        .primaryColor),
                                                child: SizedBox(
                                                  width: MediaQuery.of(context)
                                                      .size
                                                      .width,
                                                  child: Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            left: 10,
                                                            right: 10,
                                                            top: 15,
                                                            bottom: 25),
                                                    child: enableOrderNoteHtml
                                                        ? HtmlWidget(
                                                            listOrderNote[index]
                                                                .note!,
                                                            textStyle:
                                                                const TextStyle(
                                                                    color:
                                                                        kDarkBG, //7-6-2023
                                                                    //  Colors.white,
                                                                    fontSize:
                                                                        13,
                                                                    height:
                                                                        1.2),
                                                          )
                                                        : Linkify(
                                                            text: listOrderNote[
                                                                    index]
                                                                .note!,
                                                            style:
                                                                const TextStyle(
                                                                    color:
                                                                        kDarkBG, //Colors.white,//7-6-23
                                                                    fontSize:
                                                                        13,
                                                                    height:
                                                                        1.2),
                                                            onOpen:
                                                                (link) async {
                                                              await Tools
                                                                  .launchURL(
                                                                      link.url);
                                                            },
                                                          ),
                                                  ),
                                                ),
                                              ),
                                              Text(
                                                formatTime(DateTime.parse(
                                                    listOrderNote[index]
                                                        .dateCreated!)),
                                                style: const TextStyle(
                                                    fontSize: 13),
                                              )
                                            ],
                                          ),
                                        );
                                      },
                                    ),
                                    const SizedBox(height: 100),
                                  ],
                                ),
                                // ),
                                // ),
                              ],
                            );
                          },
                        ),
                      ),

                    const SizedBox(height: 50)
                  ],
                ),
              ),
            )
          : Scaffold(
              backgroundColor: Theme.of(context).backgroundColor,
              appBar: AppBar(
                systemOverlayStyle: SystemUiOverlayStyle.light,
                leading: IconButton(
                    icon: Icon(
                      isIos ? Icons.arrow_back_ios : Icons.arrow_back,
                      size: 20,
                      color: Theme.of(context).colorScheme.secondary,
                    ),
                    onPressed: () {
                      Navigator.of(context).pop();
                    }),
                // actions: [
                //   Center(child: Services().widget.reOrderButton(order)),
                // ],
                title: Text(
                  S.of(context).orderNo + ' #${order.number}',
                  style:
                      TextStyle(color: Theme.of(context).colorScheme.secondary),
                ),
                backgroundColor: Theme.of(context).backgroundColor,
                elevation: 0.0,
              ),
              body: Container(),
            );
    });
  }

  String getCountryName(country) {
    try {
      return CountryPickerUtils.getCountryByIsoCode(country).name;
    } catch (err) {
      return country;
    }
  }

  Future<void> refundOrder() async {
    _showLoading();
    try {
      await orderHistoryModel.createRefund();
      _hideLoading();
      ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(content: Text(S.of(context).refundOrderSuccess)));
    } catch (err) {
      _hideLoading();

      ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(content: Text(S.of(context).refundOrderFailed)));
    }
  }

  void _showLoading() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Center(
          child: Container(
            decoration: BoxDecoration(
              color: Colors.white30,
              borderRadius: BorderRadius.circular(5.0),
            ),
            padding: const EdgeInsets.all(50.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                kLoadingWidget(context),
                // const SizedBox(height: 48),
                ElevatedButton(
                  onPressed: Navigator.of(context).pop,
                  child: Text(S.of(context).cancel),
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  void _hideLoading() {
    Navigator.of(context).pop();
  }

  String formatTime(DateTime time) {
    return DateFormat('dd/MM/yyyy, HH:mm').format(time);
  }
}
