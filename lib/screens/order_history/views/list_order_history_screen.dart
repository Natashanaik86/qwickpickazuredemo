import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../common/constants.dart';
import '../../../generated/l10n.dart';
import '../../../models/index.dart';
import '../../../widgets/common/paging_list.dart';
import '../models/list_order_history_model.dart';
import '../models/order_history_detail_model.dart';
import 'widgets/order_list_item.dart';
import 'widgets/order_list_loading_item.dart';

class ListOrderHistoryScreen extends StatefulWidget {
  @override
  _ListOrderHistoryScreenState createState() => _ListOrderHistoryScreenState();
}

class _ListOrderHistoryScreenState extends State<ListOrderHistoryScreen> {
  ListOrderHistoryModel get listOrderViewModel =>
      Provider.of<ListOrderHistoryModel>(context, listen: false);

  var mapOrderHistoryDetailModel = <int, OrderHistoryDetailModel>{};

  //code added for internet connectivity check on 13-7-2023 by natasha
  Future<bool> checkInternetConnection() async {
    printLog('in check internet connection');
    var connectivityResult = await (Connectivity().checkConnectivity());
    return connectivityResult != ConnectivityResult.none;
  }
  //code ended on 13-7-2023

  @override
  void initState() {
    super.initState();
    checkInternetConnection();//13-7-2023
  }

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<UserModel>(context, listen: false).user ?? User();
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: AppBar(
        title: Text(
          S.of(context).orderHistory,
          style: TextStyle(
            color: Theme.of(context).colorScheme.secondary,
          ),
        ),
        //centerTitle: true,
        backgroundColor: Theme.of(context).backgroundColor,
        leading: IconButton(
          icon:Icon(isIos ? Icons.arrow_back_ios : Icons.arrow_back,
            color: Theme.of(context).colorScheme.secondary,
          ),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ),
      body: PagingList<ListOrderHistoryModel, Order>(
        onRefresh: mapOrderHistoryDetailModel.clear,
        itemBuilder: (_, order, index) {
          printLog('ORDERS ARE $order');
          printLog(order.quantity);
          if (mapOrderHistoryDetailModel[index] == null) {
            final orderHistoryDetailModel = OrderHistoryDetailModel(
              order: order,
              user: user,
            );
            mapOrderHistoryDetailModel[index] = orderHistoryDetailModel;
          }
          return ChangeNotifierProvider<OrderHistoryDetailModel>.value(
            value: mapOrderHistoryDetailModel[index]!,
            child: OrderListItem(),
          );
        },
        lengthLoadingWidget: 3,
        loadingWidget: const OrderListLoadingItem(),
      ),
    );
  }
}
