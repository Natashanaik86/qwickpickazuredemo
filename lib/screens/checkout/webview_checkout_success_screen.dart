import 'package:flutter/material.dart';

import '../../common/constants.dart';
import '../../models/index.dart' show Order;
import 'widgets/success.dart';

class WebviewCheckoutSuccessScreen extends StatelessWidget {
  final Order? order;

  const WebviewCheckoutSuccessScreen({this.order});

  @override
  Widget build(BuildContext context) {
    printLog('here ::: $order');
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
            icon: Icon(isIos ? Icons.arrow_back_ios : Icons.arrow_back,),
            onPressed: () {
              Navigator.of(context).pop();
            }),
        backgroundColor: kGrey200,
        elevation: 0.0,
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 15),
        child: OrderedSuccess(order: order),
      ),
    );
  }
}
