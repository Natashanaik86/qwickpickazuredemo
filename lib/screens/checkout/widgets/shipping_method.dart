import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import '../../../common/config.dart'
    show kAdvanceConfig, kLoadingWidget, kPaymentConfig;
import '../../../common/constants.dart';
import '../../../common/tools.dart';
import '../../../generated/l10n.dart';
import '../../../models/cart/cart_model.dart';
import '../../../models/entities/order_delivery_date.dart';
import '../../../models/shipping_method_model.dart';
import '../../../widgets/common/dialogs.dart';
import 'date_time_picker.dart';

var globalDeliveryDate = '';
var globalDeliveryTimeSlot = '';

class ShippingMethods extends StatefulWidget {
  final Function? onBack;
  final Function? onNext;

  const ShippingMethods({this.onBack, this.onNext});

  @override
  _ShippingMethodsState createState() => _ShippingMethodsState();
}

class _ShippingMethodsState extends State<ShippingMethods> {
  int? selectedIndex = 0;
  DateTime? _selectedDate;
  // final GlobalKey<ScaffoldState> _scaffoldKeyShippingMethod =
  //     GlobalKey<ScaffoldState>(); //comment unused code on 17-7-2023 by natasha
  final TextEditingController _controllerTimeSlot = TextEditingController();
  final TextEditingController _controllerDate = TextEditingController();
  bool selectTime = false;
  ShippingMethodModel get shippingMethodModel =>
      Provider.of<ShippingMethodModel>(context, listen: false);

  CartModel get cartModel => Provider.of<CartModel>(context, listen: false);
  DateTime hideTime = DateTime.now().add(const Duration(minutes: 30));
  DateTime currentTime = DateTime.now();
  // String datetime = '${DateFormat.jm('h').format(DateTime.now()).toString()} '
  //     '${DateFormat.jm('a').format(DateTime.now()).toString()}';
  bool show_9_11 = true;
  bool show_11_13 = true;
  bool show_13_15 = true;
  bool show_15_17 = true;
  bool show_17_19 = true;
  bool timeColor = false;
  int _selectedContainerIndex1 = -1;
  int _selectedContainerIndex2 = -1;
  int _selectedContainerIndex3 = -1;
  int _selectedContainerIndex4 = -1;
  int _selectedContainerIndex5 = -1;

  @override
  void initState() {
    super.initState();
    Future.delayed(
      Duration.zero,
          () async {
        final shippingMethod = cartModel.shippingMethod;
        final shippingMethods = shippingMethodModel.shippingMethods;
        if (shippingMethods != null &&
            shippingMethods.isNotEmpty &&
            shippingMethod != null) {
          final index = shippingMethods
              .indexWhere((element) => element.id == shippingMethod.id);
          if (index > -1) {
            setState(() {
              selectedIndex = index;
            });
          }
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    final shippingMethodModel = Provider.of<ShippingMethodModel>(context);
    //comment unused code on 17-7-2023 by natasha
    // final currency = Provider.of<CartModel>(context).currency;
    // final currencyRates = Provider.of<CartModel>(context).currencyRates;
    DateTime showTime;
    showTime = DateTime(currentTime.year, currentTime.month, currentTime.day);
    printLog(showTime);

    bool isItToday = false;
    if (_selectedDate != null) {
      if (_selectedDate == showTime) {
        isItToday = true;
      }
    }

    if (isItToday || selectTime != true) {
      printLog('it is today!');
      show_9_11 = true;
      if (currentTime.isAfter(DateTime(
          currentTime.year, currentTime.month, currentTime.day, 8, 45))) {
        show_9_11 = false;
        printLog(show_9_11);
      } else {
        printLog(show_9_11);
      }

      show_11_13 = true;
      if (currentTime.isAfter(DateTime(
          currentTime.year, currentTime.month, currentTime.day, 10, 45))) {
        show_11_13 = false;
        printLog(show_11_13);
      } else {
        printLog(show_11_13);
      }

      show_13_15 = true;
      if (currentTime.isAfter(DateTime(
          currentTime.year, currentTime.month, currentTime.day, 12, 45))) {
        show_13_15 = false;
        printLog(show_13_15);
      } else {
        printLog(show_13_15);
      }

      show_15_17 = true;
      if (currentTime.isAfter(DateTime(
          currentTime.year, currentTime.month, currentTime.day, 14, 45))) {
        show_15_17 = false;
        printLog(show_15_17);
      } else {
        printLog(show_15_17);
      }

      show_17_19 = true;
      if (currentTime.isAfter(DateTime(
          currentTime.year, currentTime.month, currentTime.day, 16, 45))) {
        show_17_19 = false;
        printLog(show_17_19);
      } else {
        printLog(show_17_19);
      }
    }

    //String formattedTime;//unused code comment on 17-7-23 by natasha
    // formattedTime = DateFormat('hh:mm a').format(showTime);
    //formattedTime = DateFormat('hh:mm a').format(DateTime.now()); //unused code comment on 17-7-23 by natasha
    // // print(currentTime.isBefore(showTime));
    // print(currentTime.hour);
    // print(formattedTime);
    // print(datetime);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        // Text(
        //   S.of(context).shippingMethod,
        //   style: const TextStyle(fontSize: 16),
        // ),
        // const SizedBox(height: 20),
        ListenableProvider.value(
          value: shippingMethodModel,
          child: Consumer<ShippingMethodModel>(
            builder: (context, model, child) {
              if (model.isLoading) {
                return SizedBox(height: 100, child: kLoadingWidget(context));
              }

              if (model.message != null) {
                return SizedBox(
                  height: 100,
                  child: Center(
                      child: Text(model.message!,
                          style: const TextStyle(color: kErrorRed))),
                );
              }

              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  // for (int i = 0; i < model.shippingMethods!.length; i++)
                  // Column(
                  //   children: <Widget>[
                  //     Container(
                  //       decoration: BoxDecoration(
                  //         color: i == selectedIndex
                  //             ? Theme.of(context).primaryColorLight
                  //             : Colors.transparent,
                  //       ),
                  //       child: Padding(
                  //         padding: const EdgeInsets.symmetric(
                  //             vertical: 15, horizontal: 10),
                  //         child: Row(
                  //           children: <Widget>[
                  //             Radio(
                  //               value: i,
                  //               groupValue: selectedIndex,
                  //               onChanged: (dynamic i) {
                  //                 setState(() {
                  //                   selectedIndex = i;
                  //                 });
                  //               },
                  //             ),
                  //             const SizedBox(width: 10),
                  //             Expanded(
                  //               child: Column(
                  //                 crossAxisAlignment:
                  //                     CrossAxisAlignment.start,
                  //                 children: <Widget>[
                  //                   Services()
                  //                       .widget
                  //                       .renderShippingPaymentTitle(context,
                  //                           model.shippingMethods![i].title!),
                  //                   const SizedBox(height: 5),
                  //                   if (model.shippingMethods![i].cost! >
                  //                           0.0 ||
                  //                       !isNotBlank(model
                  //                           .shippingMethods![i].classCost))
                  //                     Text(
                  //                       PriceTools.getCurrencyFormatted(
                  //                           model.shippingMethods![i].cost,
                  //                           currencyRates,
                  //                           currency: currency)!,
                  //                       style: const TextStyle(
                  //                           fontSize: 14, color: kGrey400),
                  //                     ),
                  //                   if (model.shippingMethods![i].cost ==
                  //                           0.0 &&
                  //                       isNotBlank(model
                  //                           .shippingMethods![i].classCost))
                  //                     Text(
                  //                       model.shippingMethods![i].classCost!,
                  //                       style: const TextStyle(
                  //                           fontSize: 14, color: kGrey400),
                  //                     )
                  //                 ],
                  //               ),
                  //             )
                  //           ],
                  //         ),
                  //       ),
                  //     ),
                  //     i < model.shippingMethods!.length - 1
                  //         ? const Divider(height: 1)
                  //         : Container()
                  //   ],
                  // ),
                  const SizedBox(height: 20),
                  // buildDeliveryDate(),
                  Padding(
                    padding: const EdgeInsets.all(0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                          'Schedule Order',
                          style: TextStyle(fontSize: 16,fontWeight: FontWeight.w600),
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        const Text.rich(
                          TextSpan(
                            children: [
                              TextSpan(
                                text: 'Select Date',
                                style: TextStyle(fontSize: 16,fontWeight: FontWeight.w600),
                              ),
                              TextSpan(
                                text: ' *',
                                style: TextStyle(
                                  color: Colors.red,
                                ),
                              ),
                            ],
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        TextFormField(
                          controller: _controllerDate,
                          onTap: () {
                            _controllerTimeSlot
                                .clear(); // 8-4-2023 code added for clear selected time slot by natasha
                            setState(() {
                              _selectedContainerIndex1 = -1;
                              _selectedContainerIndex2 = -1;
                              _selectedContainerIndex3 = -1;
                              _selectedContainerIndex4 = -1;
                              _selectedContainerIndex5 = -1;
                            });
                            showDatePicker(
                              context: context,
                              initialDate: DateTime.now(),
                              firstDate: DateTime.now(), //DateTime(1900),
                              lastDate: DateTime(2100),
                            ).then((selectedDate) {
                              if (selectedDate != null) {
                                setState(() {
                                  _selectedDate = selectedDate;
                                  _controllerDate.text =
                                      DateFormat('yyyy-MM-dd')
                                          .format(selectedDate);
                                  selectTime = true;
                                  if (_selectedDate!.isAfter(showTime)) {
                                    show_9_11 = true;
                                    show_11_13 = true;
                                    show_13_15 = true;
                                    show_15_17 = true;
                                    show_17_19 = true;
                                    printLog('show_9_11');
                                    printLog(show_9_11);
                                  }
                                });
                              }
                            });
                          },
                          readOnly: true,
                          decoration: const InputDecoration(
                            // labelText: 'Date',
                            hintText: 'Select Date',
                            border: OutlineInputBorder(),
                            suffixIcon: Icon(Icons.calendar_view_month),
                          ),
                          validator: (value) {
                            // check if a date has been selected
                            if (_selectedDate == null) {
                              printLog('Please select a date');
                            }
                            return null; // return null if validation passes
                          },
                        ),
                        const SizedBox(
                          height: 18,
                        ),
                        selectTime == false
                            ? const SizedBox()
                            : Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            // const Text(
                            //   'Select Time',
                            //   style: TextStyle(fontSize: 16),
                            // ),
                            const Text.rich(
                              TextSpan(
                                children: [
                                  TextSpan(
                                    text: 'Select Time',
                                    style: TextStyle(fontSize: 16,fontWeight: FontWeight.w600),
                                  ),
                                  TextSpan(
                                    text: ' *',
                                    style: TextStyle(
                                      color: Colors.red,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            TextFormField(
                              // validator: (val) {
                              //   return val!.isEmpty
                              //       ? 'Time slot is required'
                              //       : null;
                              // },
                              controller: _controllerTimeSlot,
                              readOnly: true,
                              decoration: const InputDecoration(
                                // labelText: 'Date',
                                hintText: 'Select Time Slot',
                                border: OutlineInputBorder(),
                                suffixIcon:
                                Icon(Icons.access_time_filled_sharp),
                              ),
                            ),
                            const SizedBox(
                              height: 12,
                            ),
                            (show_9_11 == false &&
                                show_11_13 == false &&
                                show_13_15 == false &&
                                show_15_17 == false &&
                                show_17_19 == false)
                                ? Container(
                              decoration: BoxDecoration(
                                border: Border.all(
                                  color: Colors.grey,
                                  // width: 2,
                                ),
                                borderRadius:
                                BorderRadius.circular(5),
                              ),
                              child: const Padding(
                                padding: EdgeInsets.all(8.0),
                                child: Text(
                                    'There are no delivery slots available, please select another date.'),
                              ),
                            )
                                : Container(
                              padding: const EdgeInsets.all(10.0),
                              // width: 350,
                              // height: 80,
                              decoration: BoxDecoration(
                                border: Border.all(
                                  color: Colors.grey,
                                  // width: 2,
                                ),
                                borderRadius:
                                BorderRadius.circular(5),
                              ),
                              child: SingleChildScrollView(
                                scrollDirection: Axis.horizontal,
                                child: Row(
                                  mainAxisAlignment:
                                  MainAxisAlignment.spaceEvenly,
                                  children: [
                                    GestureDetector(
                                      onTap: () {
                                        setState(() {
                                          _selectedContainerIndex1 =
                                          0;
                                          _selectedContainerIndex2 =
                                          -1;
                                          _selectedContainerIndex3 =
                                          -1;
                                          _selectedContainerIndex4 =
                                          -1;
                                          _selectedContainerIndex5 =
                                          -1;
                                        });
                                        _controllerTimeSlot.text =
                                        '09:00 - 11:00';
                                      },
                                      child: Visibility(
                                        visible: show_9_11,
                                        child: Container(
                                          decoration: BoxDecoration(
                                            border: Border.all(
                                              color:  _selectedContainerIndex1 ==
                                                  0 ? Colors.black
                                                  : Colors.grey,
                                              width: 1,
                                            ),
                                            color:
                                            _selectedContainerIndex1 ==
                                                0
                                                ? Theme.of(
                                                context)
                                                .primaryColor
                                                : Colors
                                                .transparent,
                                            // borderRadius: BorderRadius.circular(10),
                                          ),
                                          // width: 50,
                                          // height: 50,
                                          // color: Colors.blue,
                                          child: const Padding(
                                            padding: EdgeInsets.all(4.0),
                                            child: Text(
                                              '09:00 am\n11:00 am',
                                              textAlign:
                                              TextAlign.center,
                                              // style: TextStyle(fontSize: 15),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    const SizedBox(
                                      width: 10,
                                    ),
                                    GestureDetector(
                                      onTap: () {
                                        setState(() {
                                          _selectedContainerIndex1 =
                                          -1;
                                          _selectedContainerIndex2 =
                                          0;
                                          _selectedContainerIndex3 =
                                          -1;
                                          _selectedContainerIndex4 =
                                          -1;
                                          _selectedContainerIndex5 =
                                          -1;
                                        });
                                        _controllerTimeSlot.text =
                                        '11:00 - 01:00';
                                      },
                                      child: Visibility(
                                        visible: show_11_13,
                                        child: Container(
                                          decoration: BoxDecoration(
                                            border: Border.all(
                                              color:  _selectedContainerIndex2 ==
                                                  0 ? Colors.black
                                                  : Colors.grey,
                                              width: 1,
                                            ),
                                            color:
                                            _selectedContainerIndex2 ==
                                                0
                                                ? Theme.of(
                                                context)
                                                .primaryColor
                                                : Colors
                                                .transparent,
                                            // borderRadius: BorderRadius.circular(10),
                                          ),
                                          // width: 50,
                                          // height: 50,
                                          // color: Colors.blue,
                                          child: const Padding(
                                            padding: EdgeInsets.all(4.0),
                                            child: Text(
                                              '11:00 am\n01:00 pm',
                                              textAlign:
                                              TextAlign.center,
                                              // style: TextStyle(fontSize: 15),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    const SizedBox(
                                      width: 10,
                                    ),
                                    GestureDetector(
                                      onTap: () {
                                        setState(() {
                                          _selectedContainerIndex1 =
                                          -1;
                                          _selectedContainerIndex2 =
                                          -1;
                                          _selectedContainerIndex3 =
                                          0;
                                          _selectedContainerIndex4 =
                                          -1;
                                          _selectedContainerIndex5 =
                                          -1;
                                        });
                                        _controllerTimeSlot.text =
                                        '01:00 - 03:00';
                                      },
                                      child: Visibility(
                                        visible: show_13_15,
                                        child: Container(
                                          decoration: BoxDecoration(
                                            border: Border.all(
                                              color:  _selectedContainerIndex3 ==
                                                  0 ? Colors.black
                                                  : Colors.grey,
                                              width: 1,
                                            ),
                                            color:
                                            _selectedContainerIndex3 ==
                                                0
                                                ? Theme.of(
                                                context)
                                                .primaryColor
                                                : Colors
                                                .transparent,
                                            // borderRadius: BorderRadius.circular(10),
                                          ),
                                          // width: 50,
                                          // height: 50,
                                          // color: Colors.blue,
                                          child: const Padding(
                                            padding: EdgeInsets.all(5.0),
                                            child: Text(
                                              '01:00 pm\n03:00 pm',
                                              textAlign:
                                              TextAlign.center,
                                              // style: TextStyle(fontSize: 15),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    const SizedBox(
                                      width: 10,
                                    ),
                                    GestureDetector(
                                      onTap: () {
                                        setState(() {
                                          _selectedContainerIndex1 =
                                          -1;
                                          _selectedContainerIndex2 =
                                          -1;
                                          _selectedContainerIndex3 =
                                          -1;
                                          _selectedContainerIndex4 =
                                          0;
                                          _selectedContainerIndex5 =
                                          -1;
                                        });
                                        _controllerTimeSlot.text =
                                        '03:00 - 05:00';
                                      },
                                      child: Visibility(
                                        visible: show_15_17,
                                        child: Container(
                                          decoration: BoxDecoration(
                                            border: Border.all(
                                              color:  _selectedContainerIndex4 ==
                                                  0 ? Colors.black
                                                  : Colors.grey,
                                              width: 1,
                                            ),
                                            color:
                                            _selectedContainerIndex4 ==
                                                0
                                                ? Theme.of(
                                                context)
                                                .primaryColor
                                                : Colors
                                                .transparent,
                                            // borderRadius: BorderRadius.circular(10),
                                          ),
                                          // width: 50,
                                          // height: 50,
                                          // color: Colors.blue,
                                          child: const Padding(
                                            padding: EdgeInsets.all(4.0),
                                            child: Text(
                                              '03:00 pm\n05:00 pm',
                                              textAlign:
                                              TextAlign.center,
                                              // style: TextStyle(fontSize: 15),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    const SizedBox(
                                      width: 10,
                                    ),
                                    GestureDetector(
                                      onTap: () {
                                        setState(() {
                                          _selectedContainerIndex1 =
                                          -1;
                                          _selectedContainerIndex2 =
                                          -1;
                                          _selectedContainerIndex3 =
                                          -1;
                                          _selectedContainerIndex4 =
                                          -1;
                                          _selectedContainerIndex5 =
                                          0;
                                        });
                                        _controllerTimeSlot.text =
                                        '05:00 - 07:00';
                                      },
                                      child: Visibility(
                                        visible: show_17_19,
                                        child: Container(
                                          decoration: BoxDecoration(
                                            border: Border.all(
                                              color:  _selectedContainerIndex5 ==
                                                  0 ? Colors.black
                                                  : Colors.grey,
                                              width: 1,
                                            ),
                                            color:
                                            _selectedContainerIndex5 ==
                                                0
                                                ? Theme.of(
                                                context)
                                                .primaryColor
                                                : Colors
                                                .transparent,
                                            // borderRadius: BorderRadius.circular(10),
                                          ),
                                          // width: 50,
                                          // height: 50,
                                          // color: Colors.blue,
                                          child: const Padding(
                                            padding: EdgeInsets.all(4.0),
                                            child: Text(
                                              '05:00 pm\n07:00 pm',
                                              textAlign:
                                              TextAlign.center,
                                              // style: TextStyle(fontSize: 15),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              );
            },
          ),
        ),
        const SizedBox(
          height: 18,
        ),
        Row(
          children: [
            Expanded(
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  elevation: 0,
                  onPrimary: Colors.white,
                  primary: Theme.of(context).primaryColor,
                ),
                onPressed: () async {
                  //code added for internet connectivity check on 13-7-2023 by natasha
                  var isConnected = await checkInternetConnection();
                  if (isConnected) {
                    // Internet connection is available
                    printLog('Connected to the internet');
                    globalDeliveryDate = _controllerDate.text;
                    globalDeliveryDate = DateFormat('d MMMM, y')
                        .format(DateTime.parse(globalDeliveryDate));
                    globalDeliveryTimeSlot = _controllerTimeSlot.text;
                    //print('globalDeliveryTimeSlot $globalDeliveryTimeSlot');
                    if (globalDeliveryTimeSlot == '11:00 - 01:00') {
                      globalDeliveryTimeSlot = '11:00 - 13:00';
                    } else if (globalDeliveryTimeSlot == '01:00 - 03:00') {
                      globalDeliveryTimeSlot = '13:00 - 15:00';
                    } else if (globalDeliveryTimeSlot == '03:00 - 05:00') {
                      globalDeliveryTimeSlot = '15:00 - 17:00';
                    } else if (globalDeliveryTimeSlot == '05:00 - 07:00') {
                      globalDeliveryTimeSlot = '17:00 - 19:00';
                    }
                    printLog('globalDeliveryTimeSlot $globalDeliveryTimeSlot');
                    if (_controllerTimeSlot.text.isEmpty) {
                      Tools.showSnackBar(
                          Scaffold.of(context), 'Please select time slot !');
                      return;
                    }
                    if (_controllerDate.text.isEmpty) {
                      Tools.showSnackBar(
                          Scaffold.of(context), 'Please select time slot !');
                      return;
                    }

                    if (shippingMethodModel.shippingMethods?.isNotEmpty ??
                        false) {
                      Provider.of<CartModel>(context, listen: false)
                          .setShippingMethod(shippingMethodModel
                          .shippingMethods![selectedIndex!]);

                      widget.onNext!();
                    }
                  } else {
                    // No internet connection
                    printLog('No internet connection');
                    await showDialogNotInternet(context);
                    // await Navigator.push(
                    //   context,
                    //   MaterialPageRoute(
                    //       builder: (context) => const InternetLossPage()
                    //   ),
                    // );
                  }
                  //code ended on 13-7-2023
                  // globalDeliveryDate = _controllerDate.text;
                  // globalDeliveryDate = DateFormat('d MMMM, y')
                  //     .format(DateTime.parse(globalDeliveryDate));
                  //
                  // // print(globalDeliveryDate); // Output: "27 March, 2023"
                  //
                  // globalDeliveryTimeSlot = _controllerTimeSlot.text;
                  // //print('globalDeliveryTimeSlot $globalDeliveryTimeSlot');
                  // if (globalDeliveryTimeSlot == '11:00 - 01:00') {
                  //   globalDeliveryTimeSlot = '11:00 - 13:00';
                  // } else if (globalDeliveryTimeSlot == '01:00 - 03:00') {
                  //   globalDeliveryTimeSlot = '13:00 - 15:00';
                  // } else if (globalDeliveryTimeSlot == '03:00 - 05:00') {
                  //   globalDeliveryTimeSlot = '15:00 - 17:00';
                  // } else if (globalDeliveryTimeSlot == '05:00 - 07:00') {
                  //   globalDeliveryTimeSlot = '17:00 - 19:00';
                  // }
                  // print('globalDeliveryTimeSlot $globalDeliveryTimeSlot');
                  // if (_controllerTimeSlot.text.isEmpty) {
                  //   Tools.showSnackBar(
                  //       Scaffold.of(context), 'Please select time slot !');
                  //   return;
                  // }
                  // if (_controllerDate.text.isEmpty) {
                  //   Tools.showSnackBar(
                  //       Scaffold.of(context), 'Please select time slot !');
                  //   return;
                  // }
                  //
                  // if (shippingMethodModel.shippingMethods?.isNotEmpty ??
                  //     false) {
                  //   Provider.of<CartModel>(context, listen: false)
                  //       .setShippingMethod(shippingMethodModel
                  //           .shippingMethods![selectedIndex!]);
                  //
                  //   widget.onNext!();
                  // }
                },
                child: Text(((kPaymentConfig['EnableReview'] ?? true)
                    ? S.of(context).continueToReview
                    : S.of(context).continueToPayment),
                  style: const TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.w600,
                    color: kDarkBG,
                    //  Theme.of(context).colorScheme.secondary,
                  ),),
              ),
            ),
          ],
        ),
        if (kPaymentConfig['EnableAddress'])
          Center(
            child: TextButton(
              onPressed: () {
                widget.onBack!();
              },
              child: Text(
                S.of(context).goBackToAddress,
                textAlign: TextAlign.center,
                style: TextStyle(
                    decoration: TextDecoration.underline,
                    fontSize: 15,
                    color: Theme.of(context).colorScheme.secondary),
              ),
            ),
          )
      ],
    );
  }

  //code added for internet connectivity check on 13-7-2023 by natasha
  Future<bool> checkInternetConnection() async {
    printLog('in check internet connection');
    var connectivityResult = await (Connectivity().checkConnectivity());
    return connectivityResult != ConnectivityResult.none;
  }
  //code ended on 13-7-2023

  Widget buildDeliveryDate() {
    if (!(kAdvanceConfig['EnableDeliveryDateOnCheckout'] ?? false)) {
      return const SizedBox();
    }

    Widget deliveryWidget = DateTimePicker(
      onChanged: (DateTime datetime) {
        cartModel.selectedDate = OrderDeliveryDate(datetime);
      },
      minimumDate: DateTime.now(),
      initDate: cartModel.selectedDate?.dateTime,
      border: const OutlineInputBorder(),
    );

    // if (shippingMethodModel.deliveryDates?.isNotEmpty ?? false) {
    //   deliveryWidget =
    //       DeliveryCalendar(dates: shippingMethodModel.deliveryDates!);
    // }

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.only(
              right: Tools.isRTL(context) ? 12.0 : 0.0,
              left: !Tools.isRTL(context) ? 12.0 : 0.0),
          child: Text(S.of(context).deliveryDate,
              style: Theme.of(context).textTheme.caption!.copyWith(
                  color: Theme.of(context)
                      .colorScheme
                      .secondary
                      .withOpacity(0.7))),
        ),
        const SizedBox(height: 10),
        deliveryWidget,
        const SizedBox(height: 20),
      ],
    );
  }
}
