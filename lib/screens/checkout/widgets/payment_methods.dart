import 'dart:async';
import 'dart:convert' as convert;
import 'dart:convert';
import 'dart:developer';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'package:razorpay_flutter/razorpay_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../common/config.dart';
import '../../../common/constants.dart';
import '../../../common/tools.dart';
import '../../../generated/l10n.dart';
import '../../../models/booking/booking_model.dart';
import '../../../models/index.dart'
    show
        AppModel,
        CartModel,
        Order,
        PaymentMethodModel,
        TaxModel,
        User,
        UserModel;
import '../../../modules/native_payment/index.dart';
import '../../../services/index.dart';
import '../../../widgets/common/dialogs.dart';
import '../../cart/my_cart_screen.dart';
import '../../cart/widgets/shopping_cart_sumary.dart';
import 'shipping_method.dart';

//code added on 19-7-2023 for product error api calling by natasha
var productError = {};
var qtyToBePassed;
var qtyCondition1 = {};
bool isErrorLoading = false;//key added for show loader on 26-7-2023 by natasha
bool razorpayFailed = true,
    cashOnDeliveryMethod =
        false; // code added for failed screen on 26-5-2023 by natasha and shireen
bool isRefreshCart = false;//28-7-23

class PaymentMethods extends StatefulWidget {
  final Function? onBack;
  final Function? onFinish;
  final Function(bool)? onLoading;
  final Function? onPageLoad; //8-7-2023

  const PaymentMethods(
      {this.onBack, this.onFinish, this.onLoading, this.onPageLoad});

  @override
  _PaymentMethodsState createState() => _PaymentMethodsState();
}

class _PaymentMethodsState extends State<PaymentMethods> with RazorDelegate {
  String? selectedId;
  bool isPaying = false;
  User? get user => Provider.of<UserModel>(context, listen: false).user;
  // int count = 0;//count added for handle api calling by natasha
  // bool? get razorPayStatus =>    Provider.of<RazorPayStatus>(context, listen: false).razorPayStatus;//04072023

  bool isLoadingButton = false;
  bool isLoadingText =
      false; //code added for show place order checking by natasha on 22-7-23
  String? selectedValue = 'cod';

  @override
  void initState() {
    super.initState();
    printLog('init call in payment method');
    Future.delayed(Duration.zero, () {
      final cartModel = Provider.of<CartModel>(context, listen: false);
      final userModel = Provider.of<UserModel>(context, listen: false);
      Provider.of<PaymentMethodModel>(context, listen: false).getPaymentMethods(
          cartModel: cartModel,
          shippingMethod: cartModel.shippingMethod,
          token: userModel.user != null ? userModel.user!.cookie : null);
      //print("init state: "+ )
      if (kPaymentConfig['EnableReview'] != true) {
        Provider.of<TaxModel>(context, listen: false)
            .getTaxes(Provider.of<CartModel>(context, listen: false),
                (taxesTotal, taxes) {
          Provider.of<CartModel>(context, listen: false).taxesTotal =
              taxesTotal;
          Provider.of<CartModel>(context, listen: false).taxes = taxes;
          setState(() {});
        });
      }
    });
  }

  //code added for internet connectivity check on 13-7-2023 by natasha
  Future<bool> checkInternetConnection() async {
    printLog('in check internet connection');
    var connectivityResult = await (Connectivity().checkConnectivity());
    return connectivityResult != ConnectivityResult.none;
  }
  //code ended on 13-7-2023

  //code added on 19-7-2023 for show product error in cart page when pop up comes by natasha
  var cartModel;
  void getProductErrorListData(
    BuildContext context,
    CartModel cartModel,
  ) async {
    //23-5-2023
    isErrorLoading = true;
    printLog('cartModel.item.keys');
    printLog(cartModel.item.keys.toString());
    List<Map<String, dynamic>> selectedProducts = [];
    cartModel.productsInCart.forEach((productId, quantity) {
      //model.productsInCart.forEach((productId, quantity) {
      selectedProducts.add({
        'product_id': int.parse(productId),
        'quantity': quantity,
      });
    });

    printLog(selectedProducts);
    // Create the request body
    Map<String, dynamic> requestBody = {
      'selected_products': selectedProducts,
    };

    // Convert the request body to JSON format
    String jsonBody = json.encode(requestBody);
    printLog('jsonBody $jsonBody');
    // Make the HTTP POST request
    var url = '$baseUrl/wp-json/wc/v2/flutter/check_product_errors';
    final response = await http.post(
      Uri.parse(url),
      headers: {'Content-Type': 'application/json'},
      body: json.encode(requestBody),
    );
    // Check the response status code
    printLog('Response STATUS CODE IS ${response.statusCode}');
    if (response.statusCode == 200) {
      // Request was successful
      printLog('Response: ${response.body}');
      var responseData = json.decode(response.body);
      printLog('response Data decode $responseData');

      productError = {};
      for (var item in responseData) {
        productError[item['product_id'].toString()] =
            item['message'].toString();
        qtyCondition1[item['product_id'].toString()] = item['quantity'];
      }
      printLog(productError.toString());
      isErrorLoading = false;

      //-------------------------22-5-2023----------------------
    } else {
      // Request failed
      printLog('Error: ${response.statusCode}');
    }
  }

  @override
  Widget build(BuildContext context) {
    printLog(globalDeliveryDate);
    printLog(globalDeliveryTimeSlot);
    final cartModel = Provider.of<CartModel>(context);
    final currencyRate = Provider.of<AppModel>(context).currencyRate;
    final paymentMethodModel = Provider.of<PaymentMethodModel>(context);
    final taxModel = Provider.of<TaxModel>(context);
    var errorString = null;

    return ListenableProvider.value(
        value: paymentMethodModel,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            // const SizedBox(height: 20),
            const ShoppingCartSummary(),
            Padding(
              // padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 0),//6-6-2023 alignment
              padding:
                  const EdgeInsets.only(top: 8, right: 10), //6-6-2023 alignment
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text(
                    S.of(context).subtotal,
                    style: TextStyle(
                      fontSize: 16,
                      color: Theme.of(context).colorScheme.secondary,
                    ),
                  ),
                  Text(
                      PriceTools.getCurrencyFormatted(
                          cartModel.getSubTotal(), currencyRate,
                          currency: cartModel.currency)!,
                      style: TextStyle(
                          fontSize: 14,
                          color: Theme.of(context).colorScheme.secondary))
                ],
              ),
            ),
            Services().widget.renderShippingMethodInfo(context),
            if (cartModel.getCoupon() != '')
              Padding(
                // padding:
                //     const EdgeInsets.symmetric(vertical: 8, horizontal: 0),//6-6-2023 alignment
                padding: const EdgeInsets.only(
                    top: 8, right: 10), //6-6-2023 alignment
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      S.of(context).discount,
                      style: TextStyle(
                        fontSize: 14,
                        color: Theme.of(context)
                            .colorScheme
                            .secondary
                            .withOpacity(0.8),
                      ),
                    ),
                    Text(
                      cartModel.getCoupon(),
                      style: Theme.of(context).textTheme.subtitle1!.copyWith(
                            fontSize: 14,
                            color: Theme.of(context)
                                .colorScheme
                                .secondary
                                .withOpacity(0.8),
                          ),
                    )
                  ],
                ),
              ),
            Services().widget.renderTaxes(taxModel, context),
            Services().widget.renderRewardInfo(context),
            Services().widget.renderCheckoutWalletInfo(context),
            Padding(
              //  padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 0),//6-6-2023 alignment
              padding: const EdgeInsets.only(
                  top: 14, right: 10), //6-6-2023 alignment
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text(
                    S.of(context).total,
                    style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.w600,
                        color: Theme.of(context).colorScheme.secondary),
                  ),
                  Text(
                    PriceTools.getCurrencyFormatted(
                        cartModel.getTotal(), currencyRate,
                        currency: cartModel.currency)!,
                    style: TextStyle(
                      fontSize: 18,
                      color: Theme.of(context).colorScheme.secondary,
                      fontWeight: FontWeight.w600,
                      //decoration: TextDecoration.underline,
                    ),
                  )
                ],
              ),
            ),
            const SizedBox(height: 14),
            Text(
              'Amount is inclusive of taxes',
              style: TextStyle(
                fontSize: 14,
                color: Theme.of(context).colorScheme.secondary.withOpacity(0.8),
              ),
            ),
            const SizedBox(height: 22),
            Text(S.of(context).paymentMethods,
                style: const TextStyle(fontSize: 16)),
            const SizedBox(height: 5),
            Text(
              S.of(context).chooseYourPaymentMethod,
              style: TextStyle(
                fontSize: 14,
                color: Theme.of(context).colorScheme.secondary.withOpacity(0.6),
              ),
            ),
            Services().widget.renderPayByWallet(context),
            const SizedBox(height: 20),
            Consumer<PaymentMethodModel>(builder: (context, model, child) {
              if (model.isLoading) {
                return SizedBox(height: 100, child: kLoadingWidget(context));
              }
              errorString = model.message;
              printLog('in widget error: ');
              printLog(model.message);

              if (model.message != null) {
                var errorMessage = model.message!
                    .replaceFirst('Exception: Error: ', ''); //.trim();
                printLog(errorMessage);
                //code comment and add on 20-7-2023 by natasha for remove error message from payment page
                // return SizedBox(
                //   height: 100,
                //   child: Center(
                //       child: Text(errorMessage, //model.message!,
                //           style: const TextStyle(color: kErrorRed))),
                // );
                //code comment and add on 20-7-2023 by natasha for remove error message from payment page
                //code added on 21-7-23 by natasha for show payment method structure when error occur
                return Column(
                  children: [
                    RadioListTile<String>(
                      title: const Text('Cash on delivery'),
                      value: 'cod',
                      groupValue: selectedValue,
                      onChanged: (value) {
                        setState(() {
                          selectedValue = value;
                        });
                      },
                    ),
                    const SizedBox(
                      height: 6,
                    ),
                    RadioListTile<String>(
                      title: Row(
                        children: [
                          Image.asset(
                            'assets/images/1_RAZORPAY_LOGO.png',
                            width: 120,
                            height: 50,
                          ),
                        ],
                      ),
                      value: 'razorpay',
                      groupValue: selectedValue,
                      onChanged: (value) {
                        setState(() {
                          selectedValue = value;
                        });
                      },
                    ),
                    //   ),
                    const Divider(height: 1)
                  ],
                );
                //code end on 21-7-23 by natasha for show payment method structure when error occur
              }

              if (selectedId == null && model.paymentMethods.isNotEmpty) {
                selectedId =
                    model.paymentMethods.firstWhere((item) => item.enabled!).id;
              }

              return Column(
                children: <Widget>[
                  for (int i = 0; i < model.paymentMethods.length; i++)
                    model.paymentMethods[i].enabled!
                        ? Services().widget.renderPaymentMethodItem(
                            context, model.paymentMethods[i], (i) {
                            setState(() {
                              selectedId = i;
                            });
                          }, selectedId)
                        : Container()
                ],
              );
            }),
            const SizedBox(
              height: 8,
            ),
            const Text(
              'UPI/Card/Net banking or Online payment',
              style: TextStyle(
                fontSize: 14,
              ),
            ),
            const SizedBox(
              height: 8,
            ),
            isLoadingButton
                ?
                const SizedBox()
                // Row(children: [
                //     Expanded(
                //       child: ButtonTheme(
                //         height: 45,
                //         child: ElevatedButton(
                //           style: ElevatedButton.styleFrom(
                //             elevation: 0,
                //             onPrimary: Colors.white,
                //             primary: Theme.of(context).primaryColor,
                //           ),
                //           onPressed: () {}, //end of onPressed
                //           child:const CircularProgressIndicator()
                //         ),
                //       ),
                //     ),
                //   ])
                : Row(children: [
                    Expanded(
                      child: ButtonTheme(
                        height: 45,
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            elevation: 0,
                            onPrimary: Colors.white,
                            primary: Theme.of(context).primaryColor,
                          ),
                          onPressed: () async {
                            setState(() {
                              isLoadingText = true;
                            });
                            //  isLoadingButton = true; //22-7-2023 for when click on  one tap button disable
                            //code added for internet connectivity check on 13-7-2023 by natasha
                            var isConnected = await checkInternetConnection();
                            if (isConnected) {
                              // printLog('Connected to the internet');
                              //code added on 19-7-2023 for show pop up go to cart when product error occur by natasha and gaurav
                              printLog('in place order');
                              final paymentMethodModel3 =
                                  Provider.of<PaymentMethodModel>(context,
                                      listen: false);
                              if (paymentMethodModel3.message != null) {
                                var errorMessage = paymentMethodModel3.message!
                                    .replaceFirst(
                                        'Exception: Error: ', ''); //.trim();
                                printLog(
                                    'payment error in place order function ${paymentMethodModel3.message}');
                                var containsOops = paymentMethodModel3.message!
                                    .contains(
                                        'Error:'); //Error: added from backend for show pop up only when error relted to stock
                                if (containsOops) {
                                  await showDialog(
                                    context: context,
                                    //barrierDismissible: false,
                                    builder: (context) => WillPopScope(
                                      onWillPop: () async {
                                        return Future.value(false);
                                      },
                                      child: AlertDialog(
                                        title: Text(errorMessage,
                                            style: const TextStyle(
                                                color: kErrorRed,
                                                fontFamily: 'Roboto')),
                                        content: ElevatedButton(
                                          style: ElevatedButton.styleFrom(
                                            elevation: 0,
                                            onPrimary: Colors.white,
                                            primary:
                                                Theme.of(context).primaryColor,
                                          ),
                                          onPressed: () {
                                            setState(() {
                                              isLoadingText = false;
                                              //isRefreshCart = true;
                                            });
                                            getProductErrorListData(
                                                context, cartModel);
                                            printLog('pop');
                                            //  isLoadingButton = false; //22-7-2023 for when click on one tap button enable in pop up
                                            // Navigator.of(context).pop();
                                            // Navigator.of(context).pop();
                                            //code added by gaurav on 20-7-23
                                            Navigator.pop(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        const MyCart()));
                                            Navigator.pop(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        const MyCart()));
                                          },
                                          child: const Text(
                                            'Go to Cart',
                                            style: TextStyle(
                                              fontSize: 15,
                                              fontWeight: FontWeight.w600,
                                              color: kDarkBG,
                                              //  Theme.of(context).colorScheme.secondary,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  );
                                } else {
                                  printLog(
                                      'pop up not show for other messages');
                                  setState(() {
                                    isLoadingText = false;
                                  });
                                  //code added on 23-7-2023 by natasha for show error other than product error
                                  var errorMessage = paymentMethodModel3
                                      .message!
                                      .replaceFirst('Exception: ', '');
                                  ScaffoldMessenger.of(context).showSnackBar(
                                    SnackBar(
                                      content: Text(errorMessage),
                                    ),
                                  );
                                  //code end on 23-7-2023 by natasha for show error other than product error
                                  //  isLoadingButton = false; //22-7-2023 for when click on one tap button enable in pop up
                                }
                              } else {
                                // //code end on 19-7-2023 for show pop up by natasha and gaurav
                                if (selectedId == 'cod') {
                                  cashOnDeliveryMethod = true;
                                  printLog(
                                      'selectID on place order $selectedId');
                                  printLog(
                                      'selectID on place order $cashOnDeliveryMethod');
                                } else {
                                  cashOnDeliveryMethod = false;
                                }
                                printLog('About to place order');
                                // count++;
                                // printLog('count $count');
                                //code changes by natasha on 25-7-2023 for adding is isLoadingButton = false when snack bar occur need to enable place my order button again
                                isLoadingButton = true;
                                if (isPaying || selectedId == null) {
                                  ScaffoldMessenger.of(context).showSnackBar(
                                    const SnackBar(
                                      content:
                                          Text('Please wait page is loading.'),
                                    ),
                                  );
                                  isLoadingButton = false;
                                  setState(() {
                                    isLoadingText = false;
                                  });
                                } else {
                                  placeOrder(paymentMethodModel, cartModel,
                                      errorString);
                                }
                                // isPaying || selectedId == null //place order button
                                //     ?   ScaffoldMessenger.of(
                                //     context)
                                //     .showSnackBar(
                                //   const SnackBar(
                                //     content: Text('Please wait page is loading.'),
                                //   ),
                                // )
                                // // Navigator.pop(
                                // //         context) // code added in 24-6-2023 for go to cart page by natasha //print('show snack bar')//showSnackbar
                                //     : placeOrder(
                                //         paymentMethodModel, cartModel, errorString);
                                //code changes by natasha on 25-7-2023 for adding is isLoadingButton = false when snack bar occur need to enable place my order button again
                              }
                            } else {
                              // No internet connection
                              printLog('No internet connection');
                              //code added on 23-7-23 by natasha for remove text checking order
                              setState(() {
                                isLoadingText = false;
                              });
                              //code end on 23-7-23 by natasha for remove text checking order
                              await showDialogNotInternet(context);
                            }
                            //code ended on 13-7-2023
                            // if(selectedId == 'cod') {
                            //   cashOnDeliveryMethod = true;
                            //   print('selectID on place order $selectedId');
                            //   print('selectID on place order $cashOnDeliveryMethod');
                            // }else{
                            //   cashOnDeliveryMethod = false;
                            // }
                            // isPaying || selectedId == null  //place order button
                            //    ? Navigator.pop(
                            //        context) // code added in 24-6-2023 for go to cart page by natasha //print('show snack bar')//showSnackbar
                            //    : placeOrder(paymentMethodModel, cartModel, errorString);
                          }, //end of onPressed
                          child: Text(
                            isLoadingButton == true
                                ? S.of(context).loading
                                : S.of(context).placeMyOrder,
                            style: const TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.w600,
                              color: kDarkBG,
                              //  Theme.of(context).colorScheme.secondary,
                            ),
                          ),
                        ),
                      ),
                      // child:  getPlaceOrder(paymentMethodModel,context),
                      // child: getPlace(),
                    ),
                  ]),

            //isLoadingButton
            isLoadingText
                ? const Text('Checking the inventory to place order...')
                : const SizedBox(), //code added for showing order in process

            if (kPaymentConfig['EnableShipping'] ||
                kPaymentConfig['EnableAddress'] ||
                (kPaymentConfig['EnableReview'] ?? true))
              Center(
                child: TextButton(
                  onPressed: () {
                    isPaying ? showSnackbar : widget.onBack!();
                  },
                  child: Text(
                    (kPaymentConfig['EnableReview'] ?? true)
                        ? S.of(context).goBackToReview
                        : kPaymentConfig['EnableShipping']
                            ? 'Go back to delivery slot' //S.of(context).goBackToShipping // 18-4-2023 text change
                            : S.of(context).goBackToAddress,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      decoration: TextDecoration.underline,
                      fontSize: 15,
                      color: Theme.of(context).colorScheme.secondary,
                    ),
                  ),
                ),
              )
          ],
        ));
  }

  void showSnackbar() {
    Tools.showSnackBar(
        Scaffold.of(context), S.of(context).orderStatusProcessing);
  }

  void placeOrder(PaymentMethodModel paymentMethodModel, CartModel cartModel,
      errorString) async {
    final currencyRate =
        Provider.of<AppModel>(context, listen: false).currencyRate;
    //code added for payment validation on 24-6-2023 by natasha
    printLog('place order');
    printLog(errorString);
    final userModel = Provider.of<UserModel>(context, listen: false);
    var prefs = await SharedPreferences
        .getInstance(); //5-7-2023 code added for save userId in SharedPreferences by natasha
    await prefs.setString(
        'userId',
        userModel.user!.id ??
            ''); //5-7-203 code added for save userId in SharedPreferences by natasha
    printLog(
        'User id set in shared preference ${('userId')}'); //5-7-2023 code added for save userId in SharedPreferences by natasha
    await Provider.of<PaymentMethodModel>(context, listen: false)
        .getPaymentMethods(
            cartModel: cartModel,
            shippingMethod: cartModel.shippingMethod,
            token: userModel.user != null ? userModel.user!.cookie : null);
    final paymentMethodModel2 =
        Provider.of<PaymentMethodModel>(context, listen: false);
    printLog(
        'payment error in place order function ${paymentMethodModel2.message}');
    // widget.onLoading!(true);
    isPaying = true;
    if (paymentMethodModel2.message != null) {
      printLog('The error has occurred------------------------');
      //code added on 20-7-2023 for show pop up go to cart when product error occur by natasha
      var errorMessage = paymentMethodModel2.message!
          .replaceFirst('Exception: Error: ', ''); //.trim();
      printLog(
          'payment error in place order function ${paymentMethodModel2.message}');
      var containsOops = paymentMethodModel2.message!.contains(
          'Error:'); //Error: added from backend for show pop up only when error related to stock
      if (containsOops) {
        await showDialog(
          context: context,
          //barrierDismissible: false,
          builder: (context) => WillPopScope(
            onWillPop: () async {
              return Future.value(false);
            },
            child: AlertDialog(
              title: Text(errorMessage,
                  style:
                      const TextStyle(color: kErrorRed, fontFamily: 'Roboto')),
              content: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  elevation: 0,
                  onPrimary: Colors.white,
                  primary: Theme.of(context).primaryColor,
                ),
                onPressed: () {
                  getProductErrorListData(context, cartModel);
                  printLog('pop');
                  setState(() {
                    isLoadingText = false;
                  //  isRefreshCart = true;
                  });
                  //code added by gaurav on 20-7-23 for refreshing cart page
                  Navigator.pop(context,
                      MaterialPageRoute(builder: (context) => const MyCart()));
                  Navigator.pop(context,
                      MaterialPageRoute(builder: (context) => const MyCart()));
                },
                child: const Text(
                  'Go to Cart',
                  style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.w600,
                    color: kDarkBG,
                    //  Theme.of(context).colorScheme.secondary,
                  ),
                ),
              ),
            ),
          ),
        );
      } else {
        printLog('pop up not show for other messages');
        //code added on 23-7-2023 by natasha for show error other than product error
        var errorMessage =
            paymentMethodModel2.message!.replaceFirst('Exception: ', '');
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(errorMessage),
          ),
        );
        //code end on 23-7-2023 by natasha for show error other than product error
      }
      //code end on 19-7-2023 for show pop up go to cart when product error occur by natasha
    }
    //code ended for payment validation on 24-6-2023
    if (paymentMethodModel2.paymentMethods.isNotEmpty &&
        paymentMethodModel2.message == null) {
      //code changes on 24-6-2023
      widget.onLoading!(true); //code changes on 24-6-2023 for loader by natasha
      setState(() {
        isLoadingText = false;
      });
      //code added for payment validation on 24-6-2023 by natasha
      printLog('paymentMethodModel.message ${paymentMethodModel.message}');
      final paymentMethod = paymentMethodModel.paymentMethods
          .firstWhere((item) => item.id == selectedId);
      printLog('Selected Payment Method1 is $paymentMethod');
      printLog('Selected Payment Method2 is $selectedId');
      var isSubscriptionProduct = cartModel.item.values.firstWhere(
              (element) =>
                  element?.type == 'variable-subscription' ||
                  element?.type == 'subscription',
              orElse: () => null) !=
          null;
      Provider.of<CartModel>(context, listen: false)
          .setPaymentMethod(paymentMethod);

      /// Use Native payment

      /// Direct bank transfer (BACS)

      if (!isSubscriptionProduct && paymentMethod.id!.contains('bacs')) {
        widget.onLoading!(false);
        isPaying = false;

        await showModalBottomSheet(
            context: context,
            builder: (sContext) => Container(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 20.0, vertical: 10.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          GestureDetector(
                            onTap: () => Navigator.of(context).pop(),
                            child: Text(
                              S.of(context).cancel,
                              style: Theme.of(context)
                                  .textTheme
                                  .caption!
                                  .copyWith(color: Colors.red),
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(height: 10),
                      Text(
                        paymentMethod.description!,
                        style: Theme.of(context).textTheme.caption,
                      ),
                      const Expanded(child: SizedBox(height: 10)),
                      ElevatedButton(
                        onPressed: () {
                          Navigator.pop(context);
                          widget.onLoading!(true);
                          isPaying = true;
                          Services().widget.placeOrder(
                            context,
                            cartModel: cartModel,
                            onLoading: widget.onLoading,
                            paymentMethod: paymentMethod,
                            success: (Order order) async {
                              for (var item in order.lineItems) {
                                var product =
                                    cartModel.getProductById(item.productId!);
                                if (product?.bookingInfo != null) {
                                  product!.bookingInfo!.idOrder = order.id;
                                  var booking =
                                      await createBooking(product.bookingInfo)!;

                                  Tools.showSnackBar(
                                      Scaffold.of(context),
                                      booking
                                          ? 'Booking success!'
                                          : 'Booking error!');
                                }
                              }
                              log('ok button');

                              widget.onFinish!(order);

                              widget.onLoading!(false);
                              isPaying = false;
                            },
                            error: (message) {
                              widget.onLoading!(false);
                              if (message != null) {
                                Tools.showSnackBar(
                                    Scaffold.of(context), message);
                              }
                              isPaying = false;
                            },
                          );
                        },
                        style: ElevatedButton.styleFrom(
                          onPrimary: Colors.white,
                          primary: Theme.of(context).primaryColor,
                        ),
                        child: Text(
                          S.of(context).ok,
                        ),
                      ),
                      const SizedBox(height: 10),
                    ],
                  ),
                ));

        return;
      }

      /// RazorPay payment
      /// Check below link for parameters:
      /// https://razorpay.com/docs/payment-gateway/web-integration/standard/#step-2-pass-order-id-and-other-options
      if (!isSubscriptionProduct &&
          paymentMethod.id!.contains(kRazorpayConfig['paymentMethodId']) &&
          kRazorpayConfig['enabled'] == true) {
        printLog('inside razorpay_payment');

        await Services().api.createRazorpayOrder({
          'amount': (PriceTools.getPriceValueByCurrency(cartModel.getTotal()!,
                      cartModel.currency!, currencyRate) *
                  100)
              .toInt()
              .toString(),
          'currency': cartModel.currency,
        }).then((value) async {
          printLog('value..... $value');
          final _razorServices = RazorServices(
            amount: (PriceTools.getPriceValueByCurrency(cartModel.getTotal()!,
                        cartModel.currency!, currencyRate) *
                    100)
                .toInt()
                .toString(),
            keyId: kRazorpayConfig['keyId'],
            delegate: this,
            orderId: value,
            userInfo: RazorUserInfo(
              email: cartModel.address!.email,
              phone: cartModel.address!.phoneNumber,
              fullName:
                  '${cartModel.address!.firstName} ${cartModel.address!.lastName}',
            ),
          );
          printLog('razorpay open razorpay failed value $razorpayFailed');
          // code added on 15-7-2023 for placed order before payment by natasha
          // await createOrder(paid: false).then((value) {
          //   printLog('inside create order in razorpay paid status false');
          //   widget.onLoading!(false);
          //   isPaying = false;
          // });
          // code end 15-7-2023 for placed order before payment by natasha
          await _razorServices.openPayment(cartModel.currency!);
          //code start commenting on 3-5-2023 for placed order before payment by natasha
          // code added on 19-4-2023 for placed order before payment by natasha
          await createOrder(paid: false).then((value) {
            printLog('inside create order in razorpay paid status false');
            widget.onLoading!(false);
            isPaying = false;
          });
          isLoadingButton = false;
          //code end commenting on 3-5-2023 for placed order before payment by natasha
        }).catchError((e) {
          isLoadingButton = false;
          widget.onLoading!(false);
          Tools.showSnackBar(Scaffold.of(context), e);
          isPaying = false;
        });
        return;
      }

      /// Use WebView Payment per frameworks
      Services().widget.placeOrder(
        context,
        cartModel: cartModel,
        onLoading: widget.onLoading,
        paymentMethod: paymentMethod,
        success: (Order? order) async {
          if (order != null) {
            for (var item in order.lineItems) {
              var product = cartModel.getProductById(item.productId!);
              if (product?.bookingInfo != null) {
                product!.bookingInfo!.idOrder = order.id;
                var booking = await createBooking(product.bookingInfo)!;

                Tools.showSnackBar(Scaffold.of(context),
                    booking ? 'Booking success!' : 'Booking error!');
              }
            }
            printLog('cash on delivery web view $cashOnDeliveryMethod');
            widget.onFinish!(order, cashOnDeliveryMethod);
            isLoadingButton = false;
            log('WebView');
          }
          widget.onLoading!(false);
          isPaying = false;
        },
        error: (message) {
          isLoadingButton = false;
          widget.onLoading!(false);
          if (message != null) {
            Tools.showSnackBar(Scaffold.of(context), message);
          }

          isPaying = false;
        },
      );
    }
  }

  Future<bool>? createBooking(BookingModel? bookingInfo) async {
    return Services().api.createBooking(bookingInfo)!;
  }

  Future<void> createOrder(
      {paid = false, bacs = false, cod = false, transactionId = ''}) async {
    printLog('In create order function');
    await createOrderOnWebsite(
        paid: paid,
        bacs: bacs,
        cod: cod,
        transactionId: transactionId,
        onFinish: (Order? order) async {
          //code start commenting on 3-5-2023 for shows payment flow as before
          printLog('order number ${order?.number}');
          var prefs = await SharedPreferences.getInstance();
          await prefs.setString('OrderId', order?.number ?? '');
          printLog('order id from shared preference');
          printLog(prefs.getString('OrderId'));
          //code end commenting on 3-5-2023 for shows payment flow as before
          if (!transactionId.toString().isEmptyOrNull && order != null) {
            await Services()
                .api
                .updateOrderIdForRazorpay(transactionId, order.number);
          }
          printLog('order in first api call $order $cashOnDeliveryMethod');
          widget.onFinish!(order, cashOnDeliveryMethod);
          log('create order razor');
        });
  }

  Future<void> createOrderOnWebsite(
      {paid = false,
      bacs = false,
      cod = false,
      transactionId = '',
      required Function(Order?) onFinish}) async {
    printLog('in create order on website function');

    widget.onLoading!(true);
    await Services().widget.createOrder(
      context,
      paid: paid,
      cod: cod,
      bacs: bacs,
      transactionId: transactionId,
      onLoading: widget.onLoading,
      success: onFinish,
      error: (message) {
        Tools.showSnackBar(Scaffold.of(context), message);
      },
    );
    widget.onLoading!(false);
  }

  //code start commenting on 3-5-2023 for shows payment flow as before
  // adding async await for calling update order api on 20-4-2023
  @override
  Future<void> handlePaymentSuccess(PaymentSuccessResponse response) async {
    razorpayFailed = false;
    printLog('in handle payment success $razorpayFailed ');
    widget.onPageLoad!(razorpayFailed);
    printLog(response.paymentId);
    var transactionId = response.paymentId;
    var prefs = await SharedPreferences.getInstance();
    printLog('order id from shared preference in payment success');
    printLog(prefs.getString('OrderId'));
    var orderId = prefs.getString('OrderId');
    var userId = prefs.getString('userId'); //5-7-2023
    printLog('userId $userId'); //5-7-2023
    var id = userId; //'6848';//user?.id;
    printLog('userid $id');

    final endpoint = '$baseUrl/wp-json/api/flutter_order/update_order_status';
    //const endpoint = 'https://qwickpick.com/wp-json/api/flutter_order/update_order_status';
    final resp = await httpPost(endpoint.toUri()!, body: {
      'id': orderId, //'5768',
      'status': 'processing',
      'user_id': id
    });
    printLog('order status updated successfully');
    printLog(resp.statusCode);
    if (resp.statusCode >= 200 && resp.statusCode < 300) {
      printLog('response from update order');
      printLog(resp.body);
      printLog('transactionId:- $transactionId');
      if (!transactionId.toString().isEmptyOrNull && orderId != null) {
        printLog(!transactionId.toString().isEmptyOrNull);
        printLog('update transaction id');
        await Services().api.updateOrderIdForRazorpay(transactionId, orderId);
      }
      printLog('navigate to checkout screen');
      // widget.onPageLoad!();
      // await Navigator.pushNamed(context,RouteList.successScreen );
    } else {
      printLog(resp.reasonPhrase);
    }
  }
  //code end commenting on 3-5-2023 for shows payment flow as before

  // @override
  // void handlePaymentSuccess(PaymentSuccessResponse response) async {
  //   await createOrder(paid: true, transactionId: response.paymentId)
  //       .then((value) {
  //     widget.onLoading!(false);
  //     isPaying = false;
  //   });
  // }

  // @override
  // Future<void> handlePaymentFailure(PaymentFailureResponse response) async {
  //   widget.onLoading!(false);
  //   isPaying = false;
  //   final body = convert.jsonDecode(response.message!);
  //   if (body['error'] != null &&
  //       body['error']['reason'] != 'payment_cancelled') {
  //     Tools.showSnackBar(Scaffold.of(context), body['error']['description']);
  //   }
  // }

  //code start commenting on 3-5-2023 for shows payment flow as before
  void rebuildAllChildren(BuildContext context) {
    printLog('at rebuild');
    void rebuild(Element el) {
      el.markNeedsBuild();
      el.visitChildren(rebuild);
    }

    (context as Element).visitChildren(rebuild);
  }

  @override
  Future<void> handlePaymentFailure(PaymentFailureResponse response) async {
    razorpayFailed = true;
    printLog('before setting $razorpayFailed');
    widget.onPageLoad!(razorpayFailed);
    widget.onLoading!(false);
    isPaying = false;

    try {
      printLog('Payment failure response ${response.message}');
      if (response.message == 'Payment processing cancelled by user') {
        printLog('mounted $mounted'); //04072023
      }
      final body = convert.jsonDecode(response.message!);
      if (body['error'] != null &&
          body['error']['reason'] != 'payment_cancelled') {
        // Tools.showSnackBar(Scaffold.of(context), body['error']['description']);
      }
    } catch (e) {
      printLog('Error at failed payment $e');
    }

    printLog(response.message);
    printLog('in failed function');
    //code added on 21-4-2023 for pass status failed for order by natasha
    var prefs = await SharedPreferences.getInstance();
    printLog('order id from shared preference in payment failed');
    printLog(prefs.getString('OrderId'));
    var orderId = prefs.getString('OrderId');
    var userId = prefs.getString('userId');
    printLog('userId $userId');
    var id = userId; //user?.id;
    printLog('userid $id');
    final endpoint = '$baseUrl/wp-json/api/flutter_order/update_order_status';
    //const endpoint = 'https://qwickpick.com/wp-json/api/flutter_order/update_order_status';
    final resp = await httpPost(endpoint.toUri()!, body: {
      'id': orderId, //'5770',
      'status':
          'pending_payment', //'failed' //using this key from this code which already mentioned 27-5-2023
      'user_id': id
    });
    if (resp.statusCode >= 200 && resp.statusCode < 300) {
      printLog('response from update order when payment failed');
      printLog(resp.body);
    } else {
      printLog(resp.reasonPhrase);
    }
  }
}
