import 'dart:async';
import 'dart:convert';

import 'package:collection/collection.dart' show IterableExtension;
import 'package:connectivity/connectivity.dart';
import 'package:flutter/cupertino.dart' show CupertinoIcons;
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:localstorage/localstorage.dart';
import 'package:provider/provider.dart';

import '../../../common/config.dart';
import '../../../common/constants.dart';
import '../../../generated/l10n.dart';
import '../../../models/entities/user.dart';
import '../../../models/index.dart' show Address, CartModel, Country, UserModel;
import '../../../services/index.dart';
import '../../../widgets/common/dialogs.dart';
import '../../constants.dart';

var globalBillingAddress_3 = '';

class ShippingAddress extends StatefulWidget {
  final Function? onNext;

  const ShippingAddress({this.onNext});

  @override
  _ShippingAddressState createState() => _ShippingAddressState();
}

class _ShippingAddressState extends State<ShippingAddress> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController _cityController = TextEditingController();
  final TextEditingController _streetController = TextEditingController();
  final TextEditingController _blockController = TextEditingController();
  final TextEditingController _zipController = TextEditingController();
  final TextEditingController _stateController = TextEditingController();
  final TextEditingController _countryController = TextEditingController();
  final TextEditingController _apartmentController = TextEditingController();

  final _lastNameNode = FocusNode();
  final _phoneNode = FocusNode();
  final _emailNode = FocusNode();
  final _cityNode = FocusNode();
  final _streetNode = FocusNode();
  final _blockNode = FocusNode();
  final _zipNode = FocusNode();
  final _stateNode = FocusNode();
  final _countryNode = FocusNode();
  final _apartmentNode = FocusNode();

  int addressListAPILength = 0;
  List addressListAPI = [];

  Address? address;
  List<Country>? countries = [];
  List<dynamic> states = [];
  List<Address?> listAddress = [];
  bool showForm = false;
  bool addressStatus = true;
  User? get user => Provider.of<UserModel>(context, listen: false).user;
  var userId;
  bool showLoader = true;
  bool _isApiCallInProgress = false;

  @override
  void dispose() {
    _cityController.dispose();
    _streetController.dispose();
    _blockController.dispose();
    _zipController.dispose();
    _stateController.dispose();
    _countryController.dispose();
    _apartmentController.dispose();

    _lastNameNode.dispose();
    _phoneNode.dispose();
    _emailNode.dispose();
    _cityNode.dispose();
    _streetNode.dispose();
    _blockNode.dispose();
    _zipNode.dispose();
    _stateNode.dispose();
    _countryNode.dispose();
    _apartmentNode.dispose();

    super.dispose();
  }

  Future<void> getDataFromLocal() async {
    var _list = <Address?>[];
    final userBody = {
      'user_id': userId,
    };

    printLog('USER BODY IS $userBody');
    var url = '$baseUrl/wp-json/api/flutter_user/update_user_profile';
    //  'https://qwickpick.com/wp-json/api/flutter_user/update_user_profile';
    final response = await http.post(
      Uri.parse(url),
      body: jsonEncode(userBody),
    );
    var responseBody = response.body;
    printLog('RESPONSE B IS $responseBody');
    var decodedData = json.decode(responseBody);
    decodedAddress = decodedData['address'];
    if (decodedAddress != '') {
      printLog('INSIDE 2nd IF');
      for (var item in decodedAddress as List) {
        final add = Address.fromLocalJson(item);
        _list.add(add);
      }
    }

    setState(() {
      listAddress = _list;
    });
    printLog('<Y LIST IS $_list');
  }

  @override
  void initState() {
    userId = user?.id;
    getDataFromLocal();
    super.initState();
    Future.delayed(
      Duration.zero,
          () async {
        final addressValue =
        await Provider.of<CartModel>(context, listen: false).getAddress();
        // ignore: unnecessary_null_comparison
        if (addressValue != null) {
          setState(() {
            address = addressValue;
            _cityController.text = address?.city ?? '';
            _streetController.text = address?.street ?? '';
            _zipController.text = address?.zipCode ?? '';
            _stateController.text = address?.state ?? '';
            _blockController.text = address?.block ?? '';
            _apartmentController.text = address?.apartment ?? '';
          });
        } else {
          var user = Provider.of<UserModel>(context, listen: false).user;
          setState(() {
            address = Address(country: kPaymentConfig['DefaultCountryISOCode']);
            if (kPaymentConfig['DefaultStateISOCode'] != null) {
              address!.state = kPaymentConfig['DefaultStateISOCode'];
            }
            _countryController.text = address!.country!;
            _stateController.text = address!.state!;
            if (user != null) {
              address!.firstName = user.firstName;
              address!.lastName = user.lastName;
              address!.email = user.email;
            }
          });
        }
        countries = await Services().widget.loadCountries();
        var country = countries!.firstWhereOrNull((element) =>
        element.id == address!.country || element.code == address!.country);
        if (country == null) {
          if (countries!.isNotEmpty) {
            country = countries![0];
            address!.country = countries![0].code;
          } else {
            country = Country.fromConfig(address!.country, null, null, []);
          }
        } else {
          address!.country = country.code;
          address!.countryId = country.id;
        }
        _countryController.text = country.code!;
        if (mounted) {
          setState(() {});
        }
        states = await Services().widget.loadStates(country);
        if (mounted) {
          setState(() {});
        }
      },
    );

    printLog('Show Loader before timer $showLoader');
    Timer(const Duration(seconds: 4), () {
      setState(() {
        showLoader = false;
        printLog('Show Loader after timer $showLoader');
      });
    });
  }

  Future<void> updateState(Address? address) async {
    printLog('update function call');
    printLog(address);
    setState(() {
      _cityController.text = address?.city ?? '';
      _streetController.text = address?.street ?? '';
      _zipController.text = address?.zipCode ?? '';
      _stateController.text = address?.state ?? '';
      _countryController.text = address?.country ?? '';
      this.address?.country = address?.country ?? '';
      _apartmentController.text = address?.apartment ?? '';
      _blockController.text = address?.block ?? '';
    });
  }

  bool checkToSave() {
    final storage = LocalStorage(LocalStorageKey.address);
    var _list = <Address>[];
    try {
      var data = storage.getItem('data');
      if (data != null) {
        for (var item in (data as List)) {
          final add = Address.fromLocalJson(item);
          _list.add(add);
        }
      }
      for (var local in _list) {
        if (local.city != _cityController.text) continue;
        if (local.street != _streetController.text) continue;
        if (local.zipCode != _zipController.text) continue;
        if (local.state != _stateController.text) continue;

        Provider.of<CartModel>(context, listen: false).setAddress(address);
        _loadShipping(beforehand: false);
        widget.onNext!();
        return false;
      }
    } catch (err) {
      printLog(err);
    }
    return true;
  }

  Future<void> saveDataToLocal() async {
    setState(() {
      _isApiCallInProgress = true;
    });
    printLog('Continue payment local data save');
    var _list = <Address?>[];
    _list.add(address);

    try {
      final addressBody = {
        'user_id': userId,
      };
      var addressUrl = '$baseUrl/wp-json/api/flutter_user/update_user_profile';
      // 'https://qwickpick.com/wp-json/api/flutter_user/update_user_profile';
      final addressResponse = await http.post(
        Uri.parse(addressUrl),
        body: jsonEncode(addressBody),
      );
      var addressResponseBody = addressResponse.body;
      var decodedData = json.decode(addressResponseBody);
      var decodedAddress = decodedData['address'];
      if (decodedAddress != '') {
        printLog('INSIDE 2nd IF');
        for (var item in decodedAddress as List) {
          final add = Address.fromLocalJson(item);
          _list.add(add);
        }
      }

      printLog('FINAL LIST TO BE PASSED IS $_list');
      final userBody = {
        'user_id': userId,
        'user_address': _list,
      };
      var url = '$baseUrl/wp-json/api/flutter_user/update_user_profile';
      // 'https://qwickpick.com/wp-json/api/flutter_user/update_user_profile';
      final response = await http.post(
        Uri.parse(url),
        body: jsonEncode(userBody),
      );
      setState(() {
        _isApiCallInProgress = false;
      });
      Provider.of<CartModel>(context, listen: false).setAddress(address);
      _loadShipping(beforehand: false);
      printLog(response);
      widget.onNext!();
    } catch (err) {
      printLog(err);
    }
  }

  String? validateEmail(String value) {
    var valid = RegExp(
        r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
        .hasMatch(value);
    if (valid) {
      return null;
    }
    return 'The E-mail Address must be a valid email address.';
  }

  void _toggleFormVisibility() {
    _cityController.clear();
    _streetController.clear();
    _blockController.clear();
    _zipController.clear();
    _apartmentController.clear();
    // _formKey.currentState!.reset();
    setState(() {
      showForm = !showForm;
      addressStatus = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    userId = user?.id;
    printLog('USER ID IS $userId');
    // var countryName = S.of(context).country;
    // if (_countryController.text.isNotEmpty) {
    //   try {
    //     countryName = picker.CountryPickerUtils.getCountryByIsoCode(
    //         _countryController.text)
    //         .name;
    //   } catch (e) {
    //     countryName = S.of(context).country;
    //   }
    // }

    if (address == null) {
      return SizedBox(height: 100, child: kLoadingWidget(context));
    }
    printLog('ABC IS ${_streetController.text}');
    return SingleChildScrollView(
      child: Column(
        children: [
          showLoader == true
              ? const CircularProgressIndicator()
              : listAddress.isEmpty
              ? const SizedBox()
              : addressStatus == true
              ? Padding(
            padding: const EdgeInsets.all(10),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(3),
              child: GestureDetector(
                onTap: () {},
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                      color: Theme.of(context).primaryColorLight),
                  child: Padding(
                    padding: const EdgeInsets.all(10),
                    child: convertToTextBox(listAddress),
                  ),
                ),
              ),
            ),
          )
              : const SizedBox(),
          listAddress.isEmpty
              ? const SizedBox()
              : ButtonTheme(
            height: 60,
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                elevation: 0.0,
                onPrimary: Theme.of(context).colorScheme.secondary,
                primary: Theme.of(context).primaryColorLight,
              ),
              onPressed: () {
                getDataFromLocal();
                selectAddressBottomSheet(context);
                showForm = false;
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: const <Widget>[
                  Icon(
                    CupertinoIcons.location_solid,
                    size: 16,
                  ),
                  SizedBox(width: 10.0),
                  Text('Change Address'),
                ],
              ),
            ),
          ),
          ButtonTheme(
            height: 60,
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                elevation: 0.0,
                onPrimary: Theme.of(context).colorScheme.secondary,
                primary: Theme.of(context).primaryColorLight,
              ),
              onPressed: _toggleFormVisibility,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: const <Widget>[
                  Icon(
                    CupertinoIcons.add_circled,
                    size: 16,
                  ),
                  SizedBox(width: 10.0),
                  Text('Add new address'),
                ],
              ),
            ),
          ),
          // showForm == false ? SizedBox() : Form(
          Visibility(
            visible: showForm,
            child: Form(
              key: _formKey,
              child: AutofillGroup(
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      //Text(_cityController.text),
                      TextFormField(
                        initialValue: address!.firstName,
                        autofillHints: const [AutofillHints.givenName],
                        decoration:
                        // InputDecoration(labelText: S.of(context).firstName),
                        const InputDecoration(
                          labelText: 'Full Name',
                          labelStyle: TextStyle(color: kGrey600),
                        ),
                        textCapitalization: TextCapitalization.words,
                        textInputAction: TextInputAction.next,
                        validator: (val) {
                          return val!.isEmpty
                              ? S.of(context).firstNameIsRequired
                              : null;
                        },
                        onFieldSubmitted: (_) =>
                            FocusScope.of(context).requestFocus(_lastNameNode),
                        onSaved: (String? value) {
                          address!.firstName = value;
                        },
                      ),

                      TextFormField(
                          initialValue: address!.phoneNumber,
                          autofillHints: const [AutofillHints.telephoneNumber],
                          focusNode: _phoneNode,
                          decoration: const InputDecoration(
                            //labelText: S.of(context).phoneNumber),
                            labelText: 'Contact No',
                            labelStyle: TextStyle(color: kGrey600),
                          ),
                          textInputAction: TextInputAction.next,
                          validator: (val) {
                            return val!.isEmpty
                                ? S.of(context).phoneIsRequired
                                : null;
                          },
                          keyboardType: TextInputType.number,
                          onFieldSubmitted: (_) =>
                              FocusScope.of(context).requestFocus(_emailNode),
                          onSaved: (String? value) {
                            address!.phoneNumber = value;
                          }),

                      const SizedBox(height: 10),

                      TextFormField(
                          controller: _apartmentController,
                          focusNode: _apartmentNode,
                          validator: (val) {
                            return val!.isEmpty
                                ? 'The house no & block no field is required'
                                : null;
                          },
                          decoration: const InputDecoration(
                            labelText: 'House no & floor ',
                            labelStyle: TextStyle(color: kGrey600),
                          ),
                          textInputAction: TextInputAction.next,
                          onFieldSubmitted: (_) =>
                              FocusScope.of(context).requestFocus(_blockNode),
                          onSaved: (String? value) {
                            address!.apartment = value;
                          }),
                      TextFormField(
                          controller: _blockController,
                          focusNode: _blockNode,
                          validator: (val) {
                            return val!.isEmpty
                                ? 'The building & block no field is required'
                                : null;
                          },
                          decoration: const InputDecoration(
                            labelText: 'Building & block no',
                            labelStyle: TextStyle(color: kGrey600),
                          ),
                          textInputAction: TextInputAction.next,
                          onFieldSubmitted: (_) =>
                              FocusScope.of(context).requestFocus(_streetNode),
                          onSaved: (String? value) {
                            address!.block = value;
                          }),
                      TextFormField(
                          controller: _streetController,
                          autofillHints: const [
                            AutofillHints.fullStreetAddress
                          ],
                          focusNode: _streetNode,
                          validator: (val) {
                            return val!.isEmpty
                                ? S.of(context).streetIsRequired
                                : null;
                          },
                          decoration: const InputDecoration(
                            labelText: 'Landmark & area name',
                            labelStyle: TextStyle(color: kGrey600),
                          ),
                          textInputAction: TextInputAction.next,
                          onFieldSubmitted: (_) =>
                              FocusScope.of(context).requestFocus(_zipNode),
                          onSaved: (String? value) {
                            address!.street = value;
                          }),

                      Padding(
                        padding: const EdgeInsets.only(left: 12.0, top: 24),
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                  elevation: 0.0,
                                  primary: Theme.of(context).primaryColorLight,
                                ),
                                onPressed: () {
                                  setState(() {
                                    showForm = !showForm;
                                  });
                                  if (listAddress.isNotEmpty) {
                                    selectAddressBottomSheet(context);
                                  }
                                },
                                child: Text(
                                  'Cancel',
                                  style: TextStyle(
                                    fontSize: 12,
                                    fontWeight: FontWeight.w600,
                                    color:
                                    Theme.of(context).colorScheme.secondary,
                                  ),
                                ),
                              ),
                              Container(width: 20),
                              ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                  elevation: 0.0,
                                  onPrimary: Colors.white,
                                  primary: Theme.of(context).primaryColor,
                                ),
                                onPressed: () {
                                  if (!checkToSave()) return;
                                  if (_formKey.currentState!.validate()) {
                                    _formKey.currentState!.save();
                                    Provider.of<CartModel>(context,
                                        listen: false)
                                        .setAddress(address);
                                    saveDataToLocal();
                                  }
                                  globalBillingAddress_3 =
                                      _streetController.text;
                                },
                                child: const Text(
                                  'Save and continue',
                                  style: TextStyle(
                                    fontSize: 12,
                                    fontWeight: FontWeight.w600,
                                    color: kDarkBG,
                                    //  Theme.of(context).colorScheme.secondary,
                                  ),
                                ),
                              ),
                            ]),
                      ),
                      const SizedBox(height: 50),
                    ]),
              ),
            ),
          ),

          const SizedBox(
            height: 12,
          ),
          if (_isApiCallInProgress) // Show a transparent overlay when API call is in progress
            Center(
              child: CircularProgressIndicator(
                color: Theme.of(context).primaryColor,
              ),
            ),
          showForm == true
              ? const SizedBox()
              : listAddress.isNotEmpty
              ? ElevatedButton(
            style: ElevatedButton.styleFrom(
              elevation: 0.0,
              onPrimary: Colors.white,
              primary: Theme.of(context).primaryColor,
            ),
            onPressed: _onNext,
            child: Text(('Continue to slot booking'),
                style: TextStyle(
                    fontSize: 14,
                    color: Theme.of(context).colorScheme.secondary,
                    fontWeight: FontWeight.w700)),
          )
              : const SizedBox(),
          const SizedBox(height: 20),
        ],
      ),
    );
  }

  /// Load Shipping beforehand
  void _loadShipping({bool beforehand = true}) {
    Services().widget.loadShippingMethods(
        context, Provider.of<CartModel>(context, listen: false), beforehand);
  }

  //code added for internet connectivity check on 13-7-2023 by natasha
  Future<bool> checkInternetConnection() async {
    printLog('in check internet connection');
    var connectivityResult = await (Connectivity().checkConnectivity());
    return connectivityResult != ConnectivityResult.none;
  }
  //code ended on 13-7-2023

  /// on tap to Next Button
  Future<void> _onNext() async {
    setState(() {
      showForm = !showForm;
    });
    //code added for internet connectivity check on 13-7-2023 by natasha
    var isConnected = await checkInternetConnection();
    if (isConnected) {
      // Internet connection is available
      printLog('Connected to the internet');
      Future.delayed(const Duration(milliseconds: 20), () {
        // kLoadingWidget(context);
        if (_formKey.currentState!.validate()) {
          printLog('hii');
          _formKey.currentState!.save();
          Provider.of<CartModel>(context, listen: false).setAddress(address);
          _loadShipping(beforehand: false);
          // saveDataToLocal();
          widget.onNext!();
        }
      });
    } else {
      // No internet connection
      printLog('No internet connection');
       await showDialogNotInternet(context);
      // await Navigator.push(
      //   context,
      //   MaterialPageRoute(
      //       builder: (context) => const InternetLossPage()
      //   ),
      // );
    }
    //code ended on 13-7-2023
    // Future.delayed(const Duration(milliseconds: 20), () {
    //   // kLoadingWidget(context);
    //   if (_formKey.currentState!.validate()) {
    //     printLog('hii');
    //     _formKey.currentState!.save();
    //     Provider.of<CartModel>(context, listen: false).setAddress(address);
    //     _loadShipping(beforehand: false);
    //     // saveDataToLocal();
    //     widget.onNext!();
    //   }
    // });
  }

  Widget renderStateInput() {
    if (states.isNotEmpty) {
      var items = <DropdownMenuItem>[];
      for (var item in states) {
        items.add(
          DropdownMenuItem(
            value: item.id,
            child: Text(item.name),
          ),
        );
      }
      String? value;

      Object? firstState = states.firstWhereOrNull(
              (o) => o.id.toString() == address!.state.toString());

      if (firstState != null) {
        value = address!.state;
      }

      return DropdownButton(
        items: items,
        value: value,
        onChanged: (dynamic val) {
          setState(() {
            address!.state = val;
          });
        },
        isExpanded: true,
        itemHeight: 70,
        hint: Text(S.of(context).stateProvince),
      );
    } else {
      return TextFormField(
        controller: _stateController,
        autofillHints: const [AutofillHints.addressState],
        validator: (val) {
          return val!.isEmpty ? S.of(context).streetIsRequired : null;
        },
        decoration: InputDecoration(labelText: S.of(context).stateProvince),
        onSaved: (String? value) {
          address!.state = value;
        },
      );
    }
  }

  selectAddressBottomSheet(context) {
    return showModalBottomSheet(
      context: context,
      enableDrag: false,
      isDismissible: false,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(20),
          topRight: Radius.circular(20),
        ),
      ),
      builder: (context) {
        return WillPopScope(
          onWillPop: () async {
            //18-6-23 code added for show msg for back button by natasha
            Future.delayed(const Duration(seconds: 1), () {
              Navigator.of(context).pop();
            });
            await showDialog(
              context: context,
              builder: (context) => const AlertDialog(
                title: Text(
                  'Please click on address to select',
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600),
                ),
              ),
            );
            //18-6-23 code ended for show msg for back button
            return false;
          },
          child: SingleChildScrollView(
            child: Column(
              children: [
                const Padding(
                  padding: EdgeInsets.only(top: 24.0),
                  child: Text(
                    'Select Delivery Address',
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                  ),
                ),
                const SizedBox(
                  height: 16,
                ),
                ...List.generate(listAddress.length, (index) {
                  return Padding(
                    padding: const EdgeInsets.all(10),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(3),
                      child: GestureDetector(
                        onTap: () {
                          addressStatus = true;
                          Provider.of<CartModel>(context, listen: false)
                              .setAddress(listAddress[index]);
                          Navigator.of(context).pop();
                          updateState(listAddress[index]);
                          primaryIndex = index;
                        },
                        child: Padding(
                          padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                          child: Container(
                            width: MediaQuery.of(context).size.width,
                            decoration: BoxDecoration(
                              // color: Theme.of(context).primaryColorLight,
                              // color: Theme.of(context).primaryColor,
                              color: _apartmentController.text ==
                                  listAddress[index]!.apartment &&
                                  _blockController.text ==
                                      listAddress[index]!.block &&
                                  _streetController.text ==
                                      listAddress[index]!.street
                                  ? Theme.of(context).primaryColor
                                  : Colors.white,
                              borderRadius: BorderRadius.circular(14),
                            ),
                            // color:Colors.grey[100]),
                            child: Padding(
                              padding: const EdgeInsets.all(12),
                              child: Row(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 10),
                                    child: Column(
                                      children: const [
                                        Icon(
                                          Icons.home,
                                          // color: Theme.of(context).primaryColor,
                                          color: Colors.black,
                                          size: 20,
                                        ),
                                      ],
                                    ),
                                  ),
                                  const SizedBox(width: 10),
                                  Expanded(
                                    child: convertToCard(
                                        context, listAddress[index]!),
                                    // child: convertToCard(
                                    //     context),
                                  ),
                                  GestureDetector(
                                    onTap: () {
                                      removeData(index, listAddress);
                                      printLog('INDEX ON TAP IS $index');
                                      Navigator.pop(context);
                                    },
                                    child: const Icon(
                                      Icons.delete,
                                      // color: Theme.of(context).primaryColor,
                                      color: Colors.black,
                                      size: 20,
                                    ),
                                  ),
                                  const SizedBox(width: 10),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  );
                })
              ],
            ),
          ),
        );
      },
    );
  }

  void removeData(int index, List<Address?> listAddress) async {
    printLog('LIST ADDRESS BEFORE DELETING');
    printLog(listAddress);
    printLog(listAddress.removeAt(index));
    printLog('LIST ADDRESS After DELETING');
    printLog(listAddress);
    final userBody = {
      'user_id': userId,
      'user_address': listAddress.isEmpty ? '' : listAddress,
    };
    var url = '$baseUrl/wp-json/api/flutter_user/update_user_profile';
    // 'https://qwickpick.com/wp-json/api/flutter_user/update_user_profile';
    final response = await http.post(
      Uri.parse(url),
      body: jsonEncode(userBody),
    );
    var statusCode = response.statusCode;
    if (statusCode == 200) {
      var responseBody = response.body;
      var decodeBody = json.decode(responseBody);
      var decodeAddress = decodeBody['address'];
      if (decodeAddress != '') {
        int addressLength = decodeAddress.length;
        if (addressLength > 0) {
          await updateState(listAddress[0]);
        }
      }
    }
    if (listAddress.isEmpty) {
      Navigator.pop(context);
    }
    await getDataFromLocal();
  }

  Widget convertToTextBox(List<Address?> listAddress) {
    //Widget convertToCard(BuildContext context) {
    if (listAddress.isNotEmpty) {
      if (_apartmentController.text.isEmpty) {
        updateState(listAddress[primaryIndex]);
      }
    }
    final s = S.of(context);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        const SizedBox(height: 10.0),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              'Apartment:  ',
              style: TextStyle(
                  color: Theme.of(context).colorScheme.secondary,
                  fontWeight: FontWeight.w600),
            ),
            Flexible(
              child: Column(
                children: <Widget>[Text(_apartmentController.text)],
              ),
            )
          ],
        ),
        const SizedBox(height: 4.0),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              'Block:  ',
              style: TextStyle(
                  color: Theme.of(context).colorScheme.secondary,
                  fontWeight: FontWeight.w600),
            ),
            Flexible(
              child: Column(
                children: <Widget>[Text(_blockController.text)],
              ),
            )
          ],
        ),
        const SizedBox(height: 4.0),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              '${s.streetName}:  ',
              style: TextStyle(
                  color: Theme.of(context).colorScheme.secondary,
                  fontWeight: FontWeight.w600),
            ),
            Flexible(
              child: Column(
                children: <Widget>[Text(_streetController.text)],
              ),
            )
          ],
        ),
        const SizedBox(height: 4.0),
        const SizedBox(height: 10.0),
      ],
    );
  }

  Widget convertToCard(BuildContext context, Address address) {
    final s = S.of(context);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        const SizedBox(height: 10.0),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              'Apartment:  ',
              style: TextStyle(
                  color: Theme.of(context).colorScheme.secondary,
                  fontWeight: FontWeight.w600),
            ),
            Flexible(
              child: Column(
                children: <Widget>[Text(address.apartment.toString())],
              ),
            )
          ],
        ),
        const SizedBox(height: 4.0),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              'Block:  ',
              style: TextStyle(
                  color: Theme.of(context).colorScheme.secondary,
                  fontWeight: FontWeight.w600),
            ),
            Flexible(
              child: Column(
                children: <Widget>[Text(address.block.toString())],
              ),
            )
          ],
        ),
        const SizedBox(height: 4.0),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              '${s.streetName}:  ',
              style: TextStyle(
                  color: Theme.of(context).colorScheme.secondary,
                  fontWeight: FontWeight.w600),
            ),
            Flexible(
              child: Column(
                children: <Widget>[Text(address.street.toString())],
              ),
            )
          ],
        ),
        const SizedBox(height: 10.0),
      ],
    );
  }

  // void _openCountryPickerDialog() => showDialog(
  //   context: context,
  //   useRootNavigator: false,
  //   builder: (contextBuilder) => countries!.isEmpty
  //       ? Theme(
  //     data: Theme.of(context).copyWith(primaryColor: Colors.pink),
  //     child: SizedBox(
  //       height: 500,
  //       child: picker.CountryPickerDialog(
  //           titlePadding: const EdgeInsets.all(8.0),
  //           contentPadding: const EdgeInsets.all(2.0),
  //           searchCursorColor: Colors.pinkAccent,
  //           searchInputDecoration:
  //           const InputDecoration(hintText: 'Search...'),
  //           isSearchable: true,
  //           title: Text(S.of(context).country),
  //           onValuePicked: (picker_country.Country country) async {
  //             _countryController.text = country.isoCode;
  //             address!.country = country.isoCode;
  //             if (mounted) {
  //               setState(() {});
  //             }
  //             final c =
  //             Country(id: country.isoCode, name: country.name);
  //             states = await Services().widget.loadStates(c);
  //             if (mounted) {
  //               setState(() {});
  //             }
  //           },
  //           itemBuilder: (country) {
  //             return Row(
  //               children: <Widget>[
  //                 picker.CountryPickerUtils.getDefaultFlagImage(
  //                     country),
  //                 const SizedBox(
  //                   width: 8.0,
  //                 ),
  //                 Expanded(child: Text(country.name)),
  //               ],
  //             );
  //           }),
  //     ),
  //   )
  //       : Dialog(
  //     child: SingleChildScrollView(
  //       child: Column(
  //         children: List.generate(
  //           countries!.length,
  //               (index) {
  //             return GestureDetector(
  //               onTap: () async {
  //                 setState(() {
  //                   _countryController.text = countries![index].code!;
  //                   address!.country = countries![index].id;
  //                   address!.countryId = countries![index].id;
  //                 });
  //                 Navigator.pop(contextBuilder);
  //                 states = await Services()
  //                     .widget
  //                     .loadStates(countries![index]);
  //                 setState(() {});
  //               },
  //               child: ListTile(
  //                 leading: countries![index].icon != null
  //                     ? SizedBox(
  //                   height: 40,
  //                   width: 60,
  //                   child: Image.network(
  //                     countries![index].icon!,
  //                     fit: BoxFit.cover,
  //                   ),
  //                 )
  //                     : (countries![index].code != null
  //                     ? Image.asset(
  //                   picker.CountryPickerUtils
  //                       .getFlagImageAssetPath(
  //                       countries![index].code!),
  //                   height: 40,
  //                   width: 60,
  //                   fit: BoxFit.fill,
  //                   package: 'country_pickers',
  //                 )
  //                     : const SizedBox(
  //                   height: 40,
  //                   width: 60,
  //                   child: Icon(Icons.streetview),
  //                 )),
  //                 title: Text(countries![index].name!),
  //               ),
  //             );
  //           },
  //         ),
  //       ),
  //     ),
  //   ),
  // );
}
