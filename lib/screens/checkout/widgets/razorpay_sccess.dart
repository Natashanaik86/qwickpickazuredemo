import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../common/constants.dart';
import '../../../generated/l10n.dart';
import '../../../models/order/order.dart';
import '../../../models/user_model.dart';


class RazorpaySuccess extends StatefulWidget {

  //const RazorpaySuccess({Key? key}) : super(key: key);
  // final String? number;
  // const RazorpaySuccess({this.number});
  final Order? order;
  const RazorpaySuccess({this.order});

  @override
  State<RazorpaySuccess> createState() => _RazorpaySuccessState();
}


class _RazorpaySuccessState extends State<RazorpaySuccess> {


  Future<String> fetchPhoneNumber() async {
    final response = await http.get(
      Uri.parse('$baseUrl/wp-json/wc/v2/flutter/get_support_contact/'),
      // Uri.parse('https://qwickpick.com/wp-json/wc/v2/flutter/get_support_contact/'),
    );

    if (response.statusCode == 200) {
      final decodedData = json.decode(response.body);
      final phoneNumber = decodedData['contact_no'];
      // final phoneNumber = response.body;
      if (phoneNumber != null) {
        printLog('phone-number $phoneNumber');
        return phoneNumber;
      } else {
        throw Exception('Phone number not found in response');
      }
    } else {
      throw Exception('Failed to fetch phone number');
    }
  }


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    printLog('at init state ');
    fetchPhoneNumber();
    printLog('success screen');
    printLog('razorpayFailed value for failed ');
  }


  @override
  Widget build(BuildContext context) {
    final userModel = Provider.of<UserModel>(context);
    printLog('success page after razorpay ${widget.order}');
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: AppBar(
        backgroundColor: Theme.of(context).backgroundColor,
        title: Text(
          S.of(context).checkout,
          style: TextStyle(
            color: Theme.of(context).colorScheme.secondary,
            fontWeight: FontWeight.w500,
          ),
        ),
        // leading: GestureDetector(
        //   onTap: () {
        //    Navigator.of(context).pop();
        //    Navigator.of(context).pop();
        //   },
        //   child: Icon(
        //     isIos ? Icons.arrow_back_ios : Icons.arrow_back,
        //   ),
        // ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(10.0),
        child: ListView(
          //crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              margin: const EdgeInsets.only(top: 20),
              decoration: BoxDecoration(color: Theme.of(context).primaryColorLight),
              child: Padding(
                padding: const EdgeInsets.all(15.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    //if (widget.order?.number != null)
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            S.of(context).orderNo,
                            style: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.w600,
                                color: Theme.of(context).colorScheme.secondary),
                          ),
                          const SizedBox(width: 5),
                          Expanded(
                            child: Text(
                              //'order',
                              '#${widget.order!.number}',
                              style: TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.w600,
                                  color: Theme.of(context).colorScheme.secondary),
                            ),
                          )
                        ],
                      )
                  ],
                ),
              ),
            ),
            const SizedBox(height: 30),
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: const [
                Text(
                  'Your order is placed successfully !',
                  style: TextStyle(
                      fontSize: 16,
                      color: kSuccessGreen,
                      fontWeight: FontWeight
                          .w600), //Theme.of(context).colorScheme.secondary),
                ),
                Text(
                  'Status: Processing.',
                  style: TextStyle(
                      fontSize: 16,
                      color: kSuccessGreen,
                      fontWeight: FontWeight.w600),
                )
              ],
            ),

            const SizedBox(height: 15),

            if (userModel.user != null)
              Padding(
                padding: const EdgeInsets.only(top: 50),
                child: Column(
                  children: [
                    // code added start for show failed screen at 26-5-2023 by natasha
                    // if(tmp)
                    Column(
                      children: [
                        const Text(
                          'For help',
                          style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
                        ),
                        const Text(
                          'contact our team',
                          style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
                        ),
                        const Text(
                          'Time 9:00AM to 6:00PM',
                          style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
                        ),
                        const SizedBox(height: 24),
                        Row(children: [
                          Expanded(
                            child: ButtonTheme(
                              height: 45,
                              child: ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                  elevation: 0,
                                  primary: Theme.of(context).primaryColor,
                                  onPrimary: Colors.white,
                                ),
                                onPressed: () {
                                  fetchPhoneNumber().then((phoneNumber) {
                                    final uri =
                                    Uri(scheme: 'tel', path: phoneNumber);
                                    launchUrl(uri);
                                  }).catchError((error) {
                                    printLog('Failed to fetch phone number: $error');
                                  });
                                },
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Icon(
                                      Icons.call,
                                      color:
                                      Theme.of(context).colorScheme.secondary,
                                    ),
                                    const SizedBox(
                                      width: 2,
                                    ),
                                    Text(
                                      //S.of(context).showAllMyOrdered.toUpperCase(),
                                        'Call support',
                                        style: TextStyle(
                                            fontSize: 15,
                                            color: Theme.of(context)
                                                .colorScheme
                                                .secondary,
                                            fontWeight: FontWeight.w700)),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ]),
                      ],
                    ),
                    // code ended for failed screen at 26-5-2023
                    const SizedBox(height: 8),
                    Row(
                      children: [
                        Expanded(
                          child: ButtonTheme(
                            height: 45,
                            child: ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                elevation: 0,
                                primary: Theme.of(context).primaryColor,
                                onPrimary: Colors.white,
                              ),
                              onPressed: () {
                                final user =
                                    Provider.of<UserModel>(context, listen: false)
                                        .user;
                                Navigator.of(context).pushNamed(
                                  RouteList.orders,
                                  arguments: user,
                                );
                              },
                              child: Text(
                                //S.of(context).showAllMyOrdered.toUpperCase(),
                                  'Go to my orders',
                                  style: TextStyle(
                                      fontSize: 15,
                                      color:
                                      Theme.of(context).colorScheme.secondary,
                                      fontWeight: FontWeight.w700)),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
          ],
        ),
      ),
    );
  }


}
